import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Songs = require('../../../../models/Songs');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let song = await Songs.find({})
  .populate({
    path: "created_by",
    model: "Users",
  })
  .populate({
    path: "theme",
    model: "Themes",
  })
  .populate({
    path: "genre",
    model: "Genres",
  })
  .populate({
    path: "instrument",
    model: "Instruments",
  })
  .populate({
    path: "mood",
    model: "Moods",
  })
  .exec();
  res.status(200).json(song);
})

export default connectDb(handler);