import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Users = require("../../../../../models/Users");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let user = await Users.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(user);
});

export default connectDb(handler);
