import React from "react";
import Router from "next/router";
import Link from "next/link";
// ui
import { Form, Input, Button, Row, Col, Checkbox } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleFinish = values => {
    this.props.onFinish(values);
  };

  render() {
    return (
      <div className="login-content">
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={this.handleFinish}
        >
          <h1>Create your account</h1>
          <Row gutter={24}>
            <Col md={12}>
              <Form.Item
                name="first_name"
                rules={[
                  { required: true, message: "Please input your First Name!" }
                ]}
              >
                <Input placeholder="First Name" />
              </Form.Item>
            </Col>
            <Col md={12}>
              <Form.Item
                name="last_name"
                rules={[
                  { required: true, message: "Please input your Last Name!" }
                ]}
              >
                <Input placeholder="Last Name" />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            name="email"
            rules={[{ required: true, message: "Please input your Email!" }]}
          >
            <Input
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Your Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Your Password"
            />
          </Form.Item>

          <Form.Item
            name="agreement"
            valuePropName="checked"
            rules={[
              {
                validator: (_, value) =>
                  value
                    ? Promise.resolve()
                    : Promise.reject("Please accept the terms of use!")
              }
            ]}
          >
            <Checkbox>
              I'm at least 16 years old and agree to{" "}
              <Link href="/terms-of-service">
                <a target="_blank">Terms of Use</a>
              </Link>{" "}
              and{" "}
              <Link href="/privacy-policy">
                <a target="_blank">Privacy Policy</a>
              </Link>
            </Checkbox>
          </Form.Item>

          <Form.Item>
            <div className="actions">
              <a className="login-form-forgot" href="">
                Forgot password
              </a>
            </div>
          </Form.Item>

          <div className="action-sb">
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Create Account
            </Button>
            <div className="act-register">
              <p>Already have an account?</p>{" "}
              <a onClick={() => Router.push("/auth/login")}>Login</a>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default RegisterForm;
