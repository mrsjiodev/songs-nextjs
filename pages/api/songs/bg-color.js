import connectDb from '../../../middleware/dbMiddleware'
import nextConnect from 'next-connect';
import bgColors from '../../../common/bgColors';
import Songs from '../../../models/Songs';

const handler = nextConnect();

handler.get(async (req, res) => {
  let all_songs = await Songs.find({}).exec();

  for (let i = 0; i < all_songs.length; i++) {
    const song = all_songs[i];
    song.bgColor = bgColors[Math.floor(Math.random() * bgColors.length)];
    song.save();
  }

  res.status(200).json({ status: "success", msg: 'Updated background color all songs successfully!' })
})

export default connectDb(handler);