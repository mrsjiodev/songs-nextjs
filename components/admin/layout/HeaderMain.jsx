import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import { getProfile } from "../../../redux/actions/admin/users";
import { Row, Col, Badge, Menu, Dropdown, Avatar } from "antd";
import { DownOutlined } from "@ant-design/icons";

class HeaderMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    await this.props.getProfile();
  };

  onLogout = () => {
    localStorage.removeItem("jwt_token_admin");
    Router.push("/admin")
  };

  onProfile = () => {
    Router.push("/admin/dashboard/profile");
  }

  render() {
    const { user } = this.props;

    return (
      <div className="header">
        <div className="container">
          <Row gutter={24}>
            <Col lg={4} md={4} sm={4} xs={4}>
              <div className="logo">
                <Link href="/admin/dashboard">
                  <span>SONGS ADMIN</span>
                </Link>
              </div>
            </Col>
            <Col lg={14} md={10} sm={8} xs={8}>
              <div className="navBar">
              </div>
            </Col>
            <Col lg={6} md={10} sm={12} xs={12}>
              <div className="navMainRight">
                <div className="actions">

                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item key="0" onClick={this.onProfile}>
                          Profile
                        </Menu.Item>
                        <Menu.Divider />
                        <Menu.Item key="3" onClick={this.onLogout}>
                          Logout
                        </Menu.Item>
                      </Menu>
                    }
                    trigger={["click"]}
                  >
                    <a
                      className="ant-dropdown-link"
                      onClick={e => e.preventDefault()}
                    >
                      <Avatar style={{ background: "#f56a00", marginRight: 5 }}>
                        {(user && user.first_name.substring(0, 1)) || "A"}
                      </Avatar>{" "}
                      {(user && user.first_name) || ""} <DownOutlined />
                    </a>
                  </Dropdown>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.adminUser.user
  };
};

const mapDispatchToProps = {
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMain);
