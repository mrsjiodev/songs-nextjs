// packages
import React from "react";
import { connect } from "react-redux";
// actions
import { getProfile } from "../../redux/actions/users";
import {
  getPopulers,
  getTopSongs,
  favoriteSong,
  setRateSong
} from "../../redux/actions/songs";
import { allMoods } from "../../redux/actions/moods";
import { allThemes } from "../../redux/actions/themes";
import { allGenres } from "../../redux/actions/genres";
import { allInstruments } from "../../redux/actions/instruments";
import { login } from "../../redux/actions/auth";
// components
import HeaderBar from "../../components/layouts/Header";
import Filters from "../../components/explore/Filters";
import Categories from "../../components/common/Categories";
import PopulerWeek from "../../components/explore/PopulerWeek";
import TopSongs from "../../components/explore/TopSongs";
import PlayingBar from "../../components/playingBar/PlayingBar";
import LoginPopup from "../../components/auth/LoginPopup";
// ui
import { Layout, Form, message, Modal } from "antd";

const { Header, Content, Footer } = Layout;

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wavesurferMain: null,
      showPlayMusic: false,
      dataMusicPlaying: null,
      isPlayingMusic: false,
      isShowFilter: false,
      keywork: "",
      top_songs: [],
      populers: [],
      filters: null,
      isRequestLogin: false
    };
    this.formRef = React.createRef();
  }

  componentDidMount = async () => {
    await this.props.getTopSongs();

    this.props.allInstruments();
    this.props.allGenres();
    this.props.allMoods();
    this.props.allThemes();

    (await this.props.top_songs) &&
      this.setState({ top_songs: this.props.top_songs });

    if (this.props.filterGenre) {
      await this.setState({
        filters: { genre: this.props.filterGenre._id },
        isShowFilter: true
      });

      await this.formRef.current.setFieldsValue({
        genre: this.props.filterGenre._id
      });

      await this.onSearchSongs();
    }

    
  };

  onShowPopupLogin = isRequestLogin => {
    this.setState({ isRequestLogin });
  };

  onFilter = async () => {
    if (this.state.isShowFilter) {
      await this.setState({ filters: null });
      await this.onSearchSongs();
    }

    this.setState({ isShowFilter: !this.state.isShowFilter });
  };

  onSetWavesurferMain = wavesurferMain => {
    this.setState({ wavesurferMain });
  };

  onPlayMusic = async dataMusicPlaying => {
    this.setState({
      dataMusicPlaying,
      showPlayMusic: true,
      isPlaying: true
    });
    await this.props.setRateSong({ song_id: dataMusicPlaying._id });
  };

  onChangeStatusMusic = isPlayingMusic => {
    this.setState({ isPlayingMusic });
  };

  onNextPrevMusic = action => {
    const { songs } = this.props;
    const { dataMusicPlaying } = this.state;

    for (let i = 0; i < songs.length; i++) {
      const el = songs[i];
      if (el.id === dataMusicPlaying.id) {
        if (action === "next") {
          this.setState({ dataMusicPlaying: songs[i + 1] });
          break;
        } else {
          this.setState({ dataMusicPlaying: songs[i - 1] });
          break;
        }
      }
    }
  };

  onSearchSongs = async () => {
    const { keywork, filters } = this.state;
    const { top_songs } = this.props;

    let _data = [];

    if (keywork !== "") {
      _data = top_songs.filter(el => {
        let _artist_name = el.artist_name.toLowerCase();
        let _track_title = el.track_title.toLowerCase();
        let _keywork = this.state.keywork.toLowerCase();

        if (
          _artist_name.search(_keywork) > -1 ||
          _track_title.search(_keywork) > -1
        ) {
          return el;
        }
      });
    } else {
      _data = top_songs;
    }

    if (filters) {
      let _results_filters = _data.filter(el => {
        let count = 0;
        let total_filters = Object.entries(filters).length;

        Object.entries(filters).forEach(([key, value]) => {
          if (String(el[key]._id) === String(value)) {
            count = count + 1;
          }
          return 1;
        });

        if (count === total_filters) {
          return el;
        }
      });

      await this.setState({ top_songs: _results_filters });
    } else {
      await this.setState({ top_songs: _data });
    }
  };

  onChangeKeywork = async keywork => {
    await this.setState({ keywork });
    await this.onSearchSongs();
  };

  onChangeFilters = async (value, name) => {
    const { filters } = this.state;
    await this.setState({ filters: { ...filters, [name]: value } });

    await this.onSearchSongs();
  };

  onFavoriteSong = async (song_id, isFavorite) => {
    await this.props.favoriteSong(song_id, isFavorite);
    (await this.props.isSaveFavorite)
      ? message.success(`${isFavorite ? "Saved" : "Unsaved"} song!`)
      : message.error(`${isFavorite ? "Saved" : "Unsaved"} song failed!`);
    (await this.props.isSaveFavorite) && this.props.getProfile();
  };

  onFinish = values => {
    console.log("values", values);
  };

  onLoginPopup = async data => {
    await this.props.login(data);
    (await this.props.isLogined)
      ? message.success("Login successfully!")
      : message.error("Login failed");
    (await this.props.isLogined) && this.setState({ isRequestLogin: false });
    (await this.props.isLogined) && this.props.getProfile();
  };

  render() {
    const {
      showPlayMusic,
      dataMusicPlaying,
      isPlayingMusic,
      wavesurferMain,
      isShowFilter,
      keywork,
      top_songs,
      isRequestLogin,
    } = this.state;

    const { favorites, instruments, moods, themes, genres } = this.props;

    return (
      <Layout className="home">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderBar />
          <Filters
            onFilter={this.onFilter}
            onSearchSongs={this.onSearchSongs}
            onChangeKeywork={this.onChangeKeywork}
            keywork={keywork}
            isShowFilter={isShowFilter}
          />
        </Header>

        <Content>
          
            <Form
              ref={this.formRef}
              name="normal_login"
              className="form-upload"
              onFinish={this.onFinish}
            >
              {isShowFilter && (
              <Categories
                required={false}
                instruments={instruments}
                moods={moods}
                themes={themes}
                genres={genres}
                onChangeFilters={this.onChangeFilters}
              />
              )}
            </Form>
          <PopulerWeek
            albums={top_songs.slice(0, 6)}
            onPlayMusic={this.onPlayMusic}
            itemPlaying={dataMusicPlaying}
            isPlayingMusic={isPlayingMusic}
            wavesurferMain={wavesurferMain}
          />
          <TopSongs
            data={top_songs}
            onPlayMusic={this.onPlayMusic}
            itemPlaying={dataMusicPlaying}
            isPlayingMusic={isPlayingMusic}
            wavesurferMain={wavesurferMain}
            favorites={favorites}
            onFavoriteSong={this.onFavoriteSong}
            onShowPopupLogin={this.onShowPopupLogin}
            isRequestLogin={isRequestLogin}
            user={this.props.user}
          />
        </Content>

        {showPlayMusic && (
          <Footer>
            <PlayingBar
              item={dataMusicPlaying}
              onChangeStatusMusic={this.onChangeStatusMusic}
              onSetWavesurferMain={this.onSetWavesurferMain}
              onNextPrevMusic={this.onNextPrevMusic}
              favorites={favorites}
              onFavoriteSong={this.onFavoriteSong}
              onShowPopupLogin={this.onShowPopupLogin}
              isRequestLogin={isRequestLogin}
              user={this.props.user}
            />
          </Footer>
        )}

        <Modal
          wrapClassName="popup-login-main"
          className="popup-login-ctn"
          title={null}
          footer={null}
          closable={false}
          centered
          visible={isRequestLogin}
          onOk={() => this.onShowPopupLogin(false)}
          onCancel={() => this.onShowPopupLogin(false)}
        >
          <LoginPopup onLoginPopup={this.onLoginPopup} />
        </Modal>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    // auth
    isLogined: store.auth.isLogined,
    // categories
    themes: store.themes.themes,
    moods: store.moods.moods,
    genres: store.genres.genres,
    instruments: store.instruments.instruments,
    // users
    user: store.users.user,
    favorites: store.users.favorites,
    // songs
    top_songs: store.songs.top_songs,
    populers: store.songs.populers,
    isSaveFavorite: store.songs.isSaveFavorite,
    // genres
    filterGenre: store.genres.filterGenre
  };
};

const mapDispatchToProps = {
  // categories
  allMoods,
  allGenres,
  allThemes,
  allInstruments,
  // users
  getProfile,
  // songs
  getTopSongs,
  getPopulers,
  favoriteSong,
  setRateSong,
  // auth
  login
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
