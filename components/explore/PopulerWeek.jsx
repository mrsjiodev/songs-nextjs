// packages
import React from "react";
import moment from "moment";
// ui
import { Row, Col } from "antd";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class PopulerWeek extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.wavesurfer = null;
  }

  handlePlayMusic = async data => {
    const { wavesurferMain } = this.props;
    if (wavesurferMain) {
      await wavesurferMain.stop();
    }
    this.props.onPlayMusic(data);
  };

  loadIconPlay = item => {
    const { itemPlaying, isPlayingMusic } = this.props;
    if (itemPlaying && item._id === itemPlaying._id && isPlayingMusic) {
      return <img src="./images/icons/pause-circle-small.png" alt="" />;
    } else {
      return <img src="./images/icons/play-circle-small.png" alt="" />;
    }
  };

  render() {
    const { albums, itemPlaying, isPlayingMusic } = this.props;

    return (
      <div className="populerWeek">
        <div className="container">
          <h1 className="title">Popular this week</h1>
          <div className="songs">
            <Row gutter={24}>
              {albums &&
                albums.map((item, i) => (
                  <Col lg={4} md={8} sm={12} xs={24} key={i}>
                    <div
                      className="populerItem"
                      onClick={() => this.handlePlayMusic(item)}
                      style={{ backgroundImage: `${item.bgColor}` }}
                    >
                      {!item.bgColor && (
                        <div className="bg">
                          <img
                            src={`${URL_MEDIA}${
                              (item.image && item.image.key) || ""
                            }`}
                            alt=""
                          />
                        </div>
                      )}
                      <div className="details">
                        <a className="btn-play">{this.loadIconPlay(item)}</a>
                        <h3>{(item && item.track_title) || ""}</h3>
                      </div>
                    </div>
                  </Col>
                ))}
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default PopulerWeek;
