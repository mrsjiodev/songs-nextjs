const initData = {
  instruments: [],
  isCreatedInstrument: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_ALL_INSTRUMENTS':
      return {
        ...state,
        instruments: action.data
      }
    case 'ERROR_ALL_INSTRUMENTS':
      return {
        ...state,
        instruments: []
      }
    case 'ACTION_CREATED_INSTRUMENT':
      return {
        ...state,
        isCreatedInstrument: true
      }
    case 'ERROR_CREATED_INSTRUMENT':
      return {
        ...state,
        isCreatedInstrument: false
      }
    default:
      return {
        ...state,
      }
  }
}