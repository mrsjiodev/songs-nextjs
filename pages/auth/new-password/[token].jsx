import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { newPassword } from "../../../redux/actions/reset_password";
// components
import HeaderPage from "../../../components/layouts/Header";
// ui
import { Layout, message, Spin, Form, Input, Button } from "antd";
import { LockOutlined, RedoOutlined } from "@ant-design/icons";

const { Header, Content } = Layout;

class NewPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleFinish = async values => {
    const { token } = Router.query;
    let data = {
      token,
      password: values.password
    };

    await this.props.newPassword(data);
    (await (this.props.isNewPassword === true))
      ? message.success("Reset password successfully!")
      : message.error("Reset password failed");

    (await (this.props.isNewPassword === true)) && Router.push("/auth/login");
  };

  render() {
    return (
      <Layout className="home login">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>

        <Content>
          <div className="login-content">
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{ remember: true }}
              onFinish={this.handleFinish}
            >
              <h1>New Password</h1>

              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!"
                  }
                ]}
                // hasFeedback
              >
                <Input.Password
                  placeholder="New Password"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                />
              </Form.Item>

              <Form.Item
                name="confirm"
                dependencies={["password"]}
                // hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Please confirm your password!"
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        "The two passwords that you entered do not match!"
                      );
                    }
                  })
                ]}
              >
                <Input.Password
                  placeholder="Confirm New Password"
                  prefix={<RedoOutlined className="site-form-item-icon" />}
                />
              </Form.Item>

              <div className="action-sb">
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Save
                </Button>
              </div>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    isNewPassword: store.reset_password.isNewPassword,
    msg: store.reset_password.msg
  };
};

const mapDispatchToProps = {
  newPassword
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPassword);
