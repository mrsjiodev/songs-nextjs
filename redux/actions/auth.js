import API from '../config/request'
import request from '../config/request';

export function login(data) {
  return dispatch => {
    return API().post(`auth/signin`, data).then(res => {
      localStorage.setItem("jwt_token", res.data.token);

      dispatch({
        type: "ACTION_LOGINED",
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
      dispatch({
        type: "ERROR_LOGINED",
        msgErr: err.data.msg
      })
    })
  }
}

export function register(data) {
  return dispatch => {
    return request().post(`auth/signup`, data).then(res => {
      dispatch({
        type: 'ACTION_REGISTER_DONE',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_REGISTER',
      })
    })
  }
}

export default {
  login,
  register,
}