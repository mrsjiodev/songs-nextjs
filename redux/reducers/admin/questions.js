const initData = {
  questions: [],
  message_delete: {},
  isCreatedMood: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_CREATED_QUESTION':
      return {
        ...state,
        isCreatedMood: true
      } 
    case 'ACTION_GET_ALL_QUESTIONS':
      return {
        ...state,
        questions: action.data
      } 
      case 'ACTION_DELETE_QUESTIONS':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}