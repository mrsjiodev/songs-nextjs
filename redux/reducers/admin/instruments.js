const initData = {
  instruments: [],
  message_delete: {}
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_GET_ALL_INSTRUMENTS':
      return {
        ...state,
        instruments: action.data
      } 
      case 'ACTION_DELETE_INSTRUMENTS':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}