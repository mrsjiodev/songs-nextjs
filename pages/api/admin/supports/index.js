import nextConnect from 'next-connect'
import connectDb from '../../../../middleware/dbMiddleware'
import Supports from '../../../../models/Supports'
import Users from '../../../../models/Users'

const handler = nextConnect();

handler.post(async (req, res) => {
  let user = await Users.findOne({ role: "admin" }).exec();
  const _id = user._id;
  const { description } = req.body;

  let sp = new Supports({
    description,
    user: _id
  });

  await sp.save();

  res.status(200).json(sp)
})

export default connectDb(handler)