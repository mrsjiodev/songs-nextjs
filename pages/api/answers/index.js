import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'

const Answers = require('../../../models/Answers');
const Questions = require('../../../models/Questions');

const handler = nextConnect();

// get all supports
handler.get(async (req, res) => {
  let answers = await Answers.find({}).populate('question').exec();
  res.status(200).json(answers)
})


export default connectDb(handler)