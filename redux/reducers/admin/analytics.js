const initData = {
  analyticsUser: {},
};

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_GET_ANALYTICS_USER":
      return {
        ...state,
        analyticsUser: action.data,
      };
    default:
      return {
        ...state,
      };
  }
}
