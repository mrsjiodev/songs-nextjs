import Stripe from "stripe";
import nextConnect from 'next-connect'
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const stripe = new Stripe(process.env.STRIPE_SECRET);
const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

hanlder.post(async (req, res) => {
  try {
    const { plan_id, discount_id, type_standard, interval, billing_details } = req.body;

    console.log(req.body)

    const { _id } = req.user;

    let user = await Users.findById(_id).exec();

    let subscription = await stripe.subscriptions.create({
      customer: user.customer_stripe_id,
      coupon: discount_id,
      items: [
        {
          plan: plan_id
        }
      ],
      expand: ["latest_invoice.payment_intent"]
    })

    user.plan_default = plan_id;
    user.type_standard = type_standard;
    user.discount_id = discount_id;
    user.interval = interval;
    user.billing_details = billing_details;
    // user.payment_detail = subscription;

    user.current_period_start = subscription.current_period_start;
    user.current_period_end = subscription.current_period_end;
    user.plan_info = subscription.plan;
    user.payment_method_default = "card";

    if (subscription.latest_invoice) {
      let _latest_invoice = subscription.latest_invoice;
      user.amount = _latest_invoice.amount_paid;
      user.currency = _latest_invoice.currency;
      if (_latest_invoice.payment_intent) {
        let _payment_intent = _latest_invoice.payment_intent;
        let _data = _latest_invoice.payment_intent.charges.data;

        user.payment_method_details = _data[0].payment_method_details;
        user.payment_method = _payment_intent.payment_method;
        user.payment_method_types = _payment_intent.payment_method_types;


        // get billing address
        let pm = await stripe.paymentMethods.retrieve(_payment_intent.payment_method);
        console.log('pm: ', pm)
        user.card_info = pm.card;
        user.billing_details_default = pm.billing_details;
      }
    }


    await user.save();

    return res.status(200).json(subscription);
  } catch (err) {
    console.log(err);
    if (err.raw.message) {
      res.status(400).json({ msg: err.raw.message })
    } else {
      res.status(400).json({ msg: 'error' })
    }
  }
})

export default connectDB(hanlder)