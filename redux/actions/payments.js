import request from '../config/request'

export function createCustomer(data) {
  return dispatch => {
    return request().post(`payments/stripe/customers/create`, data).then(res => {
      dispatch({
        type: 'ACTION_STRIPE_CREATED_CUSTOMER',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_STRIPE_CREATED_CUSTOMER"
      })
    })
  }
}

export function updateCustomer(data) {
  return request().put(`payments/stripe/customers`, data).then(res => {
    dispatch({
      type: 'ACTION_STRIPE_UPDATED_CUSTOMER',
      data: res.data
    })
  }).catch(err => {
    console.log(err)
    dispatch({
      type: 'ERROR_STRIPE_UPDATED_CUSTOMER'
    })
  })
}

export function getPlans() {
  return dispatch => {
    return request().get(`payments/plans`).then(res => {
      dispatch({
        type: 'ACTION_STRIPE_ALL_PLANS',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_STRIPE_ALL_PLANS"
      })
    })
  }
}

export function createSubscription(data) {
  return dispatch => {
    return request().post(`payments/stripe/subscriptions/create`, data).then(res => [
      dispatch({
        type: 'ACTION_STRIPE_SUBSCRIPTIONS_CREATED',
        data: res.data
      })
    ]).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_STRIPE_SUBSCRIPTIONS_CREATED",
        msg: err.data.msg
      })
    })
  }
}

export function saveInfoPaypal(data) {
  return dispatch => {
    return request().post(`payments/paypal/save-info`, data).then(res => {
      dispatch({
        type: 'ACTION_SAVE_INFO_PAYPAL',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_SAVE_INFO_PAYPAL',
      })
    })
  }
}

export function checkDiscount(data) {
  return dispatch => {
    return request().post(`payments/stripe/discount/check-code`, data).then(res => {
      dispatch({
        type: 'ACTION_DISCOUNT_CHECKCODE',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_DISCOUNT_CHECKCODE'
      })
    })
  }
}

export function allInvoices() {
  return dispatch => {
    return request().get(`payments/stripe/invoices`).then(res => {
      dispatch({
        type: 'ACTION_ALL_INVOICES',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_ALL_INVOICES"
      })
    })
  }
}

export function changePaymentMethod(data) {
  console.log('data: ', data)
  return dispatch => {
    return request().post(`payments/stripe/customers/card`, data).then(res => {
      dispatch({
        type: "ACTION_CHANGE_PAYMENT_METHOD",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CHANGE_PAYMENT_METHOD"
      })
    })
  }
}

export function setDefaulPaymentMethod(data) {
  return dispatch => {
    return request().put(`payments/payment-method`, data).then(res => {
      dispatch({
        type: 'ACTION_SET_DEFAULT_PAYMENT_METHOD',
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_SET_DEFAULT_PAYMENT_METHOD'
      })
    })
  }
}

export function removePaymentMethod(data) {
  return dispatch => {
    return request().delete(`payments/payment-method`, data).then(res => {
      dispatch({
        type: "ACTION_DELETED_PAYMENT_METHOD"
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_DELTED_PAYMENT_METHOD"
      })
    })
  }
}

export function changeBillingAddress(data) {
  return dispatch => {
    return request().put(`payments/stripe/customers/address`, data).then(res => {
      dispatch({
        type: 'ACTION_CHANGE_BILLING_ADDRESS',
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CHANGE_BILLING_ADDRESS"
      })
    })
  }
}

export default {
  createCustomer,
  getPlans,
  createSubscription,
  saveInfoPaypal,
  checkDiscount,
  updateCustomer,
  allInvoices,
  changeBillingAddress,
  changePaymentMethod,
  setDefaulPaymentMethod,
  removePaymentMethod
}