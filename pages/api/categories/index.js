import connectDb from '../../../middleware/dbMiddleware'

const nextConnect = require('next-connect')

const Categories = require('../../../models/Categories');

const handler = nextConnect();


handler.get(async (req, res) => {
  try {
    let categories = await Categories.find({}).exec();
    res.status(200).json(categories);
  } catch (error) {
    res.status(400).json({ msg: "Connect mongoose failed" })
  }


})

export default connectDb(handler);
