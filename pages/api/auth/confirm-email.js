import connectDb from '../../../middleware/dbMiddleware';
import jwt from 'jsonwebtoken';

// const transporter = require("../../../middleware/transporter");
const nextConnect = require('next-connect')
const Users = require('../../../models/Users')
const nodemailer = require("nodemailer");
const handler = nextConnect();

handler.post(async (req, res) => {
  try {
    const { email } = req.body;
    console.log(req.body);

    let user = await Users.findOne({ email }).exec();
    console.log(user);
    if (user.email) {
      // send mail 

      const transporter = nodemailer.createTransport({
        // host: "mail.hostedemail.com",
        host: "smtp.gmail.com",
        // host: "mail.hostedemail.com",  
        // secureConnection: true,

        port: 587,
        secure: false, 
        auth: {
          user: process.env.MAIL_NAME,
          pass: process.env.MAIL_PASSWORD 
          // user: 'songswebapp@gmail.com',
          // pass: 'A123123z!' 
        }
      });

      // create token with 1 hour
      let token = await jwt.sign({
        email: user.email,
        _id: user._id
      }, 'secret', { expiresIn: '7d' });

      const mainOptions = {
        from: 'Thanh Batmon',
        to: `${email}`,
        subject: 'Password reset instructions',
        // text: 'You recieved message from ' + req.body.email,
        html: `<div class="content" style="width: 600px; margin: 60px auto; border: 1px solid #ddd; background: #f8f8f8; border-radius: 15px; overflow: hidden; box-shadow: 0px 0px 5px 0px rgba(209,194,209,1);">
        <div>
          <img
            src="https://dnkapj0826hbk.cloudfront.net/images/d8152eed-94e8-4562-b336-64aa85e5d433.jpg"
            alt=""
            style="width: 100%;"
          />
          <h3
            style="
              font-size: 28px;
              font-weight: 700;
              text-transform: uppercase;
              color: #4900ff;
              text-align: center;
              font-family: 'Poppins', sans-serif;
            "
          >
            Songs
          </h3>
        </div>
        <div style="padding: 30px; ">
          <h1 style="color: #4900ff; margin: 0 0 30px; font-size: 18px; font-weight: 500;">Hi ${user.first_name} ${user.last_name}!</h1>
          <p style="font-size: 16px; font-family: 'Poppins', sans-serif; margin-bottom: 15px;">Need to reset your Songs password? <a target="_blank" href="${process.env.MAIL_DOMAIN}auth/new-password/${token}">Click here</a></p>
          <p style="font-size: 16px; font-family: 'Poppins', sans-serif; margin-bottom: 15px;">If you think you received this email by mistake, feel free to ignore it.</p>
          <p style="font-size: 16px; font-family: 'Poppins', sans-serif; margin-bottom: 15px;">
            Thanks,
            <br/>
            The Songs Team
            </p>
        </div>
      </div>`
      }

      transporter.sendMail(mainOptions, function (err, info) {
        if (err) {
          console.log(err);
          // res.redirect('/');
        } else {
          res.status(200).json({ msg: 'success', msg: 'Email forgot password has been sent. Please check your email and continue!' })
        }
      });

    } else {
      res.status(400).json({ status: 'error', msg: 'Email does not exist.' });
    }
  } catch (error) {
    console.log(error)
    res.status(400).json({ status: 'error', msg: 'Error. Email does not exist.' })
  }
})

export default connectDb(handler);