const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const SongSchema = new Schema({
  image: { type: Object },
  audio: { type: Object },
  bgColor: {type: String},
  duration: { type: Number },
  track_title: { type: String },
  artist_name: { type: String },
  type: { type: Boolean, default: false }, // false: free download, true: paid users
  mood: { type: Schema.Types.ObjectId, ref: 'Moods' },
  theme: { type: Schema.Types.ObjectId, ref: 'Themes' },
  genre: { type: Schema.Types.ObjectId, ref: 'Genres' },
  instrument: { type: Schema.Types.ObjectId, ref: 'Instruments' },
  created_by: { type: Schema.Types.ObjectId, ref: 'Users' },
  updated_by: { type: Schema.Types.ObjectId, ref: 'Users' },
  total_played: { type: Number, default: 0 },
  ranking: { type: Number, default: 0 }
}, {
  timestamps: true
})

module.exports = mongoose.models.Songs || mongoose.model('Songs', SongSchema);