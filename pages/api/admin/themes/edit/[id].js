import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Themes = require("../../../../../models/Themes");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let theme = await Themes.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(theme);
});

export default connectDb(handler);
