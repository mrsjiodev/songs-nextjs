import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Supports = require("../../../../../models/Supports");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Supports.findOneAndDelete({ _id: id }, (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Supports Not found" });
    else return res.status(200).json({ message: "Delete Supports successfully" });
  });
});

export default connectDb(handler);
