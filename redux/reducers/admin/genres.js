const initData = {
  genres: [],
  message_delete: {}
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_GET_ALL_GENRES':
      return {
        ...state,
        genres: action.data
      } 
      case 'ACTION_DELETE_GENRES':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}