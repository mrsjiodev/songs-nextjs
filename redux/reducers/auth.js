const initData = {
  isLogined: false,
  token: null,
  isRegister: false,
  user: null,
  msgErr: ''
}

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_LOGINED":
      return {
        ...state,
        isLogined: true,
        token: action.data.token,
      }
    case 'ERROR_LOGINED':
      return {
        ...state,
        isLogined: false,
        msgErr: action.msgErr
      }
    case 'ACTION_REGISTER_DONE':
      return {
        ...state,
        isRegister: true,
        user: action.data
      }
    case 'ERROR_REGISTER':
      return {
        ...state,
        isRegister: false,
        user: null
      }
    default:
      return {
        ...state,
      }
  }
}