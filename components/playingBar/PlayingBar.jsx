// packages
import moment from "moment";
import Router from "next/router";
// ui
import { Row, Col, Button, Progress, Slider, Tooltip, Spin } from "antd";
import {
  HeartOutlined,
  DownloadOutlined,
  StepBackwardOutlined,
  StepForwardOutlined
} from "@ant-design/icons";
// icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faVolumeMute,
  faVolumeUp,
  faVolumeDown,
  faVolumeOff,
  faPlay,
  faPause
} from "@fortawesome/free-solid-svg-icons";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class PlayingBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: true,
      volume: 1,
      totalTime: "00:00",
      currentTime: "00:00",
      processPlaying: 0,
      loadingWave: false
    };
    this.wavesurfer = null;
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.item !== this.props.item) {
      this.wavesurfer.stop();
      this.loadNewMusicPlay(nextProps);
    }
  };

  componentDidMount = async () => {
    const { item } = this.props;

    let wavesurfer = WaveSurfer.create({
      container: "#waveform",
      waveColor: "violet",
      progressColor: "purple",
      scrollParent: false,
      autoCenter: true,
      forceDecode: true,
      cursorColor: "#fff",
      height: 31,
      mediaType: "audio",
      progressColor: "#4900FF",
      responsive: true,
      barWidth: 1,
      barRadius: 1,
      waveColor: "#C4C4C4"
    });

    let _self = this;
    if (item) {
      _self.props.onChangeStatusMusic(true);
      _self.setState({ loadingWave: true });
      // load file play
      wavesurfer.load(`${URL_MEDIA}${item.audio.key}`);

      // set volume
      wavesurfer.setVolume(this.state.volume);

      wavesurfer.on("loading", process => {
        wavesurfer.stop();
        if (process === 100) {
          _self.setState({ loadingWave: false });
        }
      });

      wavesurfer.on("ready", function () {
        wavesurfer.play();
      });

      wavesurfer.on("audioprocess", function () {
        if (wavesurfer.isPlaying()) {
          const totalTime = wavesurfer.getDuration();
          const currentTime = wavesurfer.getCurrentTime();

          let processPlaying = ((currentTime / totalTime) * 100).toFixed(2);

          let formatted = moment.utc(currentTime * 1000).format("mm:ss");
          let formattedTotal = moment.utc(totalTime * 1000).format("mm:ss");

          _self.setState({
            currentTime: formatted,
            processPlaying,
            totalTime: formattedTotal,
            isPlaying: true
          });
        }
      });

      wavesurfer.on("interaction", e => {});

      wavesurfer.on("destroy", () => {});

      wavesurfer.on("seek", time => {
        const totalTime = wavesurfer.getDuration();
        const currentTime = (time * totalTime).toFixed(2);
        // convert to time
        let formattedCurrent = moment.utc(currentTime * 1000).format("mm:ss");
        let processPlaying = (time * 100).toFixed(2);

        _self.setState({ currentTime: formattedCurrent, processPlaying });
      });

      wavesurfer.on("finish", () => {
        _self.props.onChangeStatusMusic(false);
        wavesurfer.stop();
        _self.setState({
          currentTime: "00:00",
          processPlaying: 0,
          isPlaying: false
        });
      });

      this.wavesurfer = wavesurfer;
      this.props.onSetWavesurferMain(wavesurfer);
    }
  };

  componentWillUnmount = async () => {
    this.wavesurfer.stop();
    this.wavesurfer.seekTo(0);
  };

  getAudio = path => {
    let src = path.substring(6);
    return src;
  };

  loadNewMusicPlay = async props => {
    let { item } = props;
    let _self = this;

    _self.props.onChangeStatusMusic(true);

    _self.setState({ loadingWave: true });
    // close old music
    _self.wavesurfer.stop();
    // load new music
    _self.wavesurfer.load(`${URL_MEDIA}${item.audio.key}`);
    _self.wavesurfer.on("loading", process => {
      if (process === 100) {
        _self.setState({ loadingWave: false });
      }
    });

    _self.wavesurfer.on("ready", function () {
      _self.wavesurfer.play();
      // _self.props.onChangeStatusMusic();
    });

    const totalTime = _self.wavesurfer.getDuration();

    let formattedTotal = moment.utc(totalTime * 1000).format("mm:ss");
    _self.setState({
      currentTime: "00:00",
      processPlaying: 0,
      totalTime: formattedTotal,
      isPlaying: true
    });
  };

  formatTime = secs => {
    const formatted = moment.utc(secs * 1000).format("mm:ss");
    return formatted;
  };

  handlePlaying = () => {
    const { isPlaying } = this.state;
    if (isPlaying) {
      this.props.onChangeStatusMusic(false);
      this.wavesurfer.pause();
    } else {
      this.props.onChangeStatusMusic(true);
      this.wavesurfer.play();
    }
    // getVolume
    let volume = this.wavesurfer.getVolume();
    this.setState({ isPlaying: !this.state.isPlaying, volume });
  };

  handleChangeVolume = value => {
    let volume = value / 100;
    volume.toFixed(2);
    this.wavesurfer.setVolume(volume);
    this.setState({ volume });
  };

  hanldeMuted = isMuted => {
    if (isMuted) {
      this.wavesurfer.setMute();
    } else {
      this.wavesurfer.setVolume(this.state.volume);
    }
    this.setState({ isMuted });
  };

  handleDownloadFile = url => {
    const { user } = this.props;
    if (user) {
      if (user.plan_default) {
        window.location.href = url;
      } else {
        Router.push("/plans");
      }
    } else {
      this.props.onShowPopupLogin(true);
    }
  };

  handleFavorite = song => {
    const { user, onFavoriteSong } = this.props;
    if (user) {
      onFavoriteSong(song._id, !this.checkFavoriteSong(song._id));
    } else {
      this.props.onShowPopupLogin(true);
    }
  };

  checkFavoriteSong = song_id => {
    const { favorites } = this.props;
    let cheched = favorites.includes(song_id);
    return cheched;
  };

  render() {
    const {
      isPlaying,
      volume,
      isMuted,
      totalTime,
      currentTime,
      processPlaying,
      loadingWave
    } = this.state;

    const { item, onFavoriteSong } = this.props;

    return (
      <div className="playingBar">
        <div className="songItem">
          <Row gutter={24}>
            <Col md={6} sm={8} xs={12}>
              <div className="info">
                <div
                  className="status"
                  style={{ backgroundImage: `${item.bgColor}` }}
                >
                  {!item.bgColor && (
                    <div className="bg">
                      <img
                        src={`${URL_MEDIA}${
                          (item.image && item.image.key) || ""
                        }`}
                        alt=""
                      />
                    </div>
                  )}
                  <a className="btn-play">
                    {isPlaying ? (
                      <img src="./images/icons/pause-circle.png" alt="" />
                    ) : (
                      <img src="./images/icons/play-circle.png" alt="" />
                    )}
                  </a>
                </div>
                <div className="song">
                  <h3>
                    <Tooltip title={item.track_title}>
                      {item.track_title}
                    </Tooltip>
                  </h3>
                  <p>{item.artist_name}</p>
                </div>
              </div>
            </Col>
            <Col md={15} sm={16} xs={12}>
              <div className="playing-actions">
                <div className="actions-song">
                  <a
                    className="next-prev btn-prev"
                    title="Previous"
                    onClick={() => this.props.onNextPrevMusic("prev")}
                  >
                    <StepBackwardOutlined />
                  </a>
                  <Progress
                    trailColor="#E8E8E8"
                    strokeColor="#4900FF"
                    width={44}
                    type="circle"
                    percent={processPlaying}
                    format={percent => {
                      return (
                        <a
                          className="pause_play"
                          onClick={this.handlePlaying}
                          title={`${isPlaying ? "Pause" : "Play"}`}
                        >
                          {isPlaying ? (
                            <FontAwesomeIcon icon={faPause} size="lg" />
                          ) : (
                            <FontAwesomeIcon icon={faPlay} size="lg" />
                          )}
                        </a>
                      );
                    }}
                  />
                  <a
                    className="next-prev btn-next"
                    title="Next"
                    onClick={() => this.props.onNextPrevMusic("next")}
                  >
                    <StepForwardOutlined />
                  </a>
                </div>
                <div className="chartSong">
                  <span className="realtime">{currentTime}</span>

                  <div className="chart_line">
                    <div id="waveform"></div>
                  </div>

                  <span className="realtime totalTime">{totalTime}</span>
                  <div className="volume">
                    {isMuted ? (
                      <span onClick={() => this.hanldeMuted(false)}>
                        <FontAwesomeIcon icon={faVolumeMute} size="lg" />
                      </span>
                    ) : (
                      <span onClick={() => this.hanldeMuted(true)}>
                        {volume > 0 && volume < 1 && (
                          <FontAwesomeIcon
                            icon={faVolumeDown}
                            style={{ fontSize: 18 }}
                          />
                        )}
                        {volume === 0 && (
                          <FontAwesomeIcon
                            className="volumOff"
                            icon={faVolumeOff}
                            style={{ fontSize: 18 }}
                          />
                        )}
                        {volume === 1 && (
                          <FontAwesomeIcon
                            icon={faVolumeUp}
                            style={{ fontSize: 18 }}
                          />
                        )}
                      </span>
                    )}

                    <div className="volumeNum">
                      <Slider
                        value={isMuted ? 0 : volume * 100}
                        onChange={this.handleChangeVolume}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} sm={0} xs={0}>
              <div className="actions">
                <div className="btns">
                  <Button
                    className={`${
                      this.checkFavoriteSong(item._id) ? "favorite" : ""
                    }`}
                    type="default"
                    onClick={() => this.handleFavorite(item)}
                    icon={
                      this.checkFavoriteSong(item._id) ? (
                        <img
                          style={{ width: 18 }}
                          src="/images/icons/heart-fill.svg"
                        />
                      ) : (
                        <HeartOutlined />
                      )
                    }
                    size={24}
                  />
                  <Button
                    type="default"
                    icon={<DownloadOutlined />}
                    size={24}
                    onClick={() =>
                      this.handleDownloadFile(`${URL_MEDIA}${item.audio.key}`)
                    }
                  />
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default PlayingBar;
