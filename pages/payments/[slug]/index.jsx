// packages
import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
// components
import HeaderBar from "../../../components/layouts/Header";
import Standard from "../../../components/payments/Standard";
import PaymentContent from "../../../components/payments/PaymentContent";
// actions
import { getPlans } from "../../../redux/actions/payments";
// ui
import { Layout, Menu, Form, Row, Col, Tabs, Spin } from "antd";

const { Header, Content, Footer } = Layout;
const { TabPane } = Tabs;

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      plan_id: null,
      currency: "usd",
      amount_plan: 0,
      isLoading: false,
      tabIndex: 1
    };
  }

  componentWillUpdate = async (nextProps, nextState) => {
    if (nextProps.subscription !== this.props.subscription) {
      this.onLoading(false);
    }
  };

  componentDidMount = async () => {
    await this.props.getPlans();
    (await this.props.plans.length) > 0 && this.setDataPlan();
  };

  handleSubscription = async data => {
    console.log("handleSubscription: ", data);
    this.onLoading(false);
  };

  setDataPlan = () => {
    const { plans } = this.props;
    const { tabIndex } = this.state;
    const { slug } = Router.query;

    let plan_id = null;
    let amount_plan = 0;
    let currency = "usd";
    let plan = null;
    if (slug === "basic") {
      if (tabIndex === 1) {
        plan = plans[3];
      } else {
        plan = plans[2];
      }
    } else {
      if (tabIndex === 1) {
        plan = plans[1];
      } else {
        plan = plans[0];
      }
    }
    plan_id = plan.id;
    amount_plan = plan.amount;
    currency = plan.currency;

    this.setState({ plan_id, amount_plan, currency });
  };

  onLoading = isLoading => {
    this.setState({ isLoading });
  };

  onSelectedPlan = async tabIndex => {
    await this.setState({
      tabIndex
    });

    await this.setDataPlan();
  };

  render() {
    const { plan_id, amount_plan, currency, isLoading } = this.state;

    return (
      <Spin spinning={isLoading}>
        <Layout className="home payment">
          <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
            <HeaderBar />
          </Header>
          <Content>
            <div className="payment-ctn">
              <Row gutter={48}>
                <Col md={14}>
                  <PaymentContent
                    onLoading={this.onLoading}
                    plan_id={plan_id}
                    currency={currency}
                    amount_plan={amount_plan}
                    onSelectedPlan={this.onSelectedPlan}
                  />
                </Col>
                <Col md={10}>
                  <Standard />
                </Col>
              </Row>
            </div>
          </Content>
        </Layout>
      </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    plans: store.payments.plans,
    isSubscription: store.payments.isSubscription,
    subscription: store.payments.subscription
  };
};

const mapDispatchToProps = {
  getPlans
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
