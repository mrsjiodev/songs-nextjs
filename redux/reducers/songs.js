const initData = {
  songs: [],
  song: null,
  top_songs: [],
  populers: [],
  isCreatedSong: false,
  isError: false,
  isSaveFavorite: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_SONGS_CREATED':
      return {
        ...state,
        song: action.data,
        isCreatedSong: true
      }
    case 'ERROR_SONGS_CREATED':
      return {
        ...state,
        song: null,
        isCreatedSong: false,
        isError: true
      }
    case 'ACTION_GET_TOP_SONGS':
      return {
        ...state,
        top_songs: action.data
      }
    case 'ERROR_GET_TOP_SONGS':
      return {
        ...state,
        top_songs: []
      }
    case 'ACTION_GET_POPULERS':
      return {
        ...state,
        populers: action.data
      }
    case 'ERROR_GET_POPULERS':
      return {
        ...state,
        populers: []
      }
    case 'ACTION_FAVORITE_SONG_DONE':
      return {
        ...state,
        isSaveFavorite: true
      }
    case 'ERROR_FAVORITE_SONG':
      return {
        ...state,
        isSaveFavorite: false
      }
    case 'ACTION_SET_RATE_SONG':
      return {
        ...state,
      }
    case 'ERROR_SET_RATE_SONG':
      return {
        ...state,
      }
    default:
      return {
        ...state,
        isCreatedSong: false,
        isSaveFavorite: false
      }
  }
}