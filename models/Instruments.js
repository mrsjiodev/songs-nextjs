const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const InstrumentSchema = new Schema({
  title: { type: String },
  name: { type: String },
  image: {type: Object},
}, {
  timestamps: true
})

module.exports = mongoose.models.Instruments || mongoose.model('Instruments', InstrumentSchema);