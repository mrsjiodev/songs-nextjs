// components
import PlanItem from "../dashboard/PlanItem";
// ui
import { Row, Col } from "antd";

const OurPricing = props => {
  const { pageMain } = props;

  return (
    <div className="our-pricing">
      <h1>Our Pricing</h1>
      <h5>
        Gain access to premium and exclusive songs, soundscapes, and sound
        effects curated and created by top musicians and audio engineers
      </h5>

      <div className="plans">
        <Row gutter={36}>
          <Col md={12}>
            <PlanItem
              type="basic"
              user={props.user}
              page={pageMain === "plans" ? null : "welcome"}
            />
          </Col>
          <Col md={12}>
            <PlanItem
              type="pro"
              user={props.user}
              page={pageMain === "plans" ? null : "welcome"}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default OurPricing;
