import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { getProfile, logout } from "../redux/actions/users";
import { allAnswersAndQuestions } from "../redux/actions/answers";
// components
import HeaderWelcome from "../components/home/HeaderWelcome";
import Banner from "../components/home/Banner";
import OurPricing from "../components/home/OurPricing";
import Subcrtiption from "../components/home/Subcrtiption";
import ListenNow from "../components/home/ListenNow";
import Listening from "../components/home/Listening";
import FooterWelcome from "../components/home/FooterWelcome";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDrawer: false
    };
  }

  componentDidMount = async () => {
    this.props.allAnswersAndQuestions();
    if (!this.props.user) {
      this.props.getProfile();
    }
  };

  onLogout = async () => {
    await this.props.logout();
    await Router.push("/auth/login");
  };

  showDrawer = () => {
    this.setState({ isDrawer: !this.state.isDrawer });
  };

  onClose = () => {
    this.setState({ isDrawer: false });
  };

  render() {
    const { isDrawer } = this.state;
    const { user } = this.props;

    return (
      <div className="landing-page">
        <div className="top-content">
          <HeaderWelcome
            isDrawer={isDrawer}
            showDrawer={this.showDrawer}
            onLogout={this.onLogout}
            onClose={this.onClose}
            user={user}
          />
          <Banner />
        </div>

        <OurPricing />
        <Subcrtiption answers_questions={this.props.answers_questions} />
        <ListenNow />
        <Listening />
        <FooterWelcome />
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user,
    answers_questions: store.answers.answers_questions
  };
};

const mapDispatchToProps = {
  getProfile,
  logout,
  allAnswersAndQuestions
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
