import { Table, Input, Button, Popconfirm, Form, Layout, Modal } from "antd";
import React from "react";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { connect } from "react-redux";
import moment from "moment";
import Router from "next/router";
// actions
import {
  getAllInstruments,
  deleteInstrument,
  updateInstrument,
} from "../../../redux/actions/admin/instruments";
import { createInstrument } from "../../../redux/actions/instruments";

const { Content } = Layout;

class Instrument extends React.Component {
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null,
    sortedInfo: null,
    dataSource: [],
    count: 2,
    isShowModalAdd: false,
    selectedRowKeys: [],
  };

  deleteAll = () => {
    this.state.selectedRowKeys.map(async (item, index) => {
      await this.props.deleteInstrument(item);
    });
    this.getDataFromAPI();
    this.setState({ selectedRowKeys: [] });
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleDelete = async (key) => {
    await this.props.deleteInstrument(key);
    this.getDataFromAPI();
  };

  handleAdd = async () => {
    this.setState({
      isShowModalAdd: true,
    });
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  getDataFromAPI = async () => {
    await this.props.getAllInstruments();
    this.setState({ dataSource: this.props.instruments });
    const { dataSource } = this.state;
    dataSource.map((item, index) => {
      item.key = item._id;
    });
    this.setState({ dataSource });
  };

  onCancelDelete = () => {
    this.setState({ selectedRowKeys: [] });
  };

  componentDidMount = async () => {
    if (localStorage.getItem("jwt_token_admin")) {
      this.getDataFromAPI();
    } else {
      Router.push("/admin");
    }
  };

  onCancel = () => {
    this.setState({ isShowModalAdd: false, isShowModalEdit: false });
  };
  handleFinish = async (values) => {
    console.log({ values });
    this.props.createInstrument(values);
    this.getDataFromAPI();
    this.setState({ isShowModalAdd: false });
  };

  edit = async (item) => {
    await this.setState({ title: item.title, id: item._id, name: item.name });
    await this.setState({
      isShowModalEdit: true,
    });
  };

  handleEdit = async () => {
    const value = {
      title: this.state.title,
      // name: this.state.name,
    };
    await this.props.updateInstrument(this.state.id, value);
    await this.getDataFromAPI();
    this.setState({
      isShowModalEdit: false,
    });
  };

  onChangeTitle = (e) => {
    this.setState({ title: e.target.value });
  };

  // onChangeName = (e) => {
  //   this.setState({ name: e.target.value });
  // };

  render() {
    let { sortedInfo, filteredInfo, selectedRowKeys } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
        sorter: (a, b) => a.title.localeCompare(b.title),
        sortOrder: sortedInfo.columnKey === "title" && sortedInfo.order,
        ...this.getColumnSearchProps("title"),
      },
      // {
      //   title: "Name",
      //   dataIndex: "name",
      //   key: "name",
      //   sorter: (a, b) => a.name.localeCompare(b.name),
      //   sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
      //   ellipsis: true,
      //   ...this.getColumnSearchProps("name"),
      // },
      {
        title: "Updated",
        dataIndex: "updatedAt",
        key: "updatedAt",
        sorter: (a, b) =>
          moment(a.updatedAt).unix() - moment(b.updatedAt).unix(),
        sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
        ...this.getColumnSearchProps("updatedAt"),
      },
      {
        title: "Action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <a
              onClick={() => {
                this.edit(record);
              }}
              style={{ marginRight: 15 }}
            >
              Edit
            </a>
            {this.state.dataSource.length >= 1 ? (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            ) : null}
          </span>
        ),
      },
    ];

    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <div>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                  }}
                >
                  Add Instrument
                </Button>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalEdit}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleEdit}
                  >
                    <h1>Edit Instrument</h1>
                    <Input
                      placeholder="title"
                      value={this.state.title}
                      onChange={this.onChangeTitle}
                      style={{ marginBottom: 10 }}
                    />
                    {/* <Input
                      placeholder="name"
                      value={this.state.name}
                      onChange={this.onChangeName}
                    /> */}

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                        style={{ marginTop: 20 }}
                      >
                        Edit
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalAdd}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleFinish}
                  >
                    <h1>Create Instrument</h1>
                    <Form.Item
                      name="title"
                      rules={[
                        {
                          required: true,
                          message: "Please input your title!",
                        },
                      ]}
                    >
                      <Input placeholder="Title" />
                    </Form.Item>
                    <Form.Item
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Please input your name!",
                        },
                      ]}
                    >
                      <Input placeholder="Name" />
                    </Form.Item>

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                      >
                        Create
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <div style={{ marginBottom: 16 }}>
                  <Popconfirm
                    disabled={!hasSelected}
                    title="Sure to delete?"
                    onConfirm={this.deleteAll}
                    onCancel={this.onCancelDelete}
                  >
                    <Button type="primary" disabled={!hasSelected}>
                      Delete All
                    </Button>
                  </Popconfirm>
                  <span style={{ marginLeft: 8 }}>
                    {hasSelected
                      ? `Selected ${selectedRowKeys.length} instruments`
                      : ""}
                  </span>
                </div>
                <Table
                  rowSelection={rowSelection}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "50", "100"],
                  }}
                  bordered
                  dataSource={this.state.dataSource}
                  onChange={this.handleChange}
                />
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    instruments: store.adminInstrument.instruments,
  };
};

const mapDispatchToProps = {
  getAllInstruments,
  deleteInstrument,
  updateInstrument,
  createInstrument,
};

export default connect(mapStateToProps, mapDispatchToProps)(Instrument);
