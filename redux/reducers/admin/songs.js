const initData = {
  all_songs: [],
  message_delete: {},
  song: null,
  isCreatedSong: false,
  isError: false,
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_SONGS_CREATED_ADMIN':
      return {
        ...state,
        song: action.data,
        isCreatedSong: true
      }
    case 'ERROR_SONGS_CREATED_ADMIN':
      return {
        ...state,
        song: null,
        isCreatedSong: false,
        isError: true
      }
    case 'ACTION_GET_ALL_SONGS':
      return {
        ...state,
        all_songs: action.data
      }
    case 'ERROR_GET_ALL_SONGS':
      return {
        ...state,
        all_songs: []
      } 
    case 'ACTION_DELETE_SONG':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}