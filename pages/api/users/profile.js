import connectDb from '../../../middleware/dbMiddleware'
import uploadMulterS3 from '../../../middleware/uploadMulterS3';
const nextConnect = require('next-connect')
const Users = require('../../../models/Users');
const bcrypt = require('bcrypt');
const handler = nextConnect();
const checkToken = require('../../../middleware/checkLogined');
const saltRounds = 10;

export const config = {
  api: {
    bodyParser: false,
  },
}

handler.use(checkToken);

handler.get(async (req, res) => {
  const { _id } = req.user;

  let user = await Users.findById(_id).exec();
  user = user.toObject();

  delete user.password;

  res.status(200).json(user);
})

handler.put(uploadMulterS3.single('avatar'), async (req, res) => {
  try {
    const { first_name, last_name, company_name, password } = req.body;

    const { _id } = req.user;

    bcrypt.genSalt(saltRounds, async (err, salt) => {
      if (err) {
        res.status(400).json({ msg: 'GenSalt password failed' })
      }

      bcrypt.hash(password, salt, async (err, hash) => {
        if (err) {
          res.status(400).json({ msg: 'hash password failed' })
        }

        try {
          let user = await Users.findByIdAndUpdate(_id, {
            first_name, last_name, company_name, password: hash,
          }).exec();

          if(req.file){
            user.avatar = req.file;
            await user.save();
          }

          user = user.toObject();

          delete user.password;

          res.status(200).json(user);
        } catch (error) {
          console.log(error)
          res.status(400).json({ msg: 'update profile failed' });
        }
      });
    });
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: 'bcrypt failed' })
  }

})

export default connectDb(handler);