import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Genres = require("../../../../../models/Genres");
const Songs = require("../../../../../models/Songs");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Genres.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Genres Not found" });
    else {
      await Songs.deleteMany({ genre: id });
      return res.status(200).json({ message: "Delete genres successfully" });
    }
  });
});

export default connectDb(handler);
