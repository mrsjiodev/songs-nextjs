import connectDb from '../../../middleware/dbMiddleware'
import nextConnect from 'next-connect';

const Songs = require('../../../models/Songs');

export const config = {
  api: {
    bodyParser: false,
  },
}

const handler = nextConnect();

handler.get(async (req, res, next) => {
  let populers = await Songs.find({}).populate('mood').populate('instrument').populate('genre').populate('theme').sort({ ranking: -1 }).exec();

  res.status(200).json(populers)
})

export default connectDb(handler);