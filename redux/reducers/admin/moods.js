const initData = {
  moods: [],
  message_delete: {}
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_GET_ALL_MOODS':
      return {
        ...state,
        moods: action.data
      } 
      case 'ACTION_DELETE_MOODS':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}