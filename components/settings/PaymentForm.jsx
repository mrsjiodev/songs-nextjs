import React, { useState, useEffect } from "react";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { connect } from "react-redux";
// import PaypalExpressBtn from "react-paypal-express-checkout";
import { getCountries, getStates, getCountry } from "country-state-picker";
// actions
import {
  createCustomer,
  createSubscription,
  checkDiscount,
  updateCustomer,
  changeBillingAddress,
  changePaymentMethod
} from "../../redux/actions/payments";
import { getProfile } from "../../redux/actions/users";
// stripe
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
// components
import PaypalCheckout from "../../components/payments/PaypalCheckout";
// ui
import { Form, Input, Row, Col, Button, message, Select } from "antd";
import { CreditCardOutlined } from "@ant-design/icons";

const { Option } = Select;

const PaymentForm = props => {
  const [form] = Form.useForm();
  const [isDiscount, setDiscount] = useState(false);
  const [isValid, setValid] = useState(false);
  const [valueDiscount, setValueDiscount] = useState("");
  const [statusDiscount, setStatusDiscount] = useState("");
  const stripe = useStripe();
  const elements = useElements();
  const [payment_method, setPaymentMethod] = useState("credit_card");
  const router = useRouter();
  const { slug } = router.query;
  const [states, setStates] = useState(getStates("al"));
  const countries = getCountries();
  const { isShowUpdatePayment, isShowUpdateAddress } = props;

  console.log(props);

  useEffect(() => {
    if (props.payment_intent) {
      handleSubscription(props.payment_intent);
      props.onLoading(false);
      onReset();
    }
  }, [props.payment_intent]);

  useEffect(() => {
    if (props.isErr) {
      props.onLoading(false);
      message.error(props.msgErr);
    }
  }, [props.msgErr, props.isErr]);

  useEffect(() => {
    if (isDiscount) {
      props.isValidDiscount
        ? message.success("Added discount code successfully")
        : message.error("Added discount code error");

      setValid(props.isValidDiscount);
    }
  }, [props.isValidDiscount]);

  const onCheckDiscount = async discount_id => {
    form.isFieldValidating("discount");
    await props.checkDiscount({ discount_id });
    let checked = (await props.isValidDiscount) ? "success" : "error";

    setValueDiscount(discount_id);
    setStatusDiscount(checked);
  };

  const onFinish = async values => {
    await props.onLoading(true);
    console.log(values);

    if (isShowUpdatePayment) {
      const result = await stripe.createPaymentMethod({
        type: "card",
        card: elements.getElement(CardElement)
      });

      await props.changePaymentMethod({ payment_id: result.paymentMethod.id });

      (await props.isChangePaymentMethod) === true
        ? message.success("Updated payment method successfully!")
        : message.error("Updated payment method failed!");

      (await props.isChangePaymentMethod) && props.getProfile();

      (await props.isChangePaymentMethod) &&
        props.onShowUpdatePaymentCard(false);

      // (await props.isChangePaymentMethod)
    }

    if (isShowUpdateAddress) {
      let data = {
        city: values.city,
        line1: values.address,
        country: values.country,
        line2: "",
        postal_code: values.postal_code,
        state: values.state,
        fullname: values.name
      };

      await props.changeBillingAddress(data);
      (await props.isChangeBillingAddress) === true
        ? message.success("Updated billing address successfully!")
        : message.error("Updated failed");

      (await props.isChangeBillingAddress) && props.getProfile();

      (await props.isChangeBillingAddress) &&
        props.onShowUpdateAddressBilling(false);
    }
  };

  const handleStripePaymentMethod = async (result, values, card_info) => {
    if (result.error) {
      message.error(result.error.message);
    } else {
      // check customer stripe
      if (!props.user.customer_stripe_id) {
        // create customer & subscription
        await props.createCustomer({
          payment_method_id: result.paymentMethod.id,
          city: values.city,
          address: values.address,
          state: values.state,
          country: values.country,
          full_name: values.name,
          postal_code: values.zip
        });

        await props.getProfile();
      } else {
        // await props.updateCustomer({
        //   payment_method_id: result.paymentMethod.id,
        //   city: values.city,
        //   address: values.address,
        //   state: values.state,
        //   country: values.country,
        //   full_name: values.name
        // });
      }

      // create subscription
      await props.createSubscription({
        plan_id: props.plan_id,
        discount_id: valueDiscount,
        type_standard: slug,
        interval: props.type,
        billing_details: {
          address: {
            city: values.city,
            line1: values.address,
            postal_code: values.zip,
            state: values.state,
            country: values.country
          },
          email: props.user.email,
          name: values.name,
          phone: "555-555-5555"
        }
        // card_info: elements.getElement(CardElement)
      });
    }
  };

  const handleSubscription = async payment_intent => {
    if (payment_intent) {
      const { client_secret, status, amount } = payment_intent;

      if (status === "requires_action") {
        stripe.confirmCardPayment(client_secret).then(confirm => {
          if (confirm.error) {
            message.error(confirm.error.message);
          }
        });
      }

      message.success(`You have successfully paid $${amount / 100}`);
      await Router.push("/plans");
    } else {
      message.warning("No payment information received");
    }
  };

  const onReset = () => {
    form.resetFields();
    setDiscount(false);
    setStatusDiscount("");
    setValueDiscount("");
    setValid(false);
  };

  const cardOptions = {
    iconStyle: "solid",
    style: {
      base: {
        iconColor: "#1890ff",
        color: "rgba(0, 0, 0, 0.65)",
        fontWeight: 500,
        fontFamily: '"Poppins", sans-serif',
        fontSize: "16px",
        fontSmoothing: "antialiased",
        lineHeight: "31px",
        ":-webkit-autofill": { color: "#fce883" },
        "::placeholder": { color: "#d0d7dd" },
        background: "#FFFFFF",
        border: "1px solid #D0D7DD",
        boxSizing: "border-box",
        borderRadius: "4px"
      },
      invalid: {
        iconColor: "#eb1c26",
        color: "#eb1c26"
      }
    }
  };

  // country
  const onChangeCountry = value => {
    let _states = getStates(value);

    setStates(getStates(value));
  };

  const onBlurCountry = () => {};

  const onFocusCountry = () => {};

  const onSearchCountry = val => {};

  // state
  const onChangeState = value => {};

  const onBlurState = () => {};

  const onFocusState = () => {};

  const onSearchState = val => {};

  return (
    <Form onFinish={onFinish} form={form} className="checkout-form">
      <Row gutter={24}>
        {/* <PaypalExpressBtn
          client={{
            sandbox: 'sb-4y6gd1521416@business.example.com',
            production:
              "EOwXmO2xcsXE0XfV1NkgZSbBJ2-f3IHTGYUIVbg_jZvvmb1gucxJLqisVpZII_2S7kVNoSwPGRmn7SGs"
            // production: process.env.PAYPAL_CLIENT_ID,
          }}
          currency={"USD"}
          total={1.0}
        /> */}

        {isShowUpdatePayment && (
          <Col md={24}>
            <Form.Item
              name="card"
              rules={[{ required: true, message: "Please input your Card!" }]}
            >
              <CardElement options={cardOptions} />
            </Form.Item>
          </Col>
        )}

        {isShowUpdateAddress && (
          <>
            <Col md={24}>
              <h3 className="title-form">Billing Address</h3>
            </Col>

            <Col md={24}>
              <Form.Item
                name="name"
                rules={[
                  { required: true, message: "Please input your Full Name!" }
                ]}
              >
                <Input placeholder="Full Name" />
              </Form.Item>
            </Col>

            <Col md={24}>
              <Form.Item
                name="address"
                rules={[
                  { required: true, message: "Please input your Address!" }
                ]}
              >
                <Input placeholder="Street Address" />
              </Form.Item>
            </Col>

            <Col md={10}>
              <Form.Item
                name="city"
                rules={[{ required: true, message: "Please input your City!" }]}
              >
                <Input placeholder="City" />
              </Form.Item>
            </Col>

            <Col md={14}>
              <Form.Item
                name="country"
                rules={[
                  { required: true, message: "Please select your Country!" }
                ]}
              >
                {/* <Input placeholder="Country" /> */}

                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Country"
                  optionFilterProp="children"
                  onChange={onChangeCountry}
                  onFocus={onFocusCountry}
                  onBlur={onBlurCountry}
                  onSearch={onSearchCountry}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {countries &&
                    countries.map((item, i) => (
                      <Option key={i} value={item.code}>
                        {item.name}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>

            <Col md={14}>
              <Form.Item
                name="state"
                rules={[
                  { required: true, message: "Please select your State!" }
                ]}
              >
                {/* <Input placeholder="State" /> */}
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a State"
                  optionFilterProp="children"
                  onChange={onChangeState}
                  onFocus={onFocusState}
                  onBlur={onBlurState}
                  onSearch={onSearchState}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {states &&
                    states.map((item, i) => (
                      <Option key={i} value={item}>
                        {item}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>

            <Col md={10}>
              <Form.Item
                name="zip"
                rules={[
                  { required: true, message: "Please input your Zipcode!" }
                ]}
              >
                <Input placeholder="Zipcode" />
              </Form.Item>
            </Col>
          </>
        )}
      </Row>

      <Button
        // loading={isLoading}
        type="primary"
        htmlType="submit"
        className="checkout-button"
        disabled={!stripe}
      >
        Save
      </Button>
    </Form>
  );
};

const mapStateToProps = store => {
  console.log(store.payments);
  return {
    isCreated: store.payments.isCreated,
    isSubscription: store.payments.isSubscription,
    customer: store.payments.customer,
    subscription: store.payments.subscription,
    latest_invoice: store.payments.latest_invoice,
    payment_intent: store.payments.payment_intent,
    isChangeBillingAddress: store.payments.isChangeBillingAddress,
    isChangePaymentMethod: store.payments.isChangePaymentMethod,
    // plans
    plans: store.payments.plans,
    isValidDiscount: store.payments.isValidDiscount,
    discountInfo: store.payments.discountInfo,
    msgErr: store.payments.msgErr,
    isErr: store.payments.isErr,
    // users
    user: store.users.user
  };
};

const mapDispatchToProps = {
  // payments
  createCustomer,
  createSubscription,
  checkDiscount,
  updateCustomer,
  changeBillingAddress,
  changePaymentMethod,
  // users
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentForm);
