
import API from '../config/request'
import request from '../config/request';


export function confirmEmail(data){
  return dispatch => {
    return request().post(`auth/confirm-email`, data).then(res => {
      dispatch({
        type: "ACTION_CONFIRM_EMAIL",
        msg: res.data.msg 
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_CONFIRM_EMAIL',
        msg: err.data.msg
      })
    })
  }
}

export function checkToken(data) {
  return dispatch => {
    return request().post(`auth/reset-password/check-token`, data).then(res => {
      dispatch({
        type: "ACTION_CHECKED_TOKEN",
        msg: res.data.msg 
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CHECKED_TOKEN",
        msg: err.data.msg
      })
    })
  }
}

export function newPassword(data){
  return dispatch => {
    return request().post(`auth/reset-password/new-password`, data).then(res => {
      dispatch({
        type: "ACTION_NEW_PASSWORD",
        msg: res.data.msg 
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_NEW_PASSWORD",
        msg: err.response.msg
      })
    })
  }
}

export default {
  confirmEmail,
  checkToken,
  newPassword,
}