import request from '../../config/request';

export function getAllThemes() {
  return dispatch => {
    return request().get('admin/themes/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_THEMES",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteTheme(id) {
  return dispatch => {
    return request().delete(`admin/themes/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_THEMES",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateTheme(id,data) {
  return dispatch => {
    return request().put(`admin/themes/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllThemes,
  deleteTheme,
  updateTheme
}