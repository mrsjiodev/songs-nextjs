const initData = {
  supports: [],
  isCreatedSupport: false,
};

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_GET_SUPPORTS":
      return {
        ...state,
        supports: action.data,
      };
    case "ACTION_CREATED_SUPPORT_ADMIN":
      return {
        ...state,
        isCreatedSupport: true,
      };
    case "ERROR_CREATED_SUPPORT_ADMIN":
      return {
        ...state,
        isCreatedSupport: false,
      };
    default:
      return {
        ...state,
      };
  }
}
