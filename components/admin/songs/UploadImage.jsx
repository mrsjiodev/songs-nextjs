import React from "react";
import { Upload, message, Button, Spin } from "antd";
import { UploadOutlined } from "@ant-design/icons";

const { Dragger } = Upload;

class UploadImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { imageUrl, isUploadingImage } = this.props;

    return (
      <Spin spinning={isUploadingImage}>
        <Dragger
          name="audio"
          // disabled={dataFile}
          accept="image/*"
          showUploadList={false}
          multiple={false}
          // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          onChange={info => this.props.onChangeFile(info, "image")}
          onRemove={info => this.props.onRemoveFile(info, "image")}
          beforeUpload={file => this.props.beforeUpload(file)}
        >
          {/* <div className="choose-file">
          <Button type="default">Choose File</Button> <span>No file chosen</span>
        </div> */}

          {imageUrl ? (
            <img src={imageUrl} alt="avatar" style={{ width: "100%", height: 300 }} />
          ) : (
            <>
              <p className="ant-upload-drag-icon">
                <UploadOutlined />
              </p>
              <p className="ant-upload-text">Click & Upload Image</p>
            </>
          )}
        </Dragger>
      </Spin>
    );
  }
}

export default UploadImage;
