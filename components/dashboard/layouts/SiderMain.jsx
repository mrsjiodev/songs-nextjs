// packages
import { URL_MEDIA } from "../../../config/index";
import Link from "next/link";
// ui
import { Layout, Menu, Button } from "antd";
import {
  UploadOutlined,
  HomeOutlined,
  QuestionCircleOutlined,
  PlusOutlined
} from "@ant-design/icons";

const { Sider } = Layout;

const SiderMain = props => {
  return (
    <Sider
      className="sider-main"
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={broken => {
        // console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}
    >
      <div className="ctn">
        <div className="btn-upload">
          <Button type="primary" icon={<UploadOutlined />}>
            Upload
          </Button>
        </div>

        <h3 className="sider-title">Browse Music</h3>

        <Menu theme="dark" mode="inline" defaultSelectedKeys={["4"]}>
          <Menu.Item key="1">
            <Link href="/">
              <a>
                <HomeOutlined />
                <span className="nav-text">Home</span>
              </a>
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link href="/blog">
              <a>
                <div className="sider-bar-icon">
                  <img src={`${URL_MEDIA}/images/icons/blogger.png`} alt="" />
                </div>
                <span className="nav-text">Blog</span>
              </a>
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link href="/support">
              <a>
                <QuestionCircleOutlined />
                <span className="nav-text">Support</span>
              </a>
            </Link>
          </Menu.Item>
        </Menu>

        <h3 className="sider-title">
          My Lists{" "}
          <a href="#" className="btn-add">
            <PlusOutlined />
          </a>
        </h3>
      </div>
    </Sider>
  );
};

export default SiderMain;
