const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MoodSchema = new Schema({
  title: { type: String },
  name: { type: String },
  image: { type: Object }
}, {
  timestamps: true,
});

module.exports = mongoose.models.Moods || mongoose.model('Moods', MoodSchema);
