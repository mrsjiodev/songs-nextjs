const initData = {
  themes: [],
  message_delete: {}
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_GET_ALL_THEMES':
      return {
        ...state,
        themes: action.data
      } 
      case 'ACTION_DELETE_THEMES':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}