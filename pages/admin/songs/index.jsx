import {
  Table,
  Input,
  Button,
  Popconfirm,
  Layout,
  Modal,
  Form,
  Row,
  Col,
  InputNumber,
  Upload,
} from "antd";
import React from "react";
import Router from "next/router";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import CreateSong from "../../../components/admin/songs/CreateSong";
import { connect } from "react-redux";
import moment from "moment";
import {
  getAllSongs,
  deleteSong,
  updateSong,
} from "../../../redux/actions/admin/songs";
import { getAllCategories } from "../../../redux/actions/categories";
import { allMoods } from "../../../redux/actions/moods";
import { allThemes } from "../../../redux/actions/themes";
import { allGenres } from "../../../redux/actions/genres";
import { allInstruments } from "../../../redux/actions/instruments";
import Categories from "../../../components/admin/songs/Categories";
const { Content } = Layout;
const URL_MEDIA = process.env.CLOUD_FRONT_URL;
class Song extends React.Component {
  constructor(props) {
    super(props);
    this.formRefs = React.createRef();
  }
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null,
    sortedInfo: null,
    dataSource: [],
    count: 2,
    isShowModalAdd: false,
    selectedRowKeys: [],
    imageUrl: null,
  };

  deleteAll = () => {
    this.state.selectedRowKeys.map(async (item, index) => {
      await this.props.deleteSong(item);
    });
    this.getDataFromAPI();
    this.setState({ selectedRowKeys: [] });
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleDelete = async (key) => {
    await this.props.deleteSong(key);
    this.getDataFromAPI();
  };

  handleAdd = async () => {
    this.setState({
      isShowModalAdd: true,
    });
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={(node) => {
              this.searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() =>
              this.handleSearch(selectedKeys, confirm, dataIndex)
            }
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
          text
        ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  getDataFromAPI = async () => {
    await this.props.getAllSongs();
    this.setState({ dataSource: this.props.all_songs });
    const { dataSource } = this.state;
    dataSource.map((item, index) => {
      item.key = item._id;
    });
    this.setState({ dataSource });
  };

  componentDidMount = async () => {
    if (localStorage.getItem("jwt_token_admin")) {
      await this.getDataFromAPI();
      await this.props.getAllCategories();
      await this.props.getAllCategories();
      await this.props.allInstruments();
      await this.props.allGenres();
      await this.props.allMoods();
      await this.props.allThemes();
    } else {
      Router.push("/admin");
    }
  };

  onCancel = () => {
    this.setState({ isShowModalAdd: false, isShowModalEdit: false });
  };

  getMessage = (message) => {
    if (message) {
      this.setState({ isShowModalAdd: false });
      this.getDataFromAPI();
    }
  };

  edit = async (item) => {
    await this.setState({
      title: item.track_title,
      id: item._id,
      artist: item.artist_name,
      instrument: item.instrument._id,
      mood: item.mood._id,
      theme: item.theme._id,
      genre: item.genre._id,
      ranking: item.ranking,
    });
    await this.setState({
      isShowModalEdit: true,
    });
    // this.setState({ imageUrl: `${URL_MEDIA}${item.image.key}` });
  };

  handleEdit = async () => {
    let fd = new FormData();
    fd.append("track_title", this.state.title);
    fd.append("artist_name", this.state.artist);
    fd.append("theme", this.state.theme);
    fd.append("genre", this.state.genre);
    fd.append("instrument", this.state.instrument);
    fd.append("mood", this.state.mood);
    fd.append("ranking", this.state.ranking);
    // if (this.state.file) {
    //   fd.append("image", this.state.file.originFileObj);
    // }
    await this.props.updateSong(this.state.id, fd);
    await this.getDataFromAPI();
    this.setState({
      isShowModalEdit: false,
      // imageUrl: null,
      // file: null,
    });
  };

  onChangeTitle = (e) => {
    this.setState({ title: e.target.value });
  };

  onCancelDelete = () => {
    this.setState({ selectedRowKeys: [] });
  };

  onChangeArtist = (e) => {
    this.setState({ artist: e.target.value });
  };

  onChangeRanking = (value) => {
    this.setState({ ranking: value });
  };

  onChangeFilters = async (value, name) => {
    this.setState({ [name]: value });
  };

  onChangeImage = async (info, id) => {
    if (info.file.status !== "uploading") {
      this.setState({ loading: true });
    }
    if (info.file.status === "done") {
      this.getBase64Edit(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
          file: info.file,
        })
      );
      let fd = new FormData();
      fd.append("image", info.file.originFileObj);
      await this.props.updateSong(id, fd);
      await this.getDataFromAPI();
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  getBase64Edit(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUploadEdit(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  render() {
    let { sortedInfo, filteredInfo, selectedRowKeys, imageUrl } = this.state;
    const { instruments, moods, themes, genres } = this.props;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "Track Title",
        dataIndex: "track_title",
        key: "track_title",
        sorter: (a, b) => a.track_title.localeCompare(b.track_title),
        sortOrder: sortedInfo.columnKey === "track_title" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("track_title"),
      },
      {
        title: "Artist Name",
        dataIndex: "artist_name",
        key: "artist_name",
        sorter: (a, b) => a.artist_name.localeCompare(b.artist_name),
        sortOrder: sortedInfo.columnKey === "artist_name" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("artist_name"),
      },
      // {
      //   title: "Duration (minutes)",
      //   dataIndex: "duration",
      //   key: "duration",
      //   render: (duration) => `${(duration / 60).toFixed(2)}`,
      //   sorter: (a, b) => a.duration - b.duration,
      //   sortOrder: sortedInfo.columnKey === "duration" && sortedInfo.order,
      //   // ...this.getColumnSearchProps("duration"),
      //   // editable: true,
      // },
      {
        title: "Mood",
        dataIndex: "mood",
        key: "mood",
        render: (mood) => `${mood.title}`,
        sorter: (a, b) => `${a.mood.title}`.localeCompare(`${b.mood.title}`),
        sortOrder: sortedInfo.columnKey === "mood" && sortedInfo.order,
      },
      {
        title: "Theme",
        dataIndex: "theme",
        key: "theme",
        render: (theme) => `${theme.title}`,
        sorter: (a, b) => `${a.theme.title}`.localeCompare(`${b.theme.title}`),
        sortOrder: sortedInfo.columnKey === "theme" && sortedInfo.order,
      },
      {
        title: "Genre",
        dataIndex: "genre",
        key: "genre",
        render: (genre) => `${genre.title}`,
        sorter: (a, b) => `${a.genre.title}`.localeCompare(`${b.genre.title}`),
        sortOrder: sortedInfo.columnKey === "genre" && sortedInfo.order,
      },
      {
        title: "Instrument",
        dataIndex: "instrument",
        key: "instrument",
        render: (instrument) => `${instrument.title}`,
        sorter: (a, b) =>
          `${a.instrument.title}`.localeCompare(`${b.instrument.title}`),
        sortOrder: sortedInfo.columnKey === "instrument" && sortedInfo.order,
      },
      {
        title: "Image",
        dataIndex: "image",
        key: "image",
        sorter: true,
        // ellipsis: true,
        render: (image, record) => (
          // image && (
          //   <img
          //     style={{ width: 100, height: 100 }}
          //     src={`${URL_MEDIA}${image.key || ""}`}
          //     alt=""
          //   />
          // ),
          <Upload
            className="btn-upload-image"
            accept="image/png, imgage/jpg, image/jpeg"
            name="image"
            onChange={(info) => this.onChangeImage(info, record._id)}
            beforeUpload={this.beforeUploadEdit}
            showUploadList={false}
          >
            {record && record.image ? (
              <img
                title="Click to Change image of song"
                style={{ width: 90, height: 90, cursor: "pointer" }}
                src={`${URL_MEDIA}${record.image.key || ""}`}
                alt=""
              />
            ) : (
                <div>
                  <Button>Upload</Button>
                </div>
              )}
          </Upload>
        ),
      },
      // {
      //   title: "Image",
      //   dataIndex: "image",
      //   key: "image",
      //   sorter: true,
      //   render: (image) => (
      //     <img
      //       style={{ width: 100 }}
      //       src={`${URL_MEDIA}${image.key || ""}`}
      //       alt=""
      //     />
      //   ),
      // },
      // {
      //   title: "Audio",
      //   dataIndex: "audio",
      //   key: "audio",
      //   render: (audio) => `${audio.originalname}`,
      //   sorter: true,
      // },
      // {
      //   title: "Create by",
      //   dataIndex: "created_by",
      //   key: "created_by",
      //   render: (created_by) =>
      //     created_by &&
      //     created_by.first_name &&
      //     created_by.last_name &&
      //     `${created_by.first_name} ${created_by.last_name}`,
      //   sorter: (a, b) =>
      //     created_by &&
      //     created_by.first_name &&
      //     created_by.last_name &&
      //     `${a.created_by.first_name} ${a.created_by.last_name}`.localeCompare(
      //       `${b.created_by.first_name} ${b.created_by.last_name}`
      //     ),
      //   sortOrder: sortedInfo.columnKey === "created_by" && sortedInfo.order,
      // },
      {
        title: "Ranking",
        dataIndex: "ranking",
        key: "ranking",
        sorter: (a, b) => a.ranking - b.ranking,
        sortOrder: sortedInfo.columnKey === "ranking" && sortedInfo.order,
        ...this.getColumnSearchProps("ranking"),
        ellipsis: true,
      },

      {
        title: "Updated",
        dataIndex: "updatedAt",
        key: "updatedAt",
        sorter: (a, b) =>
          moment(a.updatedAt).unix() - moment(b.updatedAt).unix(),
        sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
        ...this.getColumnSearchProps("updatedAt"),
      },
      {
        title: "Action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <a
              onClick={() => {
                this.edit(record);
              }}
              style={{ marginRight: 15 }}
            >
              Edit
            </a>
            {this.state.dataSource.length >= 1 ? (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            ) : null}
          </span>
        ),
      },
    ];

    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <div>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                  }}
                >
                  Add Song
                </Button>
                <Modal
                  className="modal-song custom-modal-admin"
                  visible={this.state.isShowModalEdit}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    ref={this.formRefs}
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleEdit}
                  >
                    <h1>Edit Song</h1>
                    {/* <Form.Item name="image">
                      <div className="st-image" style={{ textAlign: "center" }}>
                        <Upload
                          className="btn-upload-image"
                          accept="image/png, imgage/jpg, image/jpeg"
                          name="image"
                          onChange={(info) => this.onChangeImage(info)}
                          beforeUpload={this.beforeUploadEdit}
                          showUploadList={false}
                        >
                          {this.state.imageBefore ? (
                            <img
                              src={imageUrl}
                              alt=""
                              style={{ cursor: "pointer" }}
                              title="Change image of song"
                            />
                          ) : (
                            <div>
                              <Button>Upload image</Button>
                            </div>
                          )}
                        </Upload>
                      </div>
                    </Form.Item> */}
                    <Row gutter={24}>
                      <Col lg={10} md={24} sm={24} xs={24}>
                        <Input
                          placeholder="title"
                          value={this.state.title}
                          onChange={this.onChangeTitle}
                          style={{ marginBottom: 10 }}
                        />
                      </Col>
                      <Col lg={10} md={24} sm={24} xs={24}>
                        <Input
                          placeholder="artist"
                          value={this.state.artist}
                          onChange={this.onChangeArtist}
                        />
                      </Col>
                      <Col lg={4} md={24} sm={24} xs={24}>
                        <InputNumber
                          placeholder="Ranking"
                          style={{ marginBottom: 10 }}
                          min={0}
                          max={10000000}
                          value={this.state.ranking}
                          onChange={this.onChangeRanking}
                        />
                      </Col>
                    </Row>
                    <Categories
                      instruments={instruments}
                      moods={moods}
                      themes={themes}
                      required={false}
                      genres={genres}
                      instrumentSelected={this.state.instrument}
                      moodSelected={this.state.mood}
                      themeSelected={this.state.theme}
                      genreSelected={this.state.genre}
                      onChangeFilters={this.onChangeFilters}
                    />

                    <div className="action-sb">
                      <Button
                        style={{
                          marginTop: 20,
                        }}
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                      >
                        Edit
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <Modal
                  title={"Add Song"}
                  className="modal-song custom-modal-admin"
                  visible={this.state.isShowModalAdd}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <CreateSong passMessage={this.getMessage} />
                </Modal>
                <div style={{ marginBottom: 16 }}>
                  <Popconfirm
                    disabled={!hasSelected}
                    title="Sure to delete?"
                    onConfirm={this.deleteAll}
                    onCancel={this.onCancelDelete}
                  >
                    <Button type="primary" disabled={!hasSelected}>
                      Delete All
                    </Button>
                  </Popconfirm>
                  <span style={{ marginLeft: 8 }}>
                    {hasSelected
                      ? `Selected ${selectedRowKeys.length} songs`
                      : ""}
                  </span>
                </div>
                <Table
                  rowSelection={rowSelection}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "50", "100"],
                  }}
                  bordered
                  dataSource={this.state.dataSource}
                  onChange={this.handleChange}
                />
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    all_songs: store.adminSong.all_songs,
    isRegister: store.auth.isRegister,
    categories: store.categories.categories,
    themes: store.themes.themes,
    moods: store.moods.moods,
    genres: store.genres.genres,
    instruments: store.instruments.instruments,
  };
};

const mapDispatchToProps = {
  getAllSongs,
  deleteSong,
  updateSong,
  getAllCategories,
  allMoods,
  allGenres,
  allThemes,
  allInstruments,
};

export default connect(mapStateToProps, mapDispatchToProps)(Song);
