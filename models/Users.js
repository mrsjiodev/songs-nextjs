const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const saltRounds = 10;
const ROLES = ["admin", "user"];

const UserSchema = new Schema({
  email: { type: String, required: true, unique: true },
  role: { type: String, default: 'user', enum: ROLES },
  first_name: { type: String },
  last_name: { type: String },
  company_name: { type: String },
  avatar: { type: Object },
  password: { type: String, required: true },
  token: { type: String },
  // favorite
  favorites: [{ type: Schema.Types.ObjectId, ref: 'Songs' }],
  // payments
  amount: { type: String },
  currency: { type: String },
  interval: { type: String },
  customer_stripe_id: { type: String },
  payment_method: { type: String },
  payment_detail: { type: Object },
  discount_id: { type: String },
  plan_default: { type: Object },
  plan_info: { type: Object },
  type_standard: { type: String },
  billing_details: { type: Object },
  billing_details_default: { type: Object },
  billing_address: { type: Object },
  billing_history: { type: Object },
  payment_intent: { type: Object },
  payment_method_details: { type: Object },
  payment_method_types: { type: Object },
  current_period_start: { type: String },
  current_period_end: { type: String },
  card_info: { type: Object },
  subscription_id: { type: String },
  payment_method_default: {type: String, default: 'card'},
  // paypal
  isPaypal: { type: Boolean, default: false },
  paypal_id: { type: String },
  paypal_info: { type: Object }
}, {
  timestamps: true
});

const dfPass = bcrypt.hashSync("123123", 10);
const dataMigrate = [
  {
    email: 'admin@gmail.com',
    password: dfPass,
    role: ROLES[0]
  }
];
UserSchema.statics.getMigrateData = function () {
  return dataMigrate;
}


UserSchema.pre('save', function (next) {
  var user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(saltRounds, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function (passw, cb) {
  bcrypt.compare(passw, this.password).then(result => {
    cb(null, result);
  }).catch(err => {
    console.log("Err: ", err);
    return cb(err);
  })
};

module.exports = mongoose.models.Users || mongoose.model('Users', UserSchema);