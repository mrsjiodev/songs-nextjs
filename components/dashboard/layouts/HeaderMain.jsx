import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { getProfile } from "../../../redux/actions/users";
// ui
import { Layout, Row, Col, Input, Badge, Menu, Dropdown, Avatar } from "antd";
import { SettingOutlined, BellFilled, DownOutlined } from "@ant-design/icons";

const { Search } = Input;

class HeaderMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    await this.props.getProfile();
  };

  onLogout = () => {
    localStorage.clear();
    Router.push("/auth/login");
  };

  render() {
    const { user } = this.props;

    return (
      <div className="header">
        <div className="container">
          <Row gutter={24}>
            <Col lg={4} md={4} sm={4} xs={4}>
              <div className="logo">
                <Link href="/">
                  <span>SONGS</span>
                </Link>
              </div>
            </Col>
            <Col lg={14} md={10} sm={8} xs={8}>
              <div className="navBar">
                <Search
                  placeholder="Search for song"
                  onSearch={value => console.log(value)}
                  className="search-main"
                />
              </div>
            </Col>
            <Col lg={6} md={10} sm={12} xs={12}>
              <div className="navMainRight">
                <div className="actions">
                  <a href="#" className="btn-setting">
                    <SettingOutlined />
                  </a>

                  <Badge count={5}>
                    <a href="#">
                      <BellFilled />
                    </a>
                  </Badge>

                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item key="0">
                          <a href="#">Profile</a>
                        </Menu.Item>
                        <Menu.Item key="1">
                          <Link href="/plans">
                            <a href="#">Upgrade</a>
                          </Link>
                        </Menu.Item>
                        <Menu.Divider />
                        <Menu.Item key="3" onClick={this.onLogout}>
                          Logout
                        </Menu.Item>
                      </Menu>
                    }
                    trigger={["click"]}
                  >
                    <a
                      className="ant-dropdown-link"
                      onClick={e => e.preventDefault()}
                    >
                      <Avatar style={{ background: "#f56a00", marginRight: 5 }}>
                        {(user && user.first_name.substring(0, 1)) || "A"}
                      </Avatar>{" "}
                      {(user && user.first_name) || ""} <DownOutlined />
                    </a>
                  </Dropdown>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user
  };
};

const mapDispatchToProps = {
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMain);
