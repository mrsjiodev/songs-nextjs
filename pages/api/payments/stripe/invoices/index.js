import Stripe from "stripe";
import nextConnect from 'next-connect'
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const stripe = new Stripe(process.env.STRIPE_SECRET);
const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

hanlder.get(async (req, res) => {
  let { _id } = req.user;

  let user = await Users.findById(_id).exec();
  console.log('user: ', user.customer_stripe_id);
  if(user.customer_stripe_id){
    let invoices = await stripe.invoices.list(
      {
        customer: user.customer_stripe_id
      }
    );
  
    res.status(200).json(invoices.data)
  } else {
    res.status(200).json([])
  }
})

export default connectDB(hanlder)