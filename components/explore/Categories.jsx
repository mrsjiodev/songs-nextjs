import React, { useState } from 'react';
// ui
import { Select, Form, Row, Col } from 'antd'

const { Option } = Select;

const Categories = props => {
  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState('vertical');
  const formItemLayout = null;

  return (
    <div className="categories">
      <div className='container'>
        <Form
          {...formItemLayout}
          layout={formLayout}
          form={form}
          initialValues={{ layout: formLayout }}
        >
          <Row gutter={40}>
            <Col md={6} sm={12}>
              <Form.Item label="Mood">
                <Select defaultValue="0">
                  <Option value='0'>Happy</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col md={6} sm={12}>
              <Form.Item label="Theme">
                <Select defaultValue="0">
                  <Option value='0'>Nothing</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col md={6} sm={12}>
              <Form.Item label="Genre">
                <Select defaultValue="0">
                  <Option value='0'>African</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col md={6} sm={12}>
              <Form.Item label="Insrument">
                <Select defaultValue="0">
                  <Option value='0'>Bells</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    </div>
  )
}

export default Categories;