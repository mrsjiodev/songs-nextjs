import React from "react";
import Router from 'next/router'
import {connect } from 'react-redux';
// actions
import { getProfile } from '../../redux/actions/users'
// components
import HeaderPage from "../../components/layouts/Header";
import Profile from "../../components/settings/Profile";
import Subscription from "../../components/settings/Subscription";
// ui
import { Layout } from "antd";

const { Header, Content } = Layout;

class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null
    };
  }

  componentDidMount = async () => {
    const { user } = this.props;
    this.setState({ user })
  }

  render() {
    const { user } = this.props;
    
    return (
      <Layout className="home settings">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>
        <Content className="content-page">
          <div className="container">
            <Profile data={user} />

            <Subscription data={user} />
          </div>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    // users
    user: store.users.user
  }
}

const mapDispatchToProps = {
  getProfile,
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
