import { Row, Col, Select, Form } from "antd";

const { Option } = Select;

const SelectAddSong = props => {
  const { instruments, moods, themes, genres, required } = props;

  return (
    <div className="categories">
      <Row gutter={24}>
        <Col lg={12} md={12} sm={12} xs={24}>
          <Form.Item
            style={{display: "block"}}
            label="Mood"
            name="mood"
            rules={[{ required, message: "Please select mood!" }]}
          >
            <Select onChange={e => props.onChangeFilters(e, "mood")}>
              {moods &&
                moods.map((item, v) => (
                  <Option key={v} value={item._id}>
                    {item.title}
                  </Option>
                ))}
            </Select>
          </Form.Item>
        </Col>
        <Col lg={12} md={12} sm={12} xs={24}>
          <Form.Item
            style={{display: "block"}}
            label="Theme"
            name="theme"
            rules={[{ required, message: "Please select theme!" }]}
          >
            <Select onChange={e => props.onChangeFilters(e, "theme")}>
              {themes &&
                themes.map((item, v) => (
                  <Option key={v} value={item._id}>
                    {item.title}
                  </Option>
                ))}
            </Select>
          </Form.Item>
        </Col>
        </Row>
        <Row gutter={24}>
        <Col lg={12} md={12} sm={12} xs={24}>
          <Form.Item
            style={{display: "block"}}
            label="Genre"
            name="genre"
            rules={[
              {
                required,
                message: "Please select genre!",
                initialValues: "5ea7fd0165885c285261b993"
              }
            ]}
          >
            <Select onChange={e => props.onChangeFilters(e, "genre")}>
              {genres &&
                genres.map((item, v) => (
                  <Option key={v} value={item._id}>
                    {item.title}
                  </Option>
                ))}
            </Select>
          </Form.Item>
        </Col>
        <Col lg={12} md={12} sm={12} xs={24}>
          <Form.Item
            style={{display: "block"}}
            label="Instrument"
            name="instrument"
            rules={[{ required, message: "Please select instrument!" }]}
          >
            <Select onChange={e => props.onChangeFilters(e, "instrument")}>
              {instruments &&
                instruments.map((item, v) => (
                  <Option key={v} value={item._id}>
                    {item.title}
                  </Option>
                ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
};

export default SelectAddSong;
