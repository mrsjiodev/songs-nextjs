import React from "react";
import App, { Container } from "next/app";
import Head from "next/head";
import stylesheet from "antd/dist/antd.min.css";
import "../assets/styles/main.less";
import { Provider } from "react-redux";

import thunk from "redux-thunk";
import { applyMiddleware, createStore } from "redux";
import rootReducer from "../redux/reducers/index";

const store = createStore(rootReducer, applyMiddleware(thunk));

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <div>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
            rel="stylesheet"
          ></link>
          <link
            href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet"
          />

          <style global jsx>{`
            body {
              font-family: "Poppins", sans-serif;
            }
          `}</style>

          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />

          <script src="https://unpkg.com/wavesurfer.js"></script>
        </Head>

        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </div>
    );
  }
}
