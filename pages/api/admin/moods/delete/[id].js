import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Moods = require("../../../../../models/Moods");
const Songs = require("../../../../../models/Songs");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Moods.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Moods Not found" });
    else {
      await Songs.deleteMany({ mood: id });
      return res.status(200).json({ message: "Delete mood successfully" });
    }
  });
});

export default connectDb(handler);
