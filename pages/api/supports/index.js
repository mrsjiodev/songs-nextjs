import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import checkAuth from '../../../middleware/checkLogined'

import Supports from '../../../models/Supports'

const handler = nextConnect();

// export const config = {
//   api: {
//     bodyParser: false
//   }
// }

// get all supports
handler.get(async (req, res) => {
  let supports = await Supports.find().exec();
  res.status(200).json(supports)
})

// create support
handler.use(checkAuth);

handler.post(async (req, res) => {
  console.log(req.body)
  const { _id } = req.user;
  const { description } = req.body;

  let sp = new Supports({
    description,
    user: _id
  });

  await sp.save();

  res.status(200).json(sp)
})

export default connectDb(handler)