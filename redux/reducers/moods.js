const initData = {
  moods: [],
  isCreatedMood: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_ALL_MOODS':
      return {
        ...state,
        moods: action.data
      }
    case 'ERROR_ALL_MOODS':
      return {
        ...state,
        moods: []
      }
    case 'ACTION_CREATED_MOOD':
      return {
        ...state,
        isCreatedMood: true
      }
    case 'ERROR_CREATED_MOOD':
      return {
        ...state,
        isCreatedMood: false
      }
    default:
      return {
        ...state,
      }
  }
}