import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Answers = require("../../../../../models/Answers");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let answer = await Answers.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(answer);
});

export default connectDb(handler);
