import React from "react";
import { connect } from "react-redux";
// actions
import { createSupport } from "../../redux/actions/supports";
// ui
import { Form, Input, Button, message } from "antd";

const { TextArea } = Input;

class SupportForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.formRef = React.createRef();
  }

  onFinish = async values => {
    console.log(values);
    await this.props.createSupport(values);
    (await this.props.isCreatedSupport)
      ? message.success("Send request support successfully!")
      : message.error("Send request support failed!");

    (await this.props.isCreatedSupport) && this.formRef.current.resetFields();
  };

  render() {
    return (
      <Form
        ref={this.formRef}
        onFinish={this.onFinish}
        className="support-form"
      >
        <Form.Item
          label="Details"
          name="description"
          rules={[{ required: true, message: "Please input detail" }]}
        >
          <TextArea rows={8} placeholder="Write Details here......." />
        </Form.Item>
        <Form.Item>
          <div className="actions">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </div>
        </Form.Item>
      </Form>
    );
  }
}

const mapStateToProps = store => {
  return {
    // supports
    isCreatedSupport: store.supports.isCreatedSupport
  };
};

const mapDispatchToProps = {
  // supports
  createSupport
};

export default connect(mapStateToProps, mapDispatchToProps)(SupportForm);
