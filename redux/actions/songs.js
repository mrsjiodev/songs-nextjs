import request from "../config/request";

export function createSong(data) {
  return dispatch => {
    return request().post(`/songs/create`, data).then(res => {
      dispatch({
        type: "ACTION_SONGS_CREATED",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: "ERROR_SONGS_CREATED"
      })
    })
  }
}

export function getTopSongs() {
  return dispatch => {
    return request().get('songs/top-songs').then(res => {
      dispatch({
        type: "ACTION_GET_TOP_SONGS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: 'ERROR_GET_TOP_SONGS',
      })
    })
  }
}

export function getPopulers(){
  return dispatch => {
    return request().get('songs/populers').then(res => {
      dispatch({
        type: "ACTION_GET_POPULERS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: 'ERROR_GET_POPULERS'
      })
    })
  }
}

export function favoriteSong(song_id, isFavorite) {
  return dispatch => {
    return request().post(`songs/favorite`, {song_id, isFavorite}).then(res => {
      dispatch({
        type: "ACTION_FAVORITE_SONG_DONE",
        data: res.data 
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_FAVORITE_SONG",
      })
    })
  }
}

export function setRateSong(data){
  console.log('data: ', data)
  return dispatch => {
    return request().post(`songs/rate`, data).then(res =>  {
      dispatch({
        type: "ACTION_SET_RATE_SONG",
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_SET_RATE_SONG",
      })
    })
  }
}

export default {
  createSong,
  getPopulers,
  getTopSongs,
  favoriteSong,
  setRateSong
}