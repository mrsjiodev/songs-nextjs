const initData = {
  isLogined: false,
  token: null,
}

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_LOGINED_ADMIN":
      return {
        ...state,
        isLogined: true,
        token: action.data.token
      }
      case "ERROR_LOGINED_ADMIN":
        return {
          ...state,
          isLogined: false,
        }
    default:
      return {
        ...state
      }
  }
}