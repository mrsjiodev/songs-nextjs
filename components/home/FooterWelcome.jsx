// packages
import Link from "next/link";
// ui
import { Row, Col } from "antd";

const FooterWelcome = props => {
  return (
    <div className="footer-welcome">
      <div className="footer-ctn">
        <Row gutter={48}>
          <Col lg={12} md={14}>
            <div className="songs">
              <div className="logos">
                <Link href="/">
                  <a>SONGS</a>
                </Link>
              </div>
            </div>
          </Col>
          <Col lg={12} md={10} sm={24} xs={24}>
            <div className="products">
              <h3>PRODUCT</h3>
              <div className="ft-navbar">
                <div className="ft-nav">
                  <Link href="/terms-of-service">
                    <a>Terms of service</a>
                  </Link>
                  <Link href="/privacy-policy">
                    <a>Privacy policy</a>
                  </Link>
                  <Link href="/license-agreement">
                    <a>License agreement</a>
                  </Link>
                </div>
                <div className="ft-nav">
                  <Link href="/faqs">
                    <a>FAQs</a>
                  </Link>
                  {/* <Link href="/contact-us">
                    <a>Contact Us</a>
                  </Link> */}
                  <a target="_blank" href="mailto:admin@songs.com">
                    Contact Us
                  </a>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default FooterWelcome;
