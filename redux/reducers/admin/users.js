const initData = {
  all_users: [],
  message_delete: {},
  user: null,
}

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_GET_PROFILE_ADMIN":
      return {
        ...state,
        user: action.data,
      }
    case 'ACTION_GET_ALL_USERS':
      return {
        ...state,
        all_users: action.data
      }
    case 'ERROR_GET_ALL_USERS':
      return {
        ...state,
        all_users: []
      } 
    case 'ACTION_DELETE_USER':
      return {
        ...state,
        message_delete: action.data
      } 
    default:
      return {
        ...state,
      }
  }
}