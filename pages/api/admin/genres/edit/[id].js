import connectDb from "../../../../../middleware/dbMiddleware";
import uploadMulterS3 from "../../../../../middleware/uploadMulterS3";
const nextConnect = require("next-connect");
const Genres = require("../../../../../models/Genres");
const handler = nextConnect();

export const config = {
  api: {
    bodyParser: false,
  },
};

handler.put(uploadMulterS3.single("image"), async (req, res) => {
  try {
    const {
      query: { id },
    } = req;
    let genre = await Genres.findOneAndUpdate(
      { _id: id },
      { $set: req.body },
      (err, response) => {
        if (err) throw err;
      }
    ).exec();
    if(req.file){
      genre.image = req.file;
      await genre.save();
    }

    return res.status(200).json(genre);
  } catch (error) {
    console.log(error);
  }
});

export default connectDb(handler);
