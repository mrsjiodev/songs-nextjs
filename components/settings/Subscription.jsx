// packages
import React from "react";
import { connect } from "react-redux";
import Link from "next/link";
import moment from "moment";
import Router from "next/router";
// actions
import { getProfile } from "../../redux/actions/users";
import {
  allInvoices,
  setDefaulPaymentMethod,
  removePaymentMethod
} from "../../redux/actions/payments";
// components
import Invoices from "./Invoices";
import PopupPayment from "./PopupPayment";
// ui
import { Row, Col, Modal, Button, Tag, Dropdown, Menu, message } from "antd";
import { DeleteOutlined, MoreOutlined, PlusOutlined } from "@ant-design/icons";

class Subscription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowInvoices: false,
      isShowUpdateAddress: false
    };
  }

  componentDidMount = async () => {
    this.props.allInvoices();
  };

  onChangeBilling = () => {
    const { user } = this.props;
    if (user && user.type_standard === "basic") {
      Router.push("/payments/basic");
    } else {
      Router.push("/payments/pro");
    }
  };

  onChangePlan = () => {
    const { user } = this.props;
    if (user && user.type_standard === "basic") {
      Router.push("/payments/pro");
    } else {
      Router.push("/payments/basic");
    }
  };

  onShowInvoices = isShowInvoices => {
    this.setState({ isShowInvoices });
  };

  onShowUpdatePaymentCard = isShowUpdatePayment => {
    this.setState({ isShowUpdatePayment });
  };

  onShowUpdateAddressBilling = isShowUpdateAddress => {
    console.log("isShowUpdateAddress: ", isShowUpdateAddress);
    this.setState({ isShowUpdateAddress });
  };

  onShowViewPayment = isViewPayment => {
    this.setState({ isViewPayment });
  };

  onSetDefaultPaymentMethod = async payment_method_default => {
    await this.props.setDefaulPaymentMethod({
      payment_method_default
    });
    (await this.props.isSetDefaultMethod) && this.props.getProfile();
    (await this.props.isSetDefaultMethod)
      ? message.success("Saved")
      : message.error("Error");
  };

  onDeletePaymentMethod = async type => {
    await this.props.removePaymentMethod({ type });
    (await this.props.isDeletedPaymentMethod) && this.props.getProfile();
    (await this.props.isDeletedPaymentMethod)
      ? message.success("Deleted successfully!")
      : message.error("Deleted failed");
  };

  render() {
    const { user, invoices } = this.props;
    const {
      isShowInvoices,
      isShowUpdatePayment,
      isShowUpdateAddress,
      isViewPayment
    } = this.state;

    return (
      <div className="subscription">
        <div className="sub-content">
          <div className="sub-item">
            <h2>Current Plan</h2>
            <Row gutter={24}>
              <Col md={12}>
                {user && (
                  <div className="ctn-left">
                    {user && user.type_standard ? (
                      <p className="text">
                        {user && user.type_standard === "basic"
                          ? "Standard"
                          : "Premium"}{" "}
                        - ${user && user.amount / 100}/
                        {user && user.interval === "yearly" ? "year" : "month"}
                      </p>
                    ) : (
                      <p className="text">No Plan selected.</p>
                    )}

                    <div className="more-action">
                      {/* <a className="act">Auto-renews on May 18th, 2020</a> */}
                      {user && user.current_period_end ? (
                        <a className="act">
                          Auto-renews on{" "}
                          {user &&
                            moment(
                              new Date(user.current_period_end * 1000)
                            ).format("MMM Do, YYYY")}
                        </a>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                )}
              </Col>
              <Col md={12}>
                {user && user.type_standard ? (
                  <div className="ctn-right">
                    <div className="more-action">
                      <a onClick={() => this.onChangeBilling()}>
                        Switch to{" "}
                        {user && user.interval === "yearly"
                          ? "Monthly"
                          : "Yearly"}{" "}
                        billing
                      </a>
                    </div>
                    <div className="more-action">
                      <a onClick={() => this.onChangePlan()}>
                        {user && user.type_standard === "basic"
                          ? "Upgrade"
                          : "Downgrade"}{" "}
                        to{" "}
                        {user && user.type_standard === "basic"
                          ? "Premium"
                          : "Standard"}{" "}
                        Plan
                      </a>
                    </div>
                  </div>
                ) : (
                  <div className="ctn-right">
                    <div className="more-action">
                      <Link href="/plans">
                        <a>Select plan now</a>
                      </Link>
                    </div>
                  </div>
                )}
              </Col>
            </Row>
          </div>
          <div className="sub-item">
            <h3>
              Payment Method
              {/* <Button
                type="primary"
                className="btn-add-method"
                onClick={() => this.onShowUpdatePaymentCard(true)}
              >
                <PlusOutlined /> Add
              </Button> */}
            </h3>
            <Row gutter={24}>
              <Col md={24}>
                <div className="ctn-left">
                  <div className="card-view">
                    {user && user.card_info && (
                      <div className="card-item">
                        <div className="card-info">
                          {/* <CreditCardOutlined /> */}
                          <img
                            style={{ width: 60 }}
                            src={`/images/cards/${
                              (user &&
                                user.card_info &&
                                user.card_info.brand) ||
                              ""
                            }.png`}
                            alt=""
                          />
                          <div className="c-card">
                            <p className="card_num">
                              **** **** ****{" "}
                              {user && user.card_info && user.card_info.last4}
                            </p>
                            <p className="exp">
                              {user &&
                                user.card_info &&
                                user.card_info.exp_month}{" "}
                              /{" "}
                              {user &&
                                user.card_info &&
                                user.card_info.exp_year}
                            </p>
                          </div>

                          {user && user.payment_method_default === "card" && (
                            <Tag color="#87d068">Active</Tag>
                          )}
                        </div>

                        <div className="actions">
                          {user && user.payment_method_default === "paypal" && (
                            <Button
                              onClick={() =>
                                this.onSetDefaultPaymentMethod("card")
                              }
                              type="primary"
                            >
                              Active
                            </Button>
                          )}

                          {/* <Dropdown
                            placement="bottomRight"
                            overlay={
                              <Menu>
                                {user &&
                                  user.payment_method_default === "paypal" && (
                                    <Menu.Item
                                      key="0"
                                      onClick={() =>
                                        this.onSetDefaultPaymentMethod("card")
                                      }
                                    >
                                      Active
                                    </Menu.Item>
                                  )}
                                <Menu.Item
                                  key="1"
                                  onClick={() =>
                                    this.onDeletePaymentMethod("card")
                                  }
                                >
                                  Remove
                                </Menu.Item>
                              </Menu>
                            }
                            trigger={["click"]}
                          >
                            <a
                              className="ant-dropdown-link"
                              onClick={e => e.preventDefault()}
                            >
                              <MoreOutlined />
                            </a>
                          </Dropdown> */}
                        </div>
                      </div>
                    )}

                    {/* show paypal */}
                    {user && user.isPaypal && (
                      <div className="card-item">
                        <div className="card-info paypal-info">
                          <img
                            style={{ width: 60 }}
                            src={`/images/cards/paypal.png`}
                            alt=""
                          />
                          <div className="c-card">
                            <p className="card_num">
                              {`${
                                user &&
                                user.paypal_info &&
                                user.paypal_info.payer &&
                                user.paypal_info.payer.email_address.substring(
                                  0,
                                  4
                                )
                              }*****@***.com`}
                            </p>
                          </div>

                          {user && user.payment_method_default === "paypal" && (
                            <Tag color="#87d068">Active</Tag>
                          )}
                        </div>
                        <div className="actions">
                          {user && user.payment_method_default === "card" && (
                            <Button
                              onClick={() =>
                                this.onSetDefaultPaymentMethod("paypal")
                              }
                              type="primary"
                            >
                              Active
                            </Button>
                          )}
                          {/* <Dropdown
                            placement="bottomRight"
                            overlay={
                              <Menu>
                                {user &&
                                  user.payment_method_default === "card" && (
                                    <Menu.Item
                                      key="0"
                                      onClick={() =>
                                        this.onSetDefaultPaymentMethod("paypal")
                                      }
                                    >
                                      Active
                                    </Menu.Item>
                                  )}
                                <Menu.Item
                                  key="1"
                                  onClick={() =>
                                    this.onDeletePaymentMethod("paypal")
                                  }
                                >
                                  Remove
                                </Menu.Item>
                              </Menu>
                            }
                            trigger={["click"]}
                          >
                            <a
                              className="ant-dropdown-link"
                              onClick={e => e.preventDefault()}
                            >
                              <MoreOutlined />
                            </a>
                          </Dropdown> */}
                        </div>
                      </div>
                    )}
                  </div>

                  {/* <div className="more-action">
                    <a onClick={() => this.onShowViewPayment(true)}>View</a>

                    <span style={{ margin: "0 5px" }}>{` / `}</span>
                    <a onClick={() => this.onShowUpdatePaymentCard(true)}>
                      {user && !user.card_info && "Auto-View / "}
                      Change Payment Method
                    </a>
                  </div> */}
                </div>
              </Col>
            </Row>
          </div>
          <div className="sub-item">
            <h3>Billing Address</h3>
            <Row gutter={24}>
              <Col md={24}>
                <div className="ctn-left">
                  {user && user.billing_details_default && (
                    <p className="text">
                      {(user &&
                        user.billing_details_default &&
                        user.billing_details_default.address.line1) ||
                        ""}{" "}
                      ,{" "}
                      {(user &&
                        user.billing_details_default &&
                        user.billing_details_default.address.city) ||
                        ""}
                      ,{" "}
                      {(user &&
                        user.billing_details_default &&
                        user.billing_details_default.address.state) ||
                        ""}
                      ,{" "}
                      {(user &&
                        user.billing_details_default &&
                        user.billing_details_default.address.country) ||
                        ""}
                    </p>
                  )}

                  <div className="more-action">
                    <a onClick={() => this.onShowUpdateAddressBilling(true)}>
                      Change Billing Address
                    </a>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
          <div className="sub-item">
            <h3>Billing History</h3>
            <Row gutter={24}>
              <Col md={24}>
                <div className="ctn-left">
                  <div className="more-action">
                    <a onClick={() => this.onShowInvoices(true)}>
                      View Billing History
                    </a>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>

        {user && user.type_standard && (
          <div className="act-cancel-sub">
            <a>Cancel Subscription</a>
          </div>
        )}

        {/* view payment */}
        <Modal
          wrapClassName="popup-view-payment"
          title="Payment Method"
          centered
          visible={isViewPayment}
          footer={null}
          onOk={() => this.onShowViewPayment(false)}
          onCancel={() => this.onShowViewPayment(false)}
        >
          <div className="card-view">
            {user && user.card_info && (
              <div className="card-item">
                <div className="card-info">
                  {/* <CreditCardOutlined /> */}
                  <img
                    style={{ width: 60 }}
                    src={`/images/cards/${
                      (user && user.card_info && user.card_info.brand) || ""
                    }.png`}
                    alt=""
                  />
                  <div className="c-card">
                    <p className="card_num">
                      **** **** ****{" "}
                      {user && user.card_info && user.card_info.last4}
                    </p>
                    <p className="exp">
                      {user && user.card_info && user.card_info.exp_month} /{" "}
                      {user && user.card_info && user.card_info.exp_year}
                    </p>
                  </div>
                </div>
                <div className="actions">
                  {user && user.payment_method_default === "card" ? (
                    <Tag color="#87d068">Active</Tag>
                  ) : (
                    <Button
                      type="default"
                      onClick={() => this.onSetDefaultPaymentMethod("card")}
                    >
                      Active
                    </Button>
                  )}
                </div>
              </div>
            )}

            {/* show paypal */}
            {user && user.isPaypal && (
              <div className="card-item">
                <div className="card-info paypal-info">
                  <img
                    style={{ width: 60 }}
                    src={`/images/cards/paypal.png`}
                    alt=""
                  />
                  <div className="c-card">
                    <p className="card_num">
                      {`${
                        user &&
                        user.paypal_info &&
                        user.paypal_info.payer &&
                        user.paypal_info.payer.email_address.substring(0, 4)
                      }*****@***.com`}
                    </p>
                  </div>
                </div>
                <div className="actions">
                  {user && user.payment_method_default === "paypal" ? (
                    <Tag color="#87d068">Active</Tag>
                  ) : (
                    <Button
                      type="default"
                      onClick={() => this.onSetDefaultPaymentMethod("paypal")}
                    >
                      Active
                    </Button>
                  )}
                </div>
              </div>
            )}
          </div>
        </Modal>

        {/* invoices */}
        <Modal
          wrapClassName="popup-invoices"
          title="Invoices"
          centered
          visible={isShowInvoices}
          footer={null}
          onOk={() => this.onShowInvoices(false)}
          onCancel={() => this.onShowInvoices(false)}
        >
          <Invoices data={invoices} />
        </Modal>

        {/* payments card */}
        <Modal
          wrapClassName="popup-payment"
          title="Add Payment Method"
          centered
          visible={isShowUpdatePayment}
          footer={null}
          onOk={() => this.onShowUpdatePaymentCard(false)}
          onCancel={() => this.onShowUpdatePaymentCard(false)}
        >
          <PopupPayment
            isShowUpdatePayment={isShowUpdatePayment}
            isShowUpdateAddress={isShowUpdateAddress}
            onShowUpdatePaymentCard={this.onShowUpdatePaymentCard}
            onShowUpdateAddressBilling={this.onShowUpdateAddressBilling}
          />
        </Modal>

        {/* payments address */}
        <Modal
          wrapClassName="popup-payment"
          title={null}
          centered
          visible={isShowUpdateAddress}
          footer={null}
          onOk={() => this.onShowUpdateAddressBilling(false)}
          onCancel={() => this.onShowUpdateAddressBilling(false)}
        >
          <PopupPayment
            isShowUpdatePayment={isShowUpdatePayment}
            isShowUpdateAddress={isShowUpdateAddress}
            onShowUpdatePaymentCard={this.onShowUpdatePaymentCard}
            onShowUpdateAddressBilling={this.onShowUpdateAddressBilling}
          />
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user,
    // payments
    invoices: store.payments.invoices,
    isSetDefaultMethod: store.payments.isSetDefaultMethod,
    isDeletedPaymentMethod: store.payments.isDeletedPaymentMethod
  };
};

const mapDispatchToProps = {
  getProfile,
  // payments
  allInvoices,
  setDefaulPaymentMethod,
  removePaymentMethod
};

export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
