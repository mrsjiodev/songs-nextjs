import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Themes = require('../../../../models/Themes');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let theme = await Themes.find({})
  .exec();
  res.status(200).json(theme);
})

export default connectDb(handler);