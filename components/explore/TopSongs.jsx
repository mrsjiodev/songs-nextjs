// packages
import moment from "moment";
import Router from "next/router";
// ui
import { Button, Tag, Row, Col, Tooltip } from "antd";
import { HeartOutlined, DownloadOutlined } from "@ant-design/icons";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class TopSongs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.wavesurfer = null;
  }

  handlePlayMusic = async data => {
    const { wavesurferMain } = this.props;
    if (wavesurferMain) {
      await wavesurferMain.stop();
    }
    this.props.onPlayMusic(data);
  };

  handleDownloadFile = url => {
    const { user } = this.props;
    if (user) {
      if (user.plan_default) {
        window.location.href = url;
      } else {
        Router.push("/plans");
      }
    } else {
      this.props.onShowPopupLogin(true);
    }
  };

  handleFavorite = song => {
    const { user, onFavoriteSong } = this.props;
    if (user) {
      onFavoriteSong(song._id, !this.checkFavoriteSong(song._id));
    } else {
      this.props.onShowPopupLogin(true);
    }
  };

  checkFavoriteSong = song_id => {
    const { favorites } = this.props;
    let cheched = favorites.includes(song_id);
    return cheched;
  };

  render() {
    const { data } = this.props;
    const { itemPlaying, isPlayingMusic } = this.props;

    return (
      <div className="topSongs">
        <div className="container">
          <h1 className="title">Top Songs</h1>

          {data &&
            data.map(el => (
              <div
                className={`songItem ${
                  itemPlaying && el._id === itemPlaying._id ? "active" : ""
                }`}
                key={el._id}
              >
                <Row gutter={24}>
                  <Col
                    md={6}
                    sm={8}
                    xs={18}
                    onClick={() => this.handlePlayMusic(el)}
                  >
                    <div className="info">
                      <div
                        className="status"
                        style={{ backgroundImage: `${el.bgColor}` }}
                      >
                        {!el.bgColor && (
                          <div className="bg">
                            <img
                              src={`${URL_MEDIA}${
                                (el.image && el.image.key) || ""
                              }`}
                              alt=""
                            />
                          </div>
                        )}
                        <a className="btn-play">
                          {itemPlaying &&
                          el._id === itemPlaying._id &&
                          isPlayingMusic ? (
                            <img
                              src="./images/icons/pause-circle-small.png"
                              alt=""
                            />
                          ) : (
                            <img
                              src="./images/icons/play-circle-small.png"
                              alt=""
                            />
                          )}
                        </a>
                      </div>
                      <div className="song">
                        <h3>
                          <Tooltip title={el.track_title}>
                            {el.track_title}
                          </Tooltip>
                        </h3>
                        <p>{el.artist_name}</p>
                      </div>
                    </div>
                  </Col>
                  <Col
                    md={6}
                    sm={0}
                    xs={0}
                    onClick={() => this.handlePlayMusic(el)}
                  ></Col>
                  <Col
                    md={6}
                    sm={8}
                    xs={0}
                    onClick={() => this.handlePlayMusic(el)}
                  >
                    <div className="labels">
                      <Tag>{el.mood.title}</Tag>
                      <Tag>{el.theme.title}</Tag>
                      <Tag>{el.genre.title}</Tag>
                      <Tag>{el.instrument.title}</Tag>
                    </div>
                  </Col>
                  <Col
                    md={3}
                    sm={4}
                    xs={6}
                    onClick={() => this.handlePlayMusic(el)}
                  >
                    <div className="timeAt">
                      <span>
                        {moment.utc(el.duration * 1000).format("mm:ss")}
                      </span>
                    </div>
                  </Col>
                  <Col md={3} sm={4} xs={0}>
                    <div className="actions">
                      <div className="btns">
                        <Button
                          className={`${
                            this.checkFavoriteSong(el._id) ? "favorite" : ""
                          }`}
                          type="default"
                          onClick={() => this.handleFavorite(el)}
                          icon={
                            this.checkFavoriteSong(el._id) ? (
                              <img
                                style={{ width: 18 }}
                                src="/images/icons/heart-fill.svg"
                              />
                            ) : (
                              <HeartOutlined />
                            )
                          }
                          size={24}
                        />
                        <Button
                          type="default"
                          icon={<DownloadOutlined />}
                          size={24}
                          onClick={() =>
                            this.handleDownloadFile(el.audio.location)
                          }
                        />
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default TopSongs;
