import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Moods = require('../../../../models/Moods');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let mood = await Moods.find({})
  .exec();
  res.status(200).json(mood);
})

export default connectDb(handler);