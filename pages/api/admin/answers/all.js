import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Answers = require('../../../../models/Answers');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let answer = await Answers.find({})
  .populate({
    path: "created_by",
    model: "Users",
  })
  .populate({
    path: "question",
    model: "Questions",
  })
  .exec();
  res.status(200).json(answer);
})

export default connectDb(handler);