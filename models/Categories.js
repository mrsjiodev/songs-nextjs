const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  title: { type: String },
  name: {type: String},
  values: { type: Array },
}, {
  timestamps: true
})

module.exports = mongoose.models.Categories || mongoose.model('Categories', CategorySchema);