import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt';
import connectDb from '../../../middleware/dbMiddleware'

const nextConnect = require('next-connect')
const Users = require('../../../models/Users')

const handler = nextConnect();

handler.post(async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log(password);
    let user = await Users.findOne({ email }).exec();

    if (user) {
      //  check pass
      let checked = bcrypt.compare(password, user.password).then(async checked => {
        console.log('checked: ', checked)
        // create token
        if (checked) {
          let token = await jwt.sign({ _id: user._id, email: user.email }, process.env.secret, { expiresIn: '7d' })

          user.token = token;
          await user.save();
          res.json({ success: true, token: 'JWT ' + token })
        } else {
          res.status(401).json({ msg: 'Password incorrect!' })
        }
      }).catch(err => {
        console.log(err)
        res.status(401).json({ msg: 'Check password failed!' })
      });


    } else {
      res.status(401).send({ success: false, msg: 'Authentication failed. Users not found.' });
    }
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Connect db failed!" })
  }

})

export default connectDb(handler);