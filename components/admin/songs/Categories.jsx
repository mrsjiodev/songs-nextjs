import { Row, Col, Select, Form } from "antd";

const { Option } = Select;

const Categories = (props) => {
  const { instruments, moods, themes, genres, required } = props;
  const {
    instrumentSelected,
    moodSelected,
    themeSelected,
    genreSelected,
  } = props;

  return (
    <div className="categories">
      <Row gutter={24}>
        <Col lg={12} md={12} sm={12} xs={24} style={{ marginBottom: 20 }}>
          <label>Mood</label>
          <Select
            style={{ width: "100%" }}
            onChange={(e) => props.onChangeFilters(e, "mood")}
            value={moodSelected}
          >
            {moods &&
              moods.map((item, v) => (
                <Option key={v} value={item._id}>
                  {item.title}
                </Option>
              ))}
          </Select>
        </Col>
        <Col lg={12} md={12} sm={12} xs={24}>
          <label>Theme</label>
          <Select
            style={{ width: "100%" }}
            onChange={(e) => props.onChangeFilters(e, "theme")}
            value={themeSelected}
          >
            {themes &&
              themes.map((item, v) => (
                <Option key={v} value={item._id}>
                  {item.title}
                </Option>
              ))}
          </Select>
        </Col>
        <Col lg={12} md={12} sm={12} xs={24}>
          <label>Genre</label>
          <Select
            style={{ width: "100%" }}
            onChange={(e) => props.onChangeFilters(e, "genre")}
            value={genreSelected}
          >
            {genres &&
              genres.map((item, v) => (
                <Option key={v} value={item._id}>
                  {item.title}
                </Option>
              ))}
          </Select>
        </Col>
        <Col lg={12} md={12} sm={12} xs={24}>
          <label>Instrument</label>
          <Select
            style={{ width: "100%" }}
            onChange={(e) => props.onChangeFilters(e, "instrument")}
            value={instrumentSelected}
          >
            {instruments &&
              instruments.map((item, v) => (
                <Option key={v} value={item._id}>
                  {item.title}
                </Option>
              ))}
          </Select>
        </Col>
      </Row>
    </div>
  );
};

export default Categories;
