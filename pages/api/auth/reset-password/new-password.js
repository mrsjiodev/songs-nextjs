import connectDb from '../../../../middleware/dbMiddleware';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const nextConnect = require('next-connect')
const Users = require('../../../../models/Users')
const handler = nextConnect();
const saltRounds = 10;

handler.post(async (req, res) => {
  try {
    const { password, token } = req.body;

    let decoded = await jwt.verify(token, 'secret', { expiresIn: '60 * 3' });
    console.log(decoded);

    const { _id } = decoded;


    bcrypt.hash(password, saltRounds).then(async (hash) => {
      console.log('hash: ', hash)
      let user = await Users.findByIdAndUpdate(_id, { password: hash }).exec();

      res.status(200).json({ status: 'success', msg: 'Reset password successfully!' })
    });

  } catch (error) {
    console.log(error)
    res.status(400).json({ status: 'error', msg: 'Error' })
  }
})

export default connectDb(handler);