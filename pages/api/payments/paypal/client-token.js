import braintree from "braintree";
import nextConnect from 'next-connect';

const handler = nextConnect();

export const config ={
  api: {
    bodyParser: false
  }
}

handler.get(async(req, res) => {
  let gateway = braintree.connect({
    accessToken: process.env.PAYPAL_ACCESS_TOKEN
  })

  console.log('gateway: ', gateway)

  gateway.clientToken.generate({}, function (err, response) {
    console.log(response);
    res.send(response.clientToken);
  });
})

export default handler;