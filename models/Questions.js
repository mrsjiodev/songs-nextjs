const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
  question: { type: String },
  created_by: { type: Schema.Types.ObjectId, ref: 'Users' }
}, {
  timestamps: true
})

module.exports = mongoose.models.Questions || mongoose.model('Questions', QuestionSchema);