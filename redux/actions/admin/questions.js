import request from '../../config/request';

export function createQuestion(data) {
  return dispatch => {
    return request().post(`admin/questions`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_QUESTION",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
    })
  }
}

export function getAllQuestion() {
  return dispatch => {
    return request().get('admin/questions/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_QUESTIONS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteQuestion(id) {
  return dispatch => {
    return request().delete(`admin/questions/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_QUESTIONS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateQuestion(id,data) {
  return dispatch => {
    return request().put(`admin/questions/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  createQuestion,
  getAllQuestion,
  deleteQuestion,
  updateQuestion
}