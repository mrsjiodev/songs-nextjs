import React, { useState, useEffect } from "react";
import Link from "next/link";
import Router, { useRouter } from "next/router";
import { connect } from "react-redux";
import { getCountries, getStates, getCountry } from "country-state-picker";
// actions
import {
  createCustomer,
  createSubscription,
  checkDiscount,
  updateCustomer
} from "../../redux/actions/payments";
import { getProfile } from "../../redux/actions/users";
// stripe
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
// components
import PaypalCheckout from "../../components/payments/PaypalCheckout";
// ui
import { Form, Input, Row, Col, Button, message, Select } from "antd";
import { CreditCardOutlined } from "@ant-design/icons";

const { Option } = Select;

const CheckoutForm = props => {
  const [form] = Form.useForm();
  const [isDiscount, setDiscount] = useState(false);
  const [isValid, setValid] = useState(false);
  const [valueDiscount, setValueDiscount] = useState("");
  const [statusDiscount, setStatusDiscount] = useState("");
  const stripe = useStripe();
  const elements = useElements();
  const [payment_method, setPaymentMethod] = useState("credit_card");
  const router = useRouter();
  const { slug } = router.query;
  const [states, setStates] = useState(getStates("al"));
  const countries = getCountries();
  const { type } = props;

  useEffect(() => {
    if (props.payment_intent) {
      handleSubscription(props.payment_intent);
      props.onLoading(false);
      onReset();
    }
  }, [props.payment_intent]);

  useEffect(() => {
    if (props.isErr) {
      props.onLoading(false);
      message.error(props.msgErr);
    }
  }, [props.msgErr, props.isErr]);

  useEffect(() => {
    if (isDiscount) {
      props.isValidDiscount
        ? message.success("Added discount code successfully")
        : message.error("Added discount code error");

      setValid(props.isValidDiscount);
    }
  }, [props.isValidDiscount]);

  const onCheckDiscount = async discount_id => {
    form.isFieldValidating("discount");
    await props.checkDiscount({ discount_id });
    let checked = (await props.isValidDiscount) ? "success" : "error";

    setValueDiscount(discount_id);
    setStatusDiscount(checked);

    getPricePay();
  };

  const onFinish = async values => {
    await props.onLoading(true);
    console.log(values);

    const result = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
      billing_details: {
        address: {
          city: values.city,
          line1: values.address,
          postal_code: values.zip,
          state: values.state,
          country: values.country
        },
        email: props.user.email,
        name: values.name,
        phone: "555-555-5555"
      }
    });

    await handleStripePaymentMethod(result, values, elements.getElement(CardElement));
  };

  const handleStripePaymentMethod = async (result, values, card_info) => {
    if (result.error) {
      message.error(result.error.message);
    } else {
      // check customer stripe
      if (!props.user.customer_stripe_id) {
        // create customer & subscription
        await props.createCustomer({
          payment_method_id: result.paymentMethod.id,
          city: values.city,
          address: values.address,
          state: values.state,
          country: values.country,
          full_name: values.name,
          postal_code: values.zip,
        });

        await props.getProfile();
      } else {
        // await props.updateCustomer({
        //   payment_method_id: result.paymentMethod.id,
        //   city: values.city,
        //   address: values.address,
        //   state: values.state,
        //   country: values.country,
        //   full_name: values.name
        // });
      }

      // create subscription
      await props.createSubscription({
        plan_id: props.plan_id,
        discount_id: valueDiscount,
        type_standard: slug,
        interval: props.type,
        billing_details: {
          address: {
            city: values.city,
            line1: values.address,
            postal_code: values.zip,
            state: values.state,
            country: values.country
          },
          email: props.user.email,
          name: values.name,
          phone: "555-555-5555"
        },
        // card_info: elements.getElement(CardElement)
      });
    }
  };

  const handleSubscription = async payment_intent => {
    if (payment_intent) {
      const { client_secret, status, amount } = payment_intent;

      if (status === "requires_action") {
        stripe.confirmCardPayment(client_secret).then(confirm => {
          if (confirm.error) {
            message.error(confirm.error.message);
          }
        });
      }

      message.success(`You have successfully paid $${amount / 100}`);
      await Router.push("/plans");
    } else {
      message.warning("No payment information received");
    }
  };

  const getPricePay = () => {
    const { type } = props;

    let _price = 0;
    if (slug === "basic") {
      _price = type === "monthly" ? 10 : 99;
    } else {
      _price = type === "monthly" ? 35 : 399;
    }

    if (valueDiscount && valueDiscount !== "" && props.discountInfo) {
      _price = (1 - props.discountInfo.percent_off / 100) * Number(_price);
    }

    return _price.toFixed(2);
  };

  const onReset = () => {
    form.resetFields();
    setDiscount(false);
    setStatusDiscount("");
    setValueDiscount("");
    setValid(false);
  };

  const cardOptions = {
    iconStyle: "solid",
    style: {
      base: {
        iconColor: "#1890ff",
        color: "rgba(0, 0, 0, 0.65)",
        fontWeight: 500,
        fontFamily: '"Poppins", sans-serif',
        fontSize: "16px",
        fontSmoothing: "antialiased",
        lineHeight: "31px",
        ":-webkit-autofill": { color: "#fce883" },
        "::placeholder": { color: "#d0d7dd" },
        background: "#FFFFFF",
        border: "1px solid #D0D7DD",
        boxSizing: "border-box",
        borderRadius: "4px"
      },
      invalid: {
        iconColor: "#eb1c26",
        color: "#eb1c26"
      }
    }
  };

  // country
  const onChangeCountry = value => {
    let _states = getStates(value);

    setStates(getStates(value));
  };

  const onBlurCountry = () => {};

  const onFocusCountry = () => {};

  const onSearchCountry = val => {};

  // state
  const onChangeState = value => {};

  const onBlurState = () => {};

  const onFocusState = () => {};

  const onSearchState = val => {};

  return (
    <Form onFinish={onFinish} form={form} className="checkout-form">
      <h2 className="tl">
        <span className="cur">$</span>
        {slug === "basic" ? (
          <span className="price">
            {type === "monthly" ? "10.00" : "99.00"}
          </span>
        ) : (
          <span className="price">
            {type === "monthly" ? "35.00" : "399.00"}
          </span>
        )}

        <span className="mo">{type === "monthly" ? "/mo" : "/ye"}</span>
      </h2>

      <Row gutter={24}>
        <Col md={24}>
          <h3 className="title-form">Payment Method</h3>
        </Col>

        <Col md={12}>
          <Button
            onClick={() => setPaymentMethod("credit_card")}
            className={`payment-method mth-card ${
              payment_method === "credit_card" ? "btn-active" : ""
            }`}
            icon={<CreditCardOutlined />}
          >
            Credit Card
          </Button>
        </Col>

        <Col md={12}>
          <Button
            onClick={() => setPaymentMethod("paypal")}
            id="btn-paypal"
            className={`payment-method mth-paypal ${
              payment_method === "paypal" ? "btn-active" : ""
            }`}
            icon={<CreditCardOutlined />}
          >
            Paypal
          </Button>
        </Col>

        {payment_method === "credit_card" ? (
          <>
            <Col md={24}>
              <Form.Item
                name="card"
                rules={[{ required: true, message: "Please input your Card!" }]}
              >
                <CardElement options={cardOptions} />
              </Form.Item>
            </Col>

            <Col md={24}>
              <h3 className="title-form">Billing Address</h3>
            </Col>

            <Col md={24}>
              <Form.Item
                name="name"
                rules={[
                  { required: true, message: "Please input your Full Name!" }
                ]}
              >
                <Input placeholder="Full Name" />
              </Form.Item>
            </Col>

            <Col md={24}>
              <Form.Item
                name="address"
                rules={[
                  { required: true, message: "Please input your Address!" }
                ]}
              >
                <Input placeholder="Street Address" />
              </Form.Item>
            </Col>

            <Col md={10}>
              <Form.Item
                name="city"
                rules={[{ required: true, message: "Please input your City!" }]}
              >
                <Input placeholder="City" />
              </Form.Item>
            </Col>

            <Col md={14}>
              <Form.Item
                name="country"
                rules={[
                  { required: true, message: "Please select your Country!" }
                ]}
              >
                {/* <Input placeholder="Country" /> */}

                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a Country"
                  optionFilterProp="children"
                  onChange={onChangeCountry}
                  onFocus={onFocusCountry}
                  onBlur={onBlurCountry}
                  onSearch={onSearchCountry}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {countries &&
                    countries.map((item, i) => (
                      <Option key={i} value={item.code}>
                        {item.name}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>

            <Col md={14}>
              <Form.Item
                name="state"
                rules={[
                  { required: true, message: "Please select your State!" }
                ]}
              >
                {/* <Input placeholder="State" /> */}
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder="Select a State"
                  optionFilterProp="children"
                  onChange={onChangeState}
                  onFocus={onFocusState}
                  onBlur={onBlurState}
                  onSearch={onSearchState}
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {states &&
                    states.map((item, i) => (
                      <Option key={i} value={item}>
                        {item}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
            </Col>

            <Col md={10}>
              <Form.Item
                name="zip"
                rules={[
                  { required: true, message: "Please input your Zipcode!" }
                ]}
              >
                <Input placeholder="Zipcode" />
              </Form.Item>
            </Col>

            {isDiscount ? (
              <Col md={24}>
                <Row gutter={24}>
                  <Col md={12}>
                    <Form.Item
                      name="discount"
                      rules={
                        [
                          // { required: true, message: "Please input your Zipcode!" }
                        ]
                      }
                    >
                      <Input
                        placeholder="Code"
                        disabled={isValid}
                        onChange={e => setValueDiscount(e.target.value)}
                      />
                    </Form.Item>
                  </Col>
                  <Col md={6}>
                    {isValid ? (
                      <Button
                        type="primary"
                        onClick={() => setValid(false)}
                        className="btn-discount"
                      >
                        Change
                      </Button>
                    ) : (
                      <Button
                        type="primary"
                        onClick={() => onCheckDiscount(valueDiscount)}
                        className="btn-discount"
                      >
                        Apply
                      </Button>
                    )}
                  </Col>
                  <Col md={6}>
                    {isValid ? (
                      <Button
                        type="danger"
                        onClick={() => {
                          setValid(false);
                          setValueDiscount("");
                          form.setFieldsValue({ discount: "" });
                        }}
                        className="btn-cancel-discount"
                      >
                        Remove
                      </Button>
                    ) : (
                      <Button
                        type="danger"
                        onClick={() => setDiscount(false)}
                        className="btn-cancel-discount"
                      >
                        Cancel
                      </Button>
                    )}
                  </Col>
                </Row>
              </Col>
            ) : (
              <Col md={24}>
                <div className="others">
                  {/* <Link href="/"> */}
                  <a
                    className="discount-code"
                    onClick={() => setDiscount(!isDiscount)}
                  >
                    Have a discount code?
                  </a>
                  {/* </Link> */}
                </div>
              </Col>
            )}
          </>
        ) : (
          <Col md={24}>
            <PaypalCheckout
              plan_id={props.plan_id}
              currency={props.currency}
              amount_plan={props.amount_plan}
            />
          </Col>
        )}
      </Row>

      {payment_method === "credit_card" && (
        <Button
          // loading={isLoading}
          type="primary"
          htmlType="submit"
          className="checkout-button"
          disabled={!stripe}
        >
          Pay Now
        </Button>
      )}
    </Form>
  );
};

const mapStateToProps = store => {
  return {
    isCreated: store.payments.isCreated,
    isSubscription: store.payments.isSubscription,
    customer: store.payments.customer,
    subscription: store.payments.subscription,
    latest_invoice: store.payments.latest_invoice,
    payment_intent: store.payments.payment_intent,
    // plans
    plans: store.payments.plans,
    isValidDiscount: store.payments.isValidDiscount,
    discountInfo: store.payments.discountInfo,
    msgErr: store.payments.msgErr,
    isErr: store.payments.isErr,
    // users
    user: store.users.user
  };
};

const mapDispatchToProps = {
  // payments
  createCustomer,
  createSubscription,
  checkDiscount,
  updateCustomer,
  // users
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutForm);
