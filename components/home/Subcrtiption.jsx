// components
import AskedQuestions from "../support/AskedQuestions";
import WhoIsSongsFor from "./WhoIsSongsFor";
// ui
import { Row, Col } from "antd";

const Subcrtiption = props => {
  return (
    <div className="subcription">
      <div className="sub-ctn">
        <h1>Subscription</h1>
        <div className="list-options">
          <Row gutter={64}>
            <Col md={12} xs={24}>
              <div className="sub-item">
                <div className="sub-icon">
                  <img src="/images/welcome/Ipad.png" alt="" />
                </div>
                <h2>Easy Filters</h2>
                <p>
                  Find the right track for your video in seconds. Filter by
                  mood, genre or instrument to find a track to fit the vibe.
                </p>
              </div>
            </Col>
            <Col md={12} xs={24}>
              <div className="sub-item">
                <div className="sub-icon">
                  <img src="/images/welcome/Mobile.png" alt="" />
                </div>
                <h2>Unlimited Licenses</h2>
                <p>
                  Unlimited access to thousands of songs. Unlimited use, Always
                  copyright free.
                </p>
              </div>
            </Col>
            <Col md={12} xs={24}>
              <div className="sub-item">
                <div className="sub-icon">
                  <img src="/images/welcome/Upload.png" alt="" />
                </div>
                <h2>Stem Files</h2>
                <p>
                  With stem files you get access to all the audio files that
                  make the song. Just want the strings from the song? Easy, want
                  to cut out the bass? No Problem.
                </p>
              </div>
            </Col>
            <Col md={12} xs={24}>
              <div className="sub-item">
                <div className="sub-icon">
                  <img src="/images/welcome/File.png" alt="" />
                </div>
                <h2>New Music Weekly</h2>
                <p>
                  We add hundreds of new exclusive songs every month. Be the
                  first to use these songs in a video
                </p>
              </div>
            </Col>
          </Row>
        </div>

        <WhoIsSongsFor />

        <div className="support-ctn">
          <AskedQuestions data={props.answers_questions} />
        </div>
      </div>
    </div>
  );
};

export default Subcrtiption;
