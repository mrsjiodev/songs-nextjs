import request from '../config/request'

export function getAllCategories(){
  return dispatch => {
    return request().get(`categories`).then(res => {
      dispatch({
        type: "ACTION_GET_ALL_CATEGORIES",
        data: res.data 
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: "ERROR_GET_ALL_CATEGORIES"
      })
    })
  }
}

export default {
  getAllCategories
}