import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);

const hanlder = nextConnect();

// export const config = {
//   api: {
//     bodyParser: false
//   }
// }

hanlder.post(async (req, res) => {
  try {
    console.log(req.body);
    const { discount_id } = req.body
    let checked = await stripe.coupons.retrieve(discount_id)
    console.log(checked);
    res.status(200).json(checked);
  } catch (error) {
    res.status(400).json({ msg: 'Discount incorrect' })
  }

})

export default hanlder