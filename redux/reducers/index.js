import { combineReducers } from 'redux';

import users from './users';
import auth from './auth'
import songs from './songs'
import categories from './categories'
import payments from './payments'
import answers from './answers';
import supports from './supports';
import genres from "./genres";
import moods from "./moods";
import themes from "./themes";
import instruments from "./instruments";
import reset_password from './reset_password';
// admin
import adminUser from './admin/users';
import adminSong from './admin/songs';
import adminMood from './admin/moods';
import adminTheme from './admin/themes';
import adminInstrument from './admin/instruments';
import adminGenre from './admin/genres';
import adminQuestion from './admin/questions';
import adminAnswer from './admin/answers';
import adminSupport from './admin/supports';
import adminAnalytics from './admin/analytics';
import adminAuth from './admin/auth'

export default combineReducers({
  users,
  auth,
  songs,
  categories,
  payments,
  answers,
  supports,
  genres,
  moods,
  themes,
  instruments,
  reset_password,
  // admin
  adminUser,
  adminSong,
  adminMood,
  adminTheme,
  adminInstrument,
  adminGenre,
  adminQuestion,
  adminAnswer,
  adminSupport,
  adminAnalytics,
  adminAuth
})