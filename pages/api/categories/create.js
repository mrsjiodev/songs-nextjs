
import connectDb from '../../../middleware/dbMiddleware'
import Categories from '../../../models/Categories';
const nextConnect = require('next-connect')
const handler = nextConnect();

const data = [
  {
    title: 'Mood',
    name: 'mood',
    values: [
      "Angry",
      "Scary & Dark",
      "Hopeful",
      "Weird",
      'Sexy',
      'Mysterious',
      'Happy',
      'Powerful',
      'Peaceful',
      'Serious',
      'Dramatic',
      'Uplifting',
      'Tense',
    ]
  },
  {
    title: 'Theme',
    name: 'theme',
    values: [
      'Vlog',
      'Cool',
      'Glitch',
      'Cinematic',
      'Travel',
      'Slow motion',
      'Business',
      'Science',
      'Fashion',
      'Tech',
      'Nature',
      'Fitness',
      'Time lapse',
      'Aerials',
      'Food',
      'Education',
    ]
  },
  {
    title: "Genre",
    name: 'genre',
    values: [
      'Electronic',
      'Pop',
      'Hip-hop',
      'Singer',
      'Classical',
      'Cinematic',
      'World',
      'Jazz',
      'Funky',
      'Acoustic',
      'Indie',
    ]
  },
  {
    title: 'Instrument',
    name: 'instrument',
    values: [
      'Piano',
      'Keys',
      'Synth',
      'Vocals',
      'Acoustic',
      'Electric',
      'Drums',
      'Percussion',
      'Claps & Snaps',
      'Bells',
      'Whistle',
      'Strings',
      'Orchestra',
      'Wind Instruments',
      'Ethnic',
    ]
  }
]

handler.post(async (req, res) => {
  try {
    for (let i = 0; i < data.length; i++) {
      const cate = data[i];
      let category = new Categories({
        title: cate.title,
        name: cate.name,
        values: cate.values
      })

      await category.save();
    }

    res.status(200).json({ success: true, msg: 'Created categories successfully!' })
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: "Create categories failed!" })
  }
})

export default connectDb(handler);