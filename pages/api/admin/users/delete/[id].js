import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Users = require("../../../../../models/Users");
const Supports = require("../../../../../models/Supports");
const Songs = require("../../../../../models/Songs");
const Questions = require("../../../../../models/Questions");
const Answers = require("../../../../../models/Answers");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Users.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "User Not found" });
    else {
      await Supports.deleteMany({ user: id });
      await Songs.deleteMany({ created_by: id });
      await Songs.deleteMany({ updated_by: id });
      await Questions.deleteMany({ created_by: id });
      await Answers.deleteMany({ created_by: id });
      return res.status(200).json({ message: "Delete user successfully" });
    }
  });
});

export default connectDb(handler);
