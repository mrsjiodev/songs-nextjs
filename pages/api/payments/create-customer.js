import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);

const hanlder = nextConnect();

// export const config = {
//   api: {
//     bodyParser: false
//   }
// }

hanlder.post(async (req, res) => {
  const { id, email } = req.body;

  // let customer = await stripe.customers.

  let new_customer = await stripe.customers.create({
    payment_method: id,
    phone: "555-555-5555",
    email,
    invoice_settings: {
      default_payment_method: id
    }
  })

  let customer_id = null;
  if(new_customer){
    customer_id = new_customer.id
  }

  // subscription
  let subscription = await stripe.subscriptions.create({
    customer: customer_id,
    items: [
      {
        plan: 'plan_H8HKSbzdV0Vb3y',
        // plan: 'plan_H8HJpL6CV1cb6u',
      }
    ],
    expand: ["latest_invoice.payment_intent"]
  })

  res.status(200).json({ subscription, customer });
})

export default hanlder;