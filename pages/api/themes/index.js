import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import Themes from '../../../models/Themes'

const hanlder = nextConnect();


hanlder.get(async (req, res) => {
  let moods = await Themes.find().exec();

  res.status(200).json(moods)
})

hanlder.post(async (req, res) => {
  try {
    const { title, name } = req.body;

    let mood = new Themes({
      title,
      name
    });

    await mood.save();

    res.status(200).json(mood)
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Created themes failed" })
  }
})

export default connectDb(hanlder);