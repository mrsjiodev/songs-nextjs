export const data = [
  {
    title: 'Angry',
    name: 'angry'
  },
  {
    title: 'Scary & Dark',
    name: 'scary_dark'
  },
  {
    title: 'Hopeful',
    name: 'hopeful'
  },
  {
    title: 'Weird',
    name: 'weird'
  },
  {
    title: 'Sexy',
    name: 'sexy'
  },
  {
    title: 'Mysterious',
    name: 'mysterious'
  },
  {
    title: 'Happy',
    name: 'happy'
  },
  {
    title: 'Powerful',
    name: 'powerful'
  },
  {
    title: 'Peaceful',
    name: 'peaceful'
  },
  {
    title: 'Serious',
    name: 'serious'
  },
  {
    title: 'Dramatic',
    name: 'dramatic'
  },
  {
    title: 'Uplifting',
    name: 'uplifting'
  },
  {
    title: 'Tense',
    name: 'tense'
  },
]

export default data;