import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { login } from "../../redux/actions/admin/auth";
import LoginForm from "../../components/admin/auth/LoginForm";
import { Layout, message } from "antd";

const { Content } = Layout;

class LoginAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount = async () => {
    if( localStorage.getItem("jwt_token_admin")){
      Router.push('/admin/dashboard');
    }else{
      Router.push('/admin');
    }
  };

  onFinish = async values => {
    await this.props.login(values);
    if( this.props.isLogined){
      message.success("Login successfully!")
      Router.push("/admin/dashboard");
    }else{
      message.error("Username or password incorrect!")
    }
  };

  render() {
    return (
      <Layout className="home login">
        <Content>
          <LoginForm onFinish={this.onFinish} />
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    isLogined: store.adminAuth.isLogined,
  };
};

const mapDispatchToProps = {
  login,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginAdmin);
