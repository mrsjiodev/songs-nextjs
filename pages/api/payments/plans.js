import nextConnect from 'next-connect'
import Stripe  from 'stripe';
const stripe = new Stripe(process.env.STRIPE_SECRET);

const hanlder = nextConnect();

hanlder.get(async(req, res) => {
  let plans = await stripe.plans.list();
  res.status(200).json(plans.data)
})

export default hanlder;