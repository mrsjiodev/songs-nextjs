// packages
import { useRouter } from "next/router";
import Link from "next/link";
// ui
import { Button } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";

const Standard = props => {
  const { type } = props;

  const router = useRouter();
  const { slug } = router.query;

  return (
    <div className="standard">
      <h3 className="title">Standard</h3>

      <div className="options">
        <div className="option-item">
          <label htmlFor="">Unlimited Music Licenses</label>
          <CheckOutlined />
        </div>
        <div className="option-item">
          <label htmlFor="">New Music Added Weekly</label>
          <CheckOutlined />
        </div>
        <div className="option-item">
          <label htmlFor="">Lifetime use for the songs you download</label>
          <CheckOutlined />
        </div>
        <div className={`${slug === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Unlimited use in paid advertising</label>
          {slug === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
        <div className={`${slug === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Sound Effects</label>
          {slug === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
        <div className={`${slug === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Songs with stems</label>
          {slug === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
      </div>

      <div className="actions">
        <Link href={`/payments/${slug === "basic" ? "pro" : "basic"}`}>
          <a className={`${slug === "basic" ? "btn-basic" : "btn-pro"}`}>
            Switch to {slug === "basic" ? "Pro" : "Basic"}
          </a>
        </Link>
      </div>
    </div>
  );
};

export default Standard;
