import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'

const hanlder = nextConnect();
const Moods = require('../../../models/Moods');

hanlder.get(async (req, res) => {
  let moods = await Moods.find().exec();

  res.status(200).json(moods)
})

hanlder.post(async (req, res) => {
  try {
    const { title, name } = req.body;

    let mood = new Moods({
      title,
      name
    });

    await mood.save();

    res.status(200).json(mood)
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Created mood failed" })
  }
})

export default connectDb(hanlder);