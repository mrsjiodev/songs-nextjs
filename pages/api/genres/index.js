import connectDb from '../../../middleware/dbMiddleware'
import nextConnect from 'next-connect';
import Genres from '../../../models/Genres';
import checkAuth from '../../../middleware/checkLogined'
import uploadMulterS3 from '../../../middleware/uploadMulterS3';

const hanlder = nextConnect();

export const config = {
  api: {
    bodyParser: false
  }
}

// all
hanlder.get(async (req, res) => {
  try {
    let genres = await Genres.find().exec();

    res.status(200).json(genres);
  } catch (err) {
    console.log(err)
    res.status(400).json({ msg: "Get data genres failed!" })
  }
})

// hanlder.use(checkAuth);
// create
hanlder.post(uploadMulterS3.array('files', 1), async (req, res) => {
  try {
    let files = req.files;
    let photoMD = null;
    let F0 = files[0];
    photoMD = F0;

    const { title, name } = req.body;

    let genre = new Genres({
      title,
      name,
      image: photoMD,
    });
    await genre.save();

    res.status(200).json(genre);
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: 'Created failed!' })
  }
})

export default hanlder;