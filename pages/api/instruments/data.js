export const data = [
  {
    title: 'Piano',
    name: 'piano'
  },
  {
    title: 'Keys',
    name: 'keys'
  },
  {
    title: 'Synth',
    name: 'synth'
  },
  {
    title: 'Vocals',
    name: 'vocals'
  },
  {
    title: 'Acoustic',
    name: 'acoustic'
  },
  {
    title: 'Electric',
    name: 'electric'
  },
  {
    title: 'Drums',
    name: 'drums'
  },
  {
    title: 'Percussion',
    name: 'percussion'
  },
  {
    title: 'Claps & Snaps',
    name: 'claps_snaps'
  },
  {
    title: 'Bells',
    name: 'bells'
  },
  {
    title: 'Whistle',
    name: 'whistle'
  },
  {
    title: 'Strings',
    name: 'strings'
  },
  {
    title: 'Orchestra',
    name: 'orchestra'
  },
  {
    title: 'Wind Instruments',
    name: 'wind_instruments'
  },
  {
    title: 'Ethnic',
    name: 'ethnic'
  },
]

export default data;