import React from "react";
import { connect } from "react-redux";
// actions
import { allAnswersAndQuestions } from "../../redux/actions/answers";
// components
import HeaderPage from "../../components/layouts/Header";
import FooterPage from "../../components/home/FooterWelcome";
import AskedQuestions from "../../components/support/AskedQuestions";
// ui
import { Layout, message } from "antd";

const { Header, Content } = Layout;

class TermsOfService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.props.allAnswersAndQuestions();
  };


  render() {
    const { answers_questions } = this.props;

    return (
      <Layout className="home support">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>
        <Content className="content-page">
          <div className="container">
            <div className="support-ctn">
              Contact Us
            </div>
          </div>
        </Content>
        <FooterPage />
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    // answers
    answers_questions: store.answers.answers_questions,
  };
};

const mapDispatchToProps = {
  // answers
  allAnswersAndQuestions,
};

export default connect(mapStateToProps, mapDispatchToProps)(TermsOfService);
