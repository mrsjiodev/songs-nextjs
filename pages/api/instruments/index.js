import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import Instruments from '../../../models/Instruments'

const hanlder = nextConnect();


hanlder.get(async (req, res) => {
  let moods = await Instruments.find().exec();

  res.status(200).json(moods)
})

hanlder.post(async (req, res) => {
  try {
    const { title, name } = req.body;

    let mood = new Instruments({
      title,
      name
    });

    await mood.save();

    res.status(200).json(mood)
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Created Instrument failed" })
  }
})

export default connectDb(hanlder);