import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Answers = require("../../../../../models/Answers");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Answers.findOneAndDelete({ _id: id }, (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Answers Not found" });
    else return res.status(200).json({ message: "Delete Answers successfully" });
  });
});

export default connectDb(handler);
