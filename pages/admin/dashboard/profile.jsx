import React from "react";
import { connect } from "react-redux";
import Router from 'next/router'
import { getProfile } from "../../../redux/actions/users";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { Layout } from "antd";

const { Content } = Layout;

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    if (!localStorage.getItem("jwt_token_admin")) {
      Router.push("/admin");
    }
  };

  render() {
    return (
      <Layout className="dashboard">
        <HeaderMain/>

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <h1>Profile Admin</h1>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user,
    isNeedLogin: store.users.isNeedLogin
  };
};

const mapDispatchToProps = {
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
