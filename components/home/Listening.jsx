// packages
import Link from "next/link";
// ui
import { Row, Button, Col } from "antd";

const Listening = props => {
  return (
    <div className="listening">
      <div className="listening-ctn">
        <Row gutter={48}>
          <Col lg={14} xs={24}>
            <div className="info">
              <h2>Thanks for listening</h2>
              <p>
                Save tracks, follow artists and build playlists. All for free
              </p>
            </div>
          </Col>
          <Col lg={10} xs={24}>
            <div className="btn-auth">
              <Link href="/auth/register">
                <a className="btn-res">Register</a>
              </Link>
              <Link href="/auth/login">
                <a className="btn-log">Login</a>
              </Link>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Listening;
