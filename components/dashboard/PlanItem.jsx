// packages
import Router from "next/router";
// ui
import { Button } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";

const PlanItem = props => {
  const { type, page, user } = props;

  const onStartNow = () => {
    if (props.page === "welcome") {
      Router.push("/plans");
    } else {
      Router.push(`/payments/${type}`);
    }
  };

  return (
    <div className="plan-item">
      <h6 className={`${type === "basic" ? "basic" : "pro"}`}>
        {type === "basic" ? "Basic" : "Pro"}
      </h6>
      <h2>
        <span className="current">$</span>
        {type === "basic" ? "10.00" : "35.00"}
        <span className="mo">/mo</span>
      </h2>

      <div className="options">
        <div className="option-item">
          <label htmlFor="">Unlimited Music Licenses</label>
          <CheckOutlined />
        </div>
        <div className="option-item">
          <label htmlFor="">New Music Added Weekly</label>
          <CheckOutlined />
        </div>
        <div className="option-item">
          <label htmlFor="">Lifetime use for the songs you download</label>
          <CheckOutlined />
        </div>
        <div className={`${type === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Unlimited use in paid advertising</label>
          {type === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
        <div className={`${type === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Sound Effects</label>
          {type === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
        <div className={`${type === "basic" ? "disable" : ""} option-item`}>
          <label htmlFor="">Songs with stems</label>
          {type === "basic" ? <CloseOutlined /> : <CheckOutlined />}
        </div>
      </div>

      <div className="actions">
        <Button
          disabled={user && user.type_standard === type}
          className={`${type === "basic" ? "btn-basic" : "btn-pro"} ${
            user && user.type_standard === type ? "btn-current" : ""
          }`}
          onClick={onStartNow}
        >
          {page === "welcome"
            ? "Join Now"
            : user && user.type_standard === type
            ? "Current"
            : "Pay Now"}
        </Button>
      </div>
    </div>
  );
};

export default PlanItem;
