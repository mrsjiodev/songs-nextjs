import React from "react";
import { Upload, message, Button, Spin } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileAudio } from "@fortawesome/free-solid-svg-icons";

const { Dragger } = Upload;

class UploadAudio extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { dataFile, isUploadingAudio } = this.props;

    return (
      <Spin spinning={isUploadingAudio}>
        <Dragger
          name="audio"
          // disabled={disabledUpload}
          accept="audio/*"
          showUploadList={false}
          multiple={false}
          // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          onChange={info => this.props.onChangeFile(info, "audio")}
          onRemove={info => this.props.onRemoveFile(info, "audio")}
          beforeUpload={() => this.props.beforeUpload()}
        >
          {/* <div className="choose-file">
          <Button type="default">Choose File</Button> <span>No file chosen</span>
        </div> */}
          {dataFile ? (
            <div className="view-song-uploaded">
              <FontAwesomeIcon
                icon={faFileAudio}
                style={{ fontSize: "48px", color: "#40a9ff", height: 150 }}
              />
              <h3>{dataFile.name}</h3>
            </div>
          ) : (
            <>
              <p className="ant-upload-drag-icon">
                <UploadOutlined />
              </p>
              <p className="ant-upload-text">
                Drag and Drop or Select Your Audio File
              </p>
            </>
          )}
        </Dragger>
      </Spin>
    );
  }
}

export default UploadAudio;
