import request from '../../config/request';

export function getAllInstruments() {
  return dispatch => {
    return request().get('admin/instruments/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_INSTRUMENTS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteInstrument(id) {
  return dispatch => {
    return request().delete(`admin/instruments/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_INSTRUMENTS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateInstrument(id,data) {
  return dispatch => {
    return request().put(`admin/instruments/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllInstruments,
  deleteInstrument,
  updateInstrument
}