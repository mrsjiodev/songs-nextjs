const initData = {
  isCreated: false,
  isSubscription: false,
  customer: null,
  subscription: null,
  payment_intent: null,
  plans: [],
  isSaveInfoPaypal: false,
  paypalInfo: null,
  latest_invoice: null,
  payment_intent: null,
  isUpdatedCustomer: false,
  isChangePaymentMethod: false,
  isChangeBillingAddress: false,
  isSetDefaultMethod: false,
  isDeletedPaymentMethod: false,
  // discount
  isValidDiscount: false,
  discountInfo: null,
  msgErr: '',
  isErr: false,
  invoices: []
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_STRIPE_CREATED_CUSTOMER':
      return {
        ...state,
        isCreated: true,
        customer: action.data
      }
    case 'ERROR_STRIPE_CREATED_CUSTOMER':
      return {
        ...state,
        isCreated: false,
        customer: null,
        subscription: null,
      }
    case 'ACTION_STRIPE_ALL_PLANS':
      return {
        ...state,
        plans: action.data
      }
    case 'ERROR_STRIPE_ALL_PLANS':
      return {
        ...state,
        plans: []
      }
    case 'ACTION_STRIPE_SUBSCRIPTIONS_CREATED':
      return {
        ...state,
        isSubscription: true,
        subscription: action.data,
        latest_invoice: action.data.latest_invoice,
        payment_intent: action.data.latest_invoice.payment_intent,
      }
    case 'ERROR_STRIPE_SUBSCRIPTIONS_CREATED':
      return {
        ...state,
        isSubscription: false,
        subscription: null,
        msgErr: action.msg,
        isErr: true
      }
    case 'ACTION_SAVE_INFO_PAYPAL':
      return {
        ...state,
        isSaveInfoPaypal: true,
        paypalInfo: action.data
      }
    case 'ERROR_SAVE_INFO_PAYPAL':
      return {
        ...state,
        isSaveInfoPaypal: false,
        paypalInfo: null
      }
    case 'ACTION_DISCOUNT_CHECKCODE':
      return {
        ...state,
        isValidDiscount: true,
        discountInfo: action.data,
      }
    case 'ERROR_DISCOUNT_CHECKCODE':
      return {
        ...state,
        isValidDiscount: false,
        discountInfo: null
      }
    case 'ACTION_STRIPE_UPDATED_CUSTOMER':
      return {
        ...state,
        isUpdatedCustomer: true
      }
    case 'ERROR_STRIPE_UPDATED_CUSTOMER':
      return {
        ...state,
        isUpdatedCustomer: false
      }
    case 'ACTION_ALL_INVOICES':
      return {
        ...state,
        invoices: action.data
      }
    case 'ERROR_ALL_INVOICES':
      return {
        ...state,
        invoices: []
      }
    case 'ACTION_CHANGE_PAYMENT_METHOD':
      return {
        ...state,
        isChangePaymentMethod: true
      }
    case 'ERROR_CHANGE_PAYMENT_METHOD':
      return {
        ...state,
        isChangePaymentMethod: false
      }
    case 'ACTION_CHANGE_BILLING_ADDRESS':
      return {
        ...state,
        isChangeBillingAddress: true,
      }
    case 'ERROR_CHANGE_BILLING_ADDRESS':
      return {
        ...state,
        isChangeBillingAddress: false
      }
    case 'ACTION_SET_DEFAULT_PAYMENT_METHOD':
      return {
        ...state,
        isSetDefaultMethod: true
      }
    case 'ERROR_SET_DEFAULT_PAYMENT_METHOD':
      return {
        ...state,
        isSetDefaultMethod: false
      }
    case 'ACTION_DELETED_PAYMENT_METHOD':
      return {
        ...state,
        isDeletedPaymentMethod: true
      }
    case 'ERROR_DELTED_PAYMENT_METHOD':
      return {
        ...state,
        isDeletedPaymentMethod: false
      }
    default:
      return {
        ...state,
        isCreated: false,
        isSubscription: false,
        isValidDiscount: false,
        isDeletedPaymentMethod: false,
        payment_intent: null,
        latest_invoice: null,
        isErr: false,
      }
  }
}