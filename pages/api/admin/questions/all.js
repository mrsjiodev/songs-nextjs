import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Questions = require('../../../../models/Questions');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let questions = await Questions.find({})
  .populate({
    path: "created_by",
    model: "Users",
  })
  .exec();
  res.status(200).json(questions);
})

export default connectDb(handler);