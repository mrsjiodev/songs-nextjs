import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Supports = require("../../../../../models/Supports");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let support = await Supports.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(support);
});

export default connectDb(handler);
