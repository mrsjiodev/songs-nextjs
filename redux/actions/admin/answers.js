import request from '../../config/request';

export function createAnswer(data) {
  return dispatch => {
    return request().post(`admin/answers`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_ANSWERS",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
    })
  }
}

export function getAllAnswers() {
  return dispatch => {
    return request().get('admin/answers/all').then(res => {
      dispatch({
        type: "ACTION_GET_ANSWERS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteAnswer(id) {
  return dispatch => {
    return request().delete(`admin/answers/delete/${id}`).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateAnswer(id,data) {
  return dispatch => {
    return request().put(`admin/answers/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  createAnswer,
  getAllAnswers,
  deleteAnswer,
  updateAnswer
}