import react from "react";
// components
import HeaderPage from "../../components/layouts/Header";
import ForgotPasswordForm from "../../components/auth/ForgotPasswordForm";
// ui
import { Layout, message, Spin } from "antd";

const { Header, Content } = Layout;

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Layout className="home login">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>

        <Content>
          <ForgotPasswordForm />
        </Content>
      </Layout>
    );
  }
}

export default ForgotPassword;
