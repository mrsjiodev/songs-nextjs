import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { getProfile } from "../../../redux/actions/users";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { Layout, DatePicker, Button, Table } from "antd";
import moment from "moment";
import { getAnalyticsUser } from "../../../redux/actions/admin/analytics";

const { RangePicker } = DatePicker;
const { Content } = Layout;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isViewTale: false };
  }

  componentDidMount = async () => {
    if (!localStorage.getItem("jwt_token_admin")) {
      Router.push("/admin");
    }
    this.viewAnalytics()
  };

  onChange = (dates, dateStrings) => {
    this.setState({ from: dateStrings[0], to: dateStrings[1] });
  };

  viewAnalytics = async () => {
    const { from, to } = this.state;
    if (!from && !to) {
      await this.props.getAnalyticsUser();
    } else await this.props.getAnalyticsUser(`${from}&${to}`);
    this.setState({ isViewTale: true });
  };

  render() {
    const { analyticsUser } = this.props;

    const dataSource = [
      {
        name: "User Sign-Up",
        today: `${analyticsUser.totalUserToday}`,
        total: analyticsUser.totalUser,
      },
      {
        name: "User Register Plan Basic",
        total: `${analyticsUser.totalUserPlanBasic} (${analyticsUser.percentTUserPlanBasic}%)`,
      },
      {
        name: "User Register Plan Pro",
        total: `${analyticsUser.totalUserPlanPro} (${analyticsUser.percentTUserPlanPro}%)`,
      },
      {
        name: "User Not Register Plan",
        total: `${analyticsUser.totalUserNoPlan} (${analyticsUser.percentTUserNoPlan}%)`,
      },
    ];
    const columns = [
      {
        title: "",
        dataIndex: "name",
        key: "name",
      },
      {
        title: <h3 style={{ color: "#01bcd4" }}>Today</h3>,
        dataIndex: "today",
        key: "today",
      },
      {
        title: <h3 style={{ color: "#01bcd4" }}>Total</h3>,
        dataIndex: "total",
        key: "total",
      },
    ];
    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <h1 style={{ marginBottom: 30 }}>
                View analytics of all users.
              </h1>
              {/* Temporarily hide RangePicker */}
              {/* <div style={{ margin: "0 auto" }}>
                <RangePicker
                  ranges={{
                    Today: [moment(), moment()],
                    "This Month": [
                      moment().startOf("month"),
                      moment().endOf("month"),
                    ],
                  }}
                  onChange={this.onChange}
                />
              </div> */}
              {/* <Button
                type="primary"
                block
                style={{
                  width: "20%",
                  margin: "0 auto",
                  marginTop: 30,
                  marginBottom: 50,
                }}
                onClick={this.viewAnalytics}
              >
                View Analytics
              </Button> */}
              <Table
                style={{ display: this.state.isViewTale ? "block" : "none" }}
                dataSource={dataSource}
                columns={columns}
                pagination={{
                  hideOnSinglePage: true,
                }}
              />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.users.user,
    isNeedLogin: store.users.isNeedLogin,
    analyticsUser: store.adminAnalytics.analyticsUser,
  };
};

const mapDispatchToProps = {
  getProfile,
  getAnalyticsUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
