// packages
import { loadStripe } from "@stripe/stripe-js";
import {
  CardElement,
  Elements,
  useStripe,
  useElements
} from "@stripe/react-stripe-js";
// components
import CheckoutForm from "./CheckoutForm";
// ui

const stripePromise = loadStripe(process.env.STRIPE_KEY);

const PaymentForm = props => {
  const { type, plan_id, currency } = props;

  return (
    <div className="payments-form">
      <Elements stripe={stripePromise}>
        
        <CheckoutForm
          amount_plan={props.amount_plan}
          onLoading={props.onLoading}
          plan_id={plan_id}
          currency={currency}
          type={type}
        />
      </Elements>
    </div>
  );
};

export default PaymentForm;
