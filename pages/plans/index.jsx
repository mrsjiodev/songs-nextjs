import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
// actions
import { allAnswersAndQuestions } from "../../redux/actions/answers";
import { getProfile } from "../../redux/actions/users";
// comonents
import HeaderMain from "../../components/layouts/Header";
import OurPricing from "../../components/home/OurPricing";
import Subcrtiption from "../../components/home/Subcrtiption";
// ui
import { Layout, Row, Col } from "antd";

const { Content, Header } = Layout;

class Plans extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    await this.props.allAnswersAndQuestions();
    await this.props.getProfile();
    (await this.props.isNeedLogin) && Router.push("/auth/login");
  };

  render() {
    return (
      <Layout className="home plans db-page">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderMain />
        </Header>
        <Content className="content-page">
          <div className="content-plan">
            <OurPricing pageMain="plans" user={this.props.user} />
            <Subcrtiption
              pageMain="plans"
              answers_questions={this.props.answers_questions}
            />
          </div>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user,
    isNeedLogin: store.users.isNeedLogin,
    // answers
    answers_questions: store.answers.answers_questions
  };
};

const mapDispatchToProps = {
  getProfile,
  allAnswersAndQuestions
};

export default connect(mapStateToProps, mapDispatchToProps)(Plans);
