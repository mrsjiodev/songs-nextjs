import { Table, Input, Button, Popconfirm, Form, Layout, Modal } from "antd";
import React from "react";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { connect } from "react-redux";
import moment from "moment";
import Router from "next/router";
import {
  getAllQuestion,
  deleteQuestion,
  updateQuestion,
} from "../../../redux/actions/admin/questions";
import { createQuestion } from "../../../redux/actions/admin/questions";
const { TextArea } = Input;

const { Content } = Layout;

class Question extends React.Component {
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null,
    sortedInfo: null,
    dataSource: [],
    count: 2,
    isShowModalAdd: false,
    selectedRowKeys: [],
  };

  deleteAll = () => {
    this.state.selectedRowKeys.map(async (item, index) => {
      await this.props.deleteQuestion(item);
    });
    this.getDataFromAPI();
    this.setState({ selectedRowKeys: [] });
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleDelete = async (key) => {
    await this.props.deleteQuestion(key);
    this.getDataFromAPI();
  };

  handleAdd = async () => {
    this.setState({
      isShowModalAdd: true,
    });
  };

  onCancelDelete = () => {
    this.setState({ selectedRowKeys: [] });
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  getDataFromAPI = async () => {
    await this.props.getAllQuestion();
    this.setState({ dataSource: this.props.questions });
    const { dataSource } = this.state;
    dataSource.map((item, index) => {
      item.key = item._id;
    });
    this.setState({ dataSource });
  };

  componentDidMount = async () => {
    if (localStorage.getItem("jwt_token_admin")) {
      this.getDataFromAPI();
    } else {
      Router.push("/admin");
    }
  };

  onCancel = () => {
    this.setState({
      isShowModalAdd: false,
      isShowModalEdit: false,
    });
  };
  handleFinish = async (values) => {
    console.log({ values });
    this.props.createQuestion(values);
    this.getDataFromAPI();
    this.setState({ isShowModalAdd: false });
  };

  edit = async (item) => {
    await this.setState({ question: item.question, id: item._id });
    await this.setState({
      isShowModalEdit: true,
    });
  };

  handleEdit = async () => {
    const value = {
      question: this.state.question,
    };
    await this.props.updateQuestion(this.state.id, value);
    await this.getDataFromAPI();
    this.setState({
      isShowModalEdit: false,
    });
  };

  onChange = (e) => {
    console.log(e.target.value);
    this.setState({ question: e.target.value });
  };

  render() {
    let { sortedInfo, filteredInfo, selectedRowKeys } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "Question",
        dataIndex: "question",
        key: "question",
        sorter: (a, b) => a.question.localeCompare(b.question),
        sortOrder: sortedInfo.columnKey === "question" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("question"),
      },
      {
        title: "Create by",
        dataIndex: "created_by",
        key: "created_by",
        render: (created_by) =>
          created_by &&
          created_by.first_name &&
          created_by.last_name &&
          `${created_by.first_name} ${created_by.last_name}`,
        sorter: (a, b) =>
          created_by &&
          created_by.first_name &&
          created_by.last_name &&
          `${a.created_by.first_name} ${a.created_by.last_name}`.localeCompare(
            `${b.created_by.first_name} ${b.created_by.last_name}`
          ),
        sortOrder: sortedInfo.columnKey === "created_by" && sortedInfo.order,
      },
      {
        title: "Updated",
        dataIndex: "updatedAt",
        key: "updatedAt",
        // render: name => `${name.first} ${name.last}`,
        sorter: (a, b) =>
          moment(a.updatedAt).unix() - moment(b.updatedAt).unix(),
        sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
        ...this.getColumnSearchProps("updatedAt"),
      },
      {
        title: "Action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <a
              onClick={() => {
                this.edit(record);
              }}
              style={{ marginRight: 15 }}
            >
              Edit
            </a>
            {this.state.dataSource.length >= 1 ? (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            ) : null}
          </span>
        ),
      },
    ];

    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <div>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                  }}
                >
                  Add Question
                </Button>

                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalEdit}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleEdit}
                  >
                    <h1>Edit Question</h1>
                    <TextArea
                      rows={4}
                      placeholder="Question"
                      value={this.state.question}
                      onChange={this.onChange}
                    />

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                        style={{ marginTop: 20 }}
                      >
                        Edit
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalAdd}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleFinish}
                  >
                    <h1>Create Question</h1>
                    <Form.Item
                      name="question"
                      rules={[
                        {
                          required: true,
                          message: "Please input your question!",
                        },
                      ]}
                    >
                      <TextArea rows={4} placeholder="Question" />
                    </Form.Item>
                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                      >
                        Create
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <div style={{ marginBottom: 16 }}>
                  <Popconfirm
                    disabled={!hasSelected}
                    title="Sure to delete?"
                    onConfirm={this.deleteAll}
                    onCancel={this.onCancelDelete}
                  >
                    <Button type="primary" disabled={!hasSelected}>
                      Delete All
                    </Button>
                  </Popconfirm>
                  <span style={{ marginLeft: 8 }}>
                    {hasSelected
                      ? `Selected ${selectedRowKeys.length} questions`
                      : ""}
                  </span>
                </div>
                <Table
                  rowSelection={rowSelection}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "50", "100"],
                  }}
                  bordered
                  dataSource={this.state.dataSource}
                  onChange={this.handleChange}
                />
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    questions: store.adminQuestion.questions,
  };
};

const mapDispatchToProps = {
  getAllQuestion,
  deleteQuestion,
  updateQuestion,
  createQuestion,
};

export default connect(mapStateToProps, mapDispatchToProps)(Question);
