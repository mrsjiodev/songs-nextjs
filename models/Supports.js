const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SupportSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'Users' },
  description: { type: String }
}, {
  timestamps: true
})

module.exports = mongoose.models.Supports || mongoose.model('Supports', SupportSchema);