// packages
import Link from "next/link";
// ui

const ListenNow = props => {
  return (
    <div className="listen-now">
      <div className="listen-ctn">
        <h1>Songs for your videos without that “Stock” feeling</h1>
        <p>Check out which tracks are trending now in the songs community</p>
        <Link href="/explore">
          <a>Listen Now</a>
        </Link>
      </div>
    </div>
  );
};

export default ListenNow;
