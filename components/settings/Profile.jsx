import React from "react";
import { connect } from "react-redux";
// actions
import { updateProfile, getProfile } from "../../redux/actions/users";
// ui
import { Upload, message, Button, Form, Input, Row, Col, Spin } from "antd";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUrl: "/images/icons/user.png",
      loading: false,
      file: null,
      isLoading: false
    };
    this.formRef = React.createRef();
  }

  componentDidUpdate = async (nextProps, nextState) => {
    if (nextProps.data !== this.props.data) {
      const { data } = this.props;
      if (data) {
        if(data.avatar){
          this.setState({ imageUrl: `${URL_MEDIA}${data.avatar.key}` });
        }


        await this.formRef.current.setFieldsValue({
          first_name: data.first_name || "",
          last_name: data.last_name || "",
          company_name: data.company_name || "",
          email: data.email || "",
          avatar: data.avatar && data.avatar.key || ""
        });
      }
    }
  };

  componentDidMount = async () => {
    const { data } = this.props;
    if (data) {
      if(data.avatar){
        this.setState({ imageUrl: `${URL_MEDIA}${data.avatar.key}` });
      }

      await this.formRef.current.setFieldsValue({
        first_name: data.first_name || "",
        last_name: data.last_name || "",
        company_name: data.company_name || "",
        email: data.email || "",
        avatar: data.avatar && data.avatar.key || ""
      });
    }
  };

  onChangeAvatar = info => {
    if (info.file.status !== "uploading") {
      this.setState({ loading: true });
    }
    if (info.file.status === "done") {
      this.getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
          file: info.file
        })
      );
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  onFinish = async values => {
    this.setState({ isLoading: true });
    console.log("values: ", values);

    const { first_name, last_name, email, new_password, company_name } = values;

    let fd = new FormData();
    fd.append("first_name", first_name);
    fd.append("last_name", last_name);
    fd.append("company_name", company_name);
    fd.append("email", email);
    fd.append("password", new_password);
    if (this.state.file) {
      fd.append("avatar", this.state.file.originFileObj);
    }

    await this.props.updateProfile(fd);
    (await this.props.isUpdatedProfile)
      ? message.success("Updated profile successfully!")
      : message.error("Updated profile failed!");
    (await this.props.isUpdatedProfile) && this.props.getProfile();

    await this.setState({ isLoading: false });
  };

  render() {
    const { imageUrl, isLoading } = this.state;

    return (
      <Spin spinning={isLoading}>
        <div className="profile">
          <Form onFinish={this.onFinish} ref={this.formRef}>
            <Row gutter={48}>
              <Col xs={24}>
                <Form.Item
                  name="avatar"
                  rules={[
                    {
                      required: true,
                      message: "Please upload avatar"
                    }
                  ]}
                >
                  <div className="st-avatar">
                    <Spin spinning={this.state.loading}>
                      <img src={imageUrl} alt="" />
                    </Spin>

                    <Upload
                      className="btn-upload-avatar"
                      accept="image/png, imgage/jpg, image/jpeg"
                      name="avatar"
                      // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                      // headers={{ authorization: "authorization-text" }}
                      onChange={info => this.onChangeAvatar(info)}
                      beforeUpload={this.beforeUpload}
                      showUploadList={false}
                    >
                      <Button>Upload Avatar</Button>
                    </Upload>
                  </div>
                </Form.Item>
              </Col>
              <Col md={12} xs={24}>
                <Form.Item
                  label="First Name"
                  name="first_name"
                  rules={[
                    { required: true, message: "Please input your first name!" }
                  ]}
                >
                  <Input placeholder="" />
                </Form.Item>
              </Col>
              <Col md={12} xs={24}>
                <Form.Item
                  label="Last Name"
                  name="last_name"
                  rules={[
                    { required: true, message: "Please input your last name!" }
                  ]}
                >
                  <Input placeholder="" />
                </Form.Item>
              </Col>
              <Col md={24} xs={24}>
                <Form.Item
                  label="Company Name"
                  name="company_name"
                  rules={[
                    {
                      required: true,
                      message: "Please input your company name!"
                    }
                  ]}
                >
                  <Input placeholder="" />
                </Form.Item>
              </Col>
              <Col md={12} xs={24}>
                <Form.Item
                  label="Email"
                  name="email"
                  type="email"
                  rules={[
                    { required: true, message: "Please input your email!" }
                  ]}
                >
                  <Input placeholder="" disabled />
                </Form.Item>
              </Col>
              <Col md={12} xs={24}>
                <Form.Item
                  label="New Password"
                  name="new_password"
                  rules={[
                    { required: true, message: "Please input new password!" }
                  ]}
                >
                  <Input placeholder="" type="password" />
                </Form.Item>
              </Col>
              <Col md={24} xs={24}>
                <div className="action-up">
                  <Button htmlType="submit" type="primary">
                    Update
                  </Button>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    isUpdatedProfile: store.users.isUpdatedProfile,
    user: store.users.user
  };
};

const mapDispatchToProps = {
  updateProfile,
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
