import connectDb from "../../../../../middleware/dbMiddleware";
import uploadMulterS3 from "../../../../../middleware/uploadMulterS3";
const nextConnect = require("next-connect");
const Songs = require("../../../../../models/Songs");
const handler = nextConnect();

export const config = {
  api: {
    bodyParser: false,
  },
};

handler.put(uploadMulterS3.single("image"), async (req, res) => {
  const {
    query: { id },
  } = req;

  let song = await Songs.findOneAndUpdate(
    { _id: id },
    {
      $set: req.body,
    },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  if(req.file){
    song.image = req.file;
    await song.save();
  }

  return res.status(200).json(song);
});

export default connectDb(handler);
