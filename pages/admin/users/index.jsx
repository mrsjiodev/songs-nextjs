import {
  Table,
  Input,
  Button,
  Popconfirm,
  Form,
  Layout,
  message,
  Modal,
  Row,
  Col,
} from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import React from "react";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { connect } from "react-redux";
import moment from "moment";
import {
  getAllUsers,
  deleteUser,
  updateUser,
} from "../../../redux/actions/admin/users";
import Router from "next/router";
import { register } from "../../../redux/actions/auth";

const { Content } = Layout;
class UserAdmin extends React.Component {
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null,
    sortedInfo: null,
    dataSource: [],
    count: 2,
    isShowModalAdd: false,
    selectedRowKeys: [],
    isShowModalEdit: false,
  };

  deleteAll = () => {
    this.state.selectedRowKeys.map(async (item, index) => {
      await this.props.deleteUser(item);
    });
    this.getDataFromAPI();
    this.setState({ selectedRowKeys: [] });
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleDelete = async (key) => {
    await this.props.deleteUser(key);
    this.getDataFromAPI();
  };

  handleAdd = async () => {
    this.setState({
      isShowModalAdd: true,
    });
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  getDataFromAPI = async () => {
    await this.props.getAllUsers();
    this.setState({ dataSource: this.props.all_users });
    const { dataSource } = this.state;
    dataSource.map((item, index) => {
      item.key = item._id;
    });
    this.setState({ dataSource });
  };

  componentDidMount = async () => {
    if (localStorage.getItem("jwt_token_admin")) {
      this.getDataFromAPI();
    } else {
      Router.push("/admin");
    }
  };

  onCancel = () => {
    this.setState({ isShowModalAdd: false, isShowModalEdit: false });
  };
  handleFinish = async (values) => {
    await this.props.register(values);
    (await this.props.isRegister)
      ? message.success("Create successfully!")
      : message.error("Create failed. Try again!");
    (await this.props.isRegister) && this.getDataFromAPI();
    this.setState({ isShowModalAdd: false });
  };

  edit = async (item) => {
    await this.setState({
      first_name: item.first_name,
      id: item._id,
      last_name: item.last_name,
      email: item.email,
    });
    await this.setState({
      isShowModalEdit: true,
    });
  };

  handleEdit = async () => {
    const value = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
    };
    await this.props.updateUser(this.state.id, value);
    await this.getDataFromAPI();
    this.setState({
      isShowModalEdit: false,
    });
  };

  onChangeFirstName = (e) => {
    this.setState({ first_name: e.target.value });
  };
  onChangeLastName = (e) => {
    this.setState({ last_name: e.target.value });
  };
  onChangeEmail = (e) => {
    this.setState({ email: e.target.value });
  };

  onCancelDelete = () => {
    this.setState({ selectedRowKeys: [] });
  };

  render() {
    let { sortedInfo, filteredInfo, selectedRowKeys } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "First Name",
        dataIndex: "first_name",
        key: "first_name",
        sorter: (a, b) => a.first_name.localeCompare(b.first_name),
        sortOrder: sortedInfo.columnKey === "first_name" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("first_name"),
      },
      {
        title: "Last Name",
        dataIndex: "last_name",
        key: "last_name",
        sorter: (a, b) => a.last_name.localeCompare(b.last_name),
        sortOrder: sortedInfo.columnKey === "last_name" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("last_name"),
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
        sorter: (a, b) => a.email.localeCompare(b.email),
        sortOrder: sortedInfo.columnKey === "email" && sortedInfo.order,
        // ellipsis: true,
        ...this.getColumnSearchProps("email"),
      },
      {
        title: "Role",
        dataIndex: "role",
        key: "role",
        sorter: (a, b) => a.role.localeCompare(b.role),
        sortOrder: sortedInfo.columnKey === "role" && sortedInfo.order,
        ellipsis: true,
        ...this.getColumnSearchProps("role"),
      },
      {
        title: "Updated",
        dataIndex: "updatedAt",
        key: "updatedAt",
        // render: name => `${name.first} ${name.last}`,
        sorter: (a, b) =>
          moment(a.updatedAt).unix() - moment(b.updatedAt).unix(),
        sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
        ...this.getColumnSearchProps("updatedAt"),
      },
      {
        title: "Action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <a
              onClick={() => {
                this.edit(record);
              }}
              style={{ marginRight: 15 }}
            >
              Edit
            </a>
            {this.state.dataSource.length >= 1 ? (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            ) : null}
          </span>
        ),
      },
    ];

    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <div>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                  }}
                >
                  Add User
                </Button>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalEdit}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleEdit}
                  >
                    <h1>Edit User</h1>
                    <Input
                      placeholder="First Name"
                      value={this.state.first_name}
                      onChange={this.onChangeFirstName}
                      style={{ marginBottom: 10 }}
                    />
                    <Input
                      placeholder="Last Name"
                      value={this.state.last_name}
                      onChange={this.onChangeLastName}
                      style={{ marginBottom: 10 }}
                    />
                    <Input
                      placeholder="Email"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                      style={{ marginBottom: 10 }}
                    />
                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                        style={{ marginTop: 20 }}
                      >
                        Edit
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalAdd}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleFinish}
                  >
                    <h1>Create User</h1>
                    <Row gutter={24}>
                      <Col md={12}>
                        <Form.Item
                          name="first_name"
                          rules={[
                            {
                              required: true,
                              message: "Please input your First Name!",
                            },
                          ]}
                        >
                          <Input placeholder="First Name" />
                        </Form.Item>
                      </Col>
                      <Col md={12}>
                        <Form.Item
                          name="last_name"
                          rules={[
                            {
                              required: true,
                              message: "Please input your Last Name!",
                            },
                          ]}
                        >
                          <Input placeholder="Last Name" />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Form.Item
                      name="email"
                      rules={[
                        { required: true, message: "Please input your Email!" },
                      ]}
                    >
                      <Input
                        prefix={
                          <MailOutlined className="site-form-item-icon" />
                        }
                        placeholder="Your Email"
                      />
                    </Form.Item>
                    <Form.Item
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Password!",
                        },
                      ]}
                    >
                      <Input
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        placeholder="Your Password"
                      />
                    </Form.Item>

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                      >
                        Create
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <div style={{ marginBottom: 16 }}>
                  <Popconfirm
                    disabled={!hasSelected}
                    title="Sure to delete?"
                    onConfirm={this.deleteAll}
                    onCancel={this.onCancelDelete}
                  >
                    <Button type="primary" disabled={!hasSelected}>
                      Delete All
                    </Button>
                  </Popconfirm>
                  <span style={{ marginLeft: 8 }}>
                    {hasSelected
                      ? `Selected ${selectedRowKeys.length} users`
                      : ""}
                  </span>
                </div>
                <Table
                  rowSelection={rowSelection}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "50", "100"],
                  }}
                  bordered
                  dataSource={this.state.dataSource}
                  onChange={this.handleChange}
                />
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    all_users: store.adminUser.all_users,
    isRegister: store.auth.isRegister,
  };
};

const mapDispatchToProps = {
  getAllUsers,
  register,
  deleteUser,
  updateUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserAdmin);
