const initData = {
  isCreatedSupport: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_CREATED_SUPPORT':
      return {
        ...state,
        isCreatedSupport: true
      }
    case 'ERROR_CREATED_SUPPORT':
      return {
        ...state,
        isCreatedSupport: false
      }
    default:
      return {
        ...state,
        isCreatedSupport: false
      }
  }
}