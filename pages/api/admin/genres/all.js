import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Genres = require('../../../../models/Genres');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let genre = await Genres.find({})
  .exec();
  res.status(200).json(genre);
})

export default connectDb(handler);