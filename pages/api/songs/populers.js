
import connectDb from '../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')

const Songs = require('../../../models/Songs');

export const config = {
  api: {
    bodyParser: false,
  },
}

const handler = nextConnect();

handler.get(async (req, res) => {
  let top_songs = await Songs.find({}).sort({ total_played: -1 }).limit(6).exec();

  res.status(200).json(top_songs)
})

export default connectDb(handler);