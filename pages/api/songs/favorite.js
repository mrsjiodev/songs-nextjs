import connectDb from '../../../middleware/dbMiddleware';
import checkUser from '../../../middleware/checkLogined';
import nextConnect from 'next-connect';
import Users from '../../../models/Users';
import Songs from '../../../models/Songs';

const handler = nextConnect();

// export const config = {
//   api: {
//     bodyParser: false
//   }
// }

handler.use(checkUser);

handler.post(async (req, res) => {
  const { song_id, isFavorite } = req.body;
  const { _id } = req.user;

  let user = await Users.findById(_id).exec();

  if (isFavorite) {
    // add favorite
    if (user.favorites) {
      let _data = user.favorites;
      _data.push(song_id);
      user.favorites = _data;
    } else {
      user.favorites = [song_id]
    }
  } else {
    // remove favorite
    let _favories = user.favorites.filter(el => {
      return String(el) !== String(song_id)
    })

    user.favorites = _favories;
  }
  await user.save();

  res.status(200).json(user.favorites);
})

export default connectDb(handler)