import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { getProfile } from "../../redux/actions/users";
// comonents
import HeaderMain from "../../components/layouts/Header";
import SiderMain from "../../components/dashboard/layouts/SiderMain";
import DashboardMain from "../../components/dashboard/DashboardMain";
// ui
import { Layout, Menu } from "antd";

const { Header, Content, Footer, Sider } = Layout;

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    await this.props.getProfile();
    await !this.props.user && Router.push('/auth/login');
    await this.props.user.role !== 'admin' && Router.push('/');
  };

  render() {
    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          {/* <SiderMain /> */}

          <Layout className="main-ctn">
            <Content className="content" style={{ margin: "24px 16px 0" }}>
              <DashboardMain />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.users.user,
    isNeedLogin: store.users.isNeedLogin
  };
};

const mapDispatchToProps = {
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
