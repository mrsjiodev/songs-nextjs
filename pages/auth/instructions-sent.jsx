import React from "react";
import Router from "next/router";
// components
import HeaderPage from "../../components/layouts/Header";

// ui
import { Layout, message, Spin, Form, Input, Button } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";

const { Header, Content } = Layout;

class InstructionsSent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    const { token } = Router.query;
    console.log(token);
  };

  render() {
    return (
      <Layout className="home login">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>

        <Content>
          <div className="login-content">
            <div className="instructions">
              <h1>Instructions sent!</h1>
              <p>
                Instructions for resetting your password have been sent to your
                email.
              </p>
              <p>
                You’ll receive this email within 5 minutes. Be sure to check
                your spam folder, too.
              </p>
              {/* <p style={{ fontWeight: 500 }}><strong>Link only exists for 1 hour.</strong></p> */}
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default InstructionsSent;
