import request from '../../config/request';

export function getProfile(){
  return dispatch => {
    return request().get(`admin/users/profile`).then(res => {
      dispatch({
        type: "ACTION_GET_PROFILE_ADMIN",
        data: res.data
      })
    }).catch(err => {
      console.log(err);
    })
  }
}

export function getAllUsers() {
  return dispatch => {
    return request().get('admin/users/all-users').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_USERS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: 'ERROR_GET_ALL_USERS',
      })
    })
  }
}

export function deleteUser(id) {
  return dispatch => {
    return request().delete(`admin/users/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_USER",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateUser(id,data) {
  return dispatch => {
    return request().put(`admin/users/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllUsers,
  deleteUser,
  updateUser,
  getProfile
}