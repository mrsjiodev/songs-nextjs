import Stripe from "stripe";
import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import checkLogin from '../../../middleware/checkLogined'
const stripe = new Stripe(process.env.STRIPE_SECRET);

const hanlder = nextConnect();
const Users = require('../../../models/Users');

hanlder.use(checkLogin);

// set default payment method
hanlder.put(async (req, res) => {
  try {
    const { _id } = req.user;
    const { payment_method_default } = req.body;
    let user = await Users.findById(_id).exec();
    user.payment_method_default = payment_method_default;
    await user.save();

    res.status(200).json({ status: 'success', msg: 'Change default payment method successfully!' })

  } catch (error) {
    res.status(400).json({ status: 'error', msg: 'Change default payment method failed!' })
  }
});

// delete method
hanlder.delete(async (req, res) => {
  try {
    const { _id } = req.user;
    const { type } = req.body;
    let user = await Users.findById(_id).exec();

    if (type === 'card') {
      // Detach a PaymentMethod from a Customer
      let pm = await stripe.paymentMethods.detach(user.payment_method, async (err, paymentMethod) => {
        if (err) {
          console.log(err)
          res.status(400).json({ status: 'error', msg: 'Detach payment method failed' })
        }
        if (paymentMethod) {
          user.payment_method = null;
          user.card_info = null;

          if (user.payment_method_default === 'card') {
            if (user.paypal_info && user.isPaypal) {
              user.payment_method_default = 'paypal';
            } else {
              user.payment_method_default = 'null';
            }
          }
          await user.save();
        }
      })
    } else {
      user.isPaypal = false;
      user.paypal_info = null;
      if(user.payment_method_default === 'paypal'){
        if(user.card_info){
          user.payment_method_default = 'card';
        }
      }

      await user.save();
    }

    res.status(200).json({ status: 'success', msg: 'Deleted payment method successfully!' })

  } catch (error) {
    console.log(error)
    res.status(400).json({ status: 'error', msg: "Deleted payment method failed!" })
  }
})

export default connectDb(hanlder);