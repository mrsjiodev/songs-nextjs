import API from '../../config/request'

export function login(data) {
  return dispatch => {
    return API().post(`admin/signin`, data).then(res => {
      localStorage.setItem("jwt_token_admin", res.data.token);

      dispatch({
        type: "ACTION_LOGINED_ADMIN",
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
      dispatch({
        type: "ERROR_LOGINED_ADMIN",
        msgErr: "Username or password incorrect!"
      })
    })
  }
}

export default {
  login
}