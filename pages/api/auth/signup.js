import connectDb from '../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')

const Users = require('../../../models/Users');

// export const config = {
//   api: {
//     bodyParser: false,
//   },
// }

const handler = nextConnect();

handler.post(async (req, res) => {
  console.log(req.body);
  try {
    const { email, password, first_name, last_name } = req.body;

    let user = new Users({
      email,
      password,
      first_name, last_name
    });

    await user.save();

    res.status(200).json(user);
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: 'Email Email already exists!' })
  }
})

export default connectDb(handler);