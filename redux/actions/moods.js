import request from '../config/request'

export function allMoods(){
  return dispatch => {
    return request().get(`moods`).then(res => {
      dispatch({
        type: "ACTION_ALL_MOODS",
        data: res.data
      })
    }).catch(err => {
      dispatch({
        type: "ERROR_ALL_MOODS"
      })
    })
  }
}

export function createMood(data) {
  return dispatch => {
    return request().post(`moods`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_MOOD",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CREATED_MOOD"
      })
    })
  }
}

export default {
  allMoods,
  createMood
}