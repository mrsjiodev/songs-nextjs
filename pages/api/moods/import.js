import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import data from './data';

const Moods = require('../../../models/Moods');

const hanlder = nextConnect();

hanlder.post(async (req, res) => {
  try {

    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      let mood = new Moods({
        title: item.title,
        name: item.name
      });

      await mood.save();
    }

    res.status(200).json({ msg: 'Import successfully!' })
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Import moods failed" })
  }
})

export default connectDb(hanlder);