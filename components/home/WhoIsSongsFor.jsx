import React from "react";
import { Row, Col } from "antd";

class WhoIsSongsFor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="who-is-songs">
        <h1 className="title">Who is Songs For?</h1>
        <Row gutter={30}>
          <Col xs={24} sm={12} md={12} lg={6}>
            <div className="box-item">
              <img
                src="/images/welcome/video.png"
                alt="video"
                className="img-title"
              />
              <h3>Youtubers</h3>
              <p>
                Whether you have 10,000 subscribers or 10 million; your license
                has you covered. Unlimited Royalty Free Music For All!
              </p>
            </div>
          </Col>
          <Col xs={24} sm={12} md={12} lg={6}>
            <div className="box-item">
              <img
                src="/images/welcome/rolls.png"
                alt="video"
                className="img-title"
              />
              <h3>Filmmakers</h3>
              <p>
                Filmmakers love us for our cinematic tracks, sound fx, and
                soundscapes that add depth and character to your footage.
              </p>
            </div>
          </Col>
          <Col xs={24} sm={12} md={12} lg={6}>
            <div className="box-item">
              <img
                src="/images/welcome/technology.png"
                alt="video"
                className="img-title"
              />
              <h3>Ad Agencies</h3>
              <p>
                Whether you’re creating a superbowl commerical or a youtube ad;
                your license has you covered.
              </p>
            </div>
          </Col>
          <Col xs={24} sm={12} md={12} lg={6}>
            <div className="box-item">
              <img
                src="/images/welcome/music.png"
                alt="video"
                className="img-title"
              />
              <h3>Singers & Rappers</h3>
              <p>
                Need premium instrumentals for your next song? Search through
                our “beats” category and find a track that fits you’re style!
              </p>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default WhoIsSongsFor;
