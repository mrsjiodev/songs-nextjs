import React from "react";
import { connect } from "react-redux";
// actions
import { allAnswersAndQuestions } from "../../redux/actions/answers";
// components
import HeaderPage from "../../components/layouts/Header";
import FooterPage from "../../components/home/FooterWelcome";
import AskedQuestions from "../../components/support/AskedQuestions";
// ui
import { Layout, message } from "antd";

const { Header, Content } = Layout;

class TermsOfService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.props.allAnswersAndQuestions();
  };

  render() {
    const { answers_questions } = this.props;

    return (
      <Layout className="home support">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>
        <Content className="content-page">
          <div className="container">
            <div className="support-ctn other-page">
            <h1>License Agreement</h1>

              <p className="pg-text">
                THE COPYRIGHTED WORK(S) BEING OFFERED UNDER THIS LICENSE IS/ARE
                PROVIDED SOLELY UNDER THE TERMS OF THIS LICENSE ("LICENSE"). THE
                WORK IS PROTECTED BY COPYRIGHT. ANY USE OF THE WORK OTHER THAN
                AS AUTHORIZED BY THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED. BY
                EXERCISING ANY RIGHTS TO THE COPYRIGHTED WORK(S) PROVIDED, YOU
                AGREE TO BE BOUND BY THIS LICENSE. Songs PROVIDES THIS license
                agreement as a courtesy to its users, and does not represent any
                party in any capacity. Each party is advised to obtain its own
                legal representation in connection with this Agreement before
                agreeing to the terms herein.
              </p>
              <p className="pg-text">
                For good and valuable consideration, receipt and sufficiency of
                which is hereby acknowledged, the owner or authorized licensor
                of the Property (“Artist”) hereby grants to
                ___________________________ ("User") a gratis, worldwide,
                non-exclusive, royalty-free, non-transferable license to use,
                copy, publicly perform and display, synchronize with video,
                publish, and distribute the sound recording titled
                “______________” (including the composition or other content
                embodied therein) ("Property"), solely in connection with
                ______________________ (the “Video”), in perpetuity, in any
                medium currently known or later created. For the avoidance of
                doubt, Artist will receive no royalties, whether mechanical,
                public performance, or otherwise, for any use of the Property,
                except in those jurisdictions in which the right to collect
                royalties through any statutory or compulsory licensing scheme
                cannot be waived, in which case Artist reserves the right to
                collect such royalties for any exercise by User of the rights
                granted under this License.{" "}
              </p>
              <p className="pg-text">
                All rights not expressly granted by Artist are hereby reserved.
                User may not sublicense the Property or assign this License.
                User shall not (a) make available, distribute or perform the
                Property separately from content into which the Property has
                been incorporated (e.g., standalone distribution of the Property
                is not permitted); (b) use the Property in connection with
                defamatory, or fraudulent content or in connection with
                pornographic or illegal images, sounds, or content, or any
                depictions of illegal activity whatsoever, whether directly or
                in context or by juxtaposition with other materials; (c) make
                any change in the language of the Property; or (d) change the
                Property, including altering the harmonic structure or melody of
                the Property.
              </p>
              <p className="pg-text">
                In the event User arranges a public performance of the Video in
                connection with any medium that retains valid performance
                licenses from the American Society of Composers Authors and
                Publishers (“ASCAP”), Broadcast Music, Inc. (“BMI”), or other
                applicable performing rights society, User shall deliver to
                Artist a music copyright information sheet with regard to the
                Video within sixty (60) days of the initial commercial broadcast
                of the Video.
              </p>
              <p className="pg-text">
                In connection with each use of the Property, User should use
                reasonable efforts provide, reasonable to the medium: (i) the
                name of the author; (ii) the title; (iii) if supplied, the URL
                associated with the author of the Property (for example, the
                website address for the band); and (iv) a link to Songs.com User
                may not implicitly or explicitly imply any connection with,
                sponsorship or endorsement by Artist of User, User’s use of the
                Property, or any product or service without the separate prior
                written permission of Artist. User shall also incorporate all
                applicable notices of copyright, trademark, or other proprietary
                rights that Artist requires to be incorporated into any media
                that contains or uses the Property. User is hereby granted a
                non-exclusive license to use Artist’s professional name in
                connection with the Property.
              </p>
              <p className="pg-text">
                Artist agrees to defend, indemnify and hold harmless User and
                Songs (which is an intended third-party beneficiary of this
                License), and the parent corporation, officers, directors,
                employees and agents of the foregoing, from and against any and
                all claims, damages, losses, liabilities, costs or debt, and
                expenses (including but not limited to attorney's fees) arising
                from: (i) Artist’s violation of any term of this agreement; and
                (ii) Artist’s violation of any third-party rights, including,
                without limitation, copyright. User agrees to defend, indemnify
                and hold harmless Artist and Songs, and the parent corporation,
                officers, directors, employees and agents of the foregoing, from
                and against any and all claims, damages, losses, liabilities,
                costs or debt, and expenses (including but not limited to
                attorney's fees) arising from: (i) User’s violation of any term
                of this agreement; and (ii) User’s use of the Property.
              </p>
              <p className="pg-text">
                ARTIST OFFERS THE PROPERTY AS-IS AND MAKES NO REPRESENTATIONS OR
                WARRANTIES OF ANY KIND CONCERNING THE PROPERTY, EXPRESS,
                IMPLIED, OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE,
                NONINFRINGEMENT, OR THE ABSENCE OF DEFECTS, WHETHER OR NOT
                DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF
                IMPLIED WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO USER.
                EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN NO EVENT
                WILL ARTIST BE LIABLE TO USER FOR ANY SPECIAL, INCIDENTAL,
                CONSEQUENTIAL, OR PUNITIVE DAMAGES ARISING OUT OF THIS LICENSE
                OR THE USE OF THE PROPERTY.
              </p>
              <p className="pg-text">
                Artist represents, and warrants that Artist has the necessary
                licenses, rights, consents, and permissions to grant the rights
                herein and that User’s use of the Property will not infringe any
                third-party rights. User affirms that he/she is either more than
                eighteen (18) years of age, or an emancipated minor, or possess
                legal parental or guardian consent, and is fully able and
                competent to enter into this License. In any case, User affirms
                that he/she is over the age of 13, as this website is not
                intended for children under 14.
              </p>
              <p className="pg-text">
                This License contains the entire agreement of the parties with
                respect to the subject matter hereof. Each party acknowledges
                that it is not relying upon any warranty, representation, or
                promise made by any other party hereto in agreeing to this
                License. No provision hereof may be waived unless such waiver is
                in writing and signed by each party. Waiver of any one provision
                herein shall not be deemed to be a waiver of any other
                provision. This License may be modified only by a written
                agreement executed by all of the parties. This License and the
                rights and liabilities of the parties, shall in all respects be
                interpreted under the laws of Delaware, and any action to
                enforce or interpret the terms hereof shall be brought
                exclusively in the courts of Delaware. If any provision of this
                License is held to be invalid or unenforceable, the remaining
                provisions shall continue in full force and effect.
              </p>
            </div>
          </div>
        </Content>
        <FooterPage />
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    // answers
    answers_questions: store.answers.answers_questions
  };
};

const mapDispatchToProps = {
  // answers
  allAnswersAndQuestions
};

export default connect(mapStateToProps, mapDispatchToProps)(TermsOfService);
