const initData = {
  genres: [],
  isCreatedGenre: false,
  filterGenre: null
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_ALL_GENRES':
      return {
        ...state,
        genres: action.data
      }
    case 'ERROR_ALL_GENRES':
      return {
        ...state,
        genres: []
      }
    case 'ACTION_CREATED_GENRE':
      return {
        ...state,
        isCreatedGenre: true
      }
    case 'ERROR_CREATED_GENRE':
      return {
        ...state,
        isCreatedGenre: false
      }
    case 'ACTION_SET_FILTER_GENRES':
      return {
        ...state,
        filterGenre: action.data
      }
    default:
      return {
        ...state,
      }
  }
}