const initData = {
  answers_questions: []
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_ALL_ANSWERS_OF_QUESTIONS':
      return {
        ...state,
        answers_questions: action.data
      }
    case 'ERROR_ALL_ANSWERS_OF_QUESTIONS':
      return {
        ...state,
        answers_questions: []
      }
    default:
      return {
        ...state,
      }
  }
}