import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'

const Questions = require('../../../models/Questions');
const handler = nextConnect();

// get all supports
handler.get(async (req, res) => {
  let questions = await Questions.find().exec();
  res.status(200).json(questions)
})

handler.post(async (req, res) => {
  const { _id } = req.user;
  const { question } = req.body;

  let qs = new Questions({
    question,
    created_by: _id
  });

  await qs.save();

  res.status(200).json(qs)
})

export default connectDb(handler)