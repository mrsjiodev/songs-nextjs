import React from "react";
import moment from "moment";
// ui
import { Table } from "antd";

class Invoices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data } = this.props;

    const columns = [
      {
        title: "Amount",
        dataIndex: "amount_paid",
        key: "amount_paid",
        sorter: {
          compare: (a, b) => a.amount_paid - b.amount_paid,
          // multiple: 4,
        },
        render: amount => <span>US ${(amount / 100).toFixed(2)}</span>
      },
      {
        title: "INVOICE NUMBER",
        dataIndex: "number",
        key: "number",
        responsive: ["md"],
        // sorter: {
        //   compare: (a, b) => a.number - b.number,
        //   // multiple: 3,
        // },
        render: (inv, item) => (
          <a href={item.invoice_pdf} download>
            {inv}
          </a>
        )
      },
      {
        title: "CUSTOMER",
        dataIndex: "customer_email",
        key: "customer_email",
        responsive: ["lg"],
        // sorter: {
        //   compare: (a, b) => a.customer_email - b.customer_email,
        //   // multiple: 2,
        // },
      },
      {
        title: "CREATED",
        dataIndex: "created",
        responsive: ["lg"],
        key: "created",
        sorter: {
          compare: (a, b) => a.created - b.created,
          // multiple: 1,
        },
        render: created => {
          let date = new Date(created * 1000);
          return <span>{moment(date).format("DD MMM YYYY, HH:mm", "x")}</span>;
        }
      }
    ];

    return (
      <div className="invoices">
        <Table columns={columns} dataSource={data} />
      </div>
    );
  }
}

export default Invoices;
