import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

// update address customer
hanlder.put(async (req, res) => {
  const { _id } = req.user;
  const { line1, city, country, line2, postal_code, state, fullname } = req.body;

  console.log(req.body);

  let user = await Users.findById(_id).exec();
  console.log(user.customer_stripe_id)
  let customor = await stripe.customers.update(user.customer_stripe_id,
    {
      address: {
        line1, city, country, line2, postal_code, state,
      },
      name: fullname
    },
    async (err, result) => {
      if (err) {
        console.log(err);
        res.status(400).json({ status: 'error', msg: "Updated failed!" })
      }
      if (result) {
        console.log('result: ', result)
        user.billing_details_default = {
          address: {
            city,
            country,
            line1,
            line2,
            postal_code,
            state
          },
          email: user.billing_details_default.email,
          name: fullname,
          phone: user.billing_details_default.phone
        }

        await user.save();

        res.status(200).json(result)
      }
    })
})

export default connectDB(hanlder)