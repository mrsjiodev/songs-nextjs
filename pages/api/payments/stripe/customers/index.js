import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

hanlder.put(async (req, res) => {
  try {
    const { payment_method_id, full_name, city, address, state, country } = req.body;
    const { _id } = req.user;

    let user = await Users.findById(_id).exec();

    let update_customer = await stripe.customers.update(user.customer_stripe_id, {
      invoice_settings: {
        default_payment_method: payment_method_id
      },
      payment_method: payment_method_id,
      name: full_name,
      address: `${address}, ${city}, ${state}, ${country}`
    });

    user.payment_method = payment_method_id;

    await user.save();

    return res.status(200).json(update_customer);
  } catch (err) {
    console.log(err);
    res.status(400).json({ msg: 'Created customer failed!' })
  }
})

export default connectDB(hanlder)