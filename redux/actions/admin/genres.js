import request from '../../config/request';

export function getAllGenres() {
  return dispatch => {
    return request().get('admin/genres/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_GENRES",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteGenre(id) {
  return dispatch => {
    return request().delete(`admin/genres/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_GENRES",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateGenre(id,data) {
  return dispatch => {
    return request().put(`admin/genres/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllGenres,
  deleteGenre,
  updateGenre
}