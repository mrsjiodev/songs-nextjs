import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Users = require("../../../../../models/Users");
const moment = require("moment");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.get(async (req, res) => {
  const {
    query: { time },
  } = req;
  const [fromDate, toDate] = time.split("&");
  let query = {};
  let fromDateHour = new Date(fromDate);
  fromDateHour.setHours(0, 0, 0);
  let toDateHour = new Date(toDate);
  toDateHour.setHours(23, 59, 59);
  if (fromDate && toDate) {
    query["createdAt"] = { $gte: fromDateHour, $lte: toDateHour };
  }
  try {
    const users = await Users.find(query).lean();
    const totalUser = users.length;
    const userNow = await Users.find().lean();
    const usersToday = userNow.filter(
      (t) => t.createdAt && moment(t.createdAt).isSame(Date.now(), "day")
    );
    const totalUserToday = usersToday.length;
    const totalUserNoPlan = userNow.filter((t) => !t.plan_default).length;
    const totalUserPlan = userNow.filter((t) => t.plan_default).length;
    const totalUserPlanBasic = userNow.filter(
      (t) => t.plan_default === "plan_H95DhVoZrTDwpw"
    ).length;
    const totalUserPlanPro = totalUserPlan - totalUserPlanBasic;
    const totalUserPl = totalUserPlan + totalUserNoPlan;
    let percentTodaySignup = 0;
    if (totalUser !== 0) {
      percentTodaySignup = ((totalUserToday / totalUser) * 100).toFixed(2);
    }
    let percentTUserPlanBasic = 0;
    if (totalUserPl !== 0) {
      percentTUserPlanBasic = (
        (totalUserPlanBasic / totalUserPl) *
        100
      ).toFixed(2);
    }
    let percentTUserPlanPro = 0;
    if (totalUserPl !== 0) {
      percentTUserPlanPro = ((totalUserPlanPro / totalUserPl) * 100).toFixed(2);
    }
    let percentTUserNoPlan = 0;
    if (totalUserPl !== 0) {
      percentTUserNoPlan = ((totalUserNoPlan / totalUserPl) * 100).toFixed(2);
    }
    const user = {
      totalUser,
      totalUserToday,
      totalUserNoPlan,
      totalUserPlanBasic,
      totalUserPlanPro,
      percentTodaySignup,
      percentTUserPlanBasic,
      percentTUserPlanPro,
      percentTUserNoPlan,
    };

    res.status(200).json(user);
  } catch (error) {
    console.log(error);
  }
});

export default connectDb(handler);
