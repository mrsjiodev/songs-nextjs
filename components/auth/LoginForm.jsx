import React from "react";
import Router from "next/router";
// ui
import { Form, Input, Button } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import Link from "next/link";

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleFinish = values => {
    this.props.onFinish(values);
  };

  render() {
    return (
      <div className="login-content">
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={this.handleFinish}
        >
          <h1>Welcome back to Songs.</h1>
          <Form.Item
            name="email"
            rules={[{ required: true, message: "Please input your Email!" }]}
          >
            <Input
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Your Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Your Password"
            />
          </Form.Item>
          <Form.Item>
            <div className="actions">
              <Link href="/auth/forgot-password">
                <a className="login-form-forgot">Forgot password</a>
              </Link>
            </div>
          </Form.Item>

          <div className="action-sb">
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Sign in
            </Button>
            <div className="act-register">
              <p>Dont have an account?</p>{" "}
              <a onClick={() => Router.push("/auth/register")}>Register</a>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

export default LoginForm;
