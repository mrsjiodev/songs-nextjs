// ui
import { Input, Row, Col, Button } from "antd";
import { CloseOutlined } from "@ant-design/icons";

const { Search } = Input;

const Filters = props => {
  const {
    onFilter,
    onSearchSongs,
    onChangeKeywork,
    keywork,
    isShowFilter
  } = props;

  return (
    <div className="filters">
      <div className="container">
        <Row gutter={24}>
          <Col md={20} sm={16} xs={12}>
            <Search
              placeholder="Search for songs, artists...."
              onSearch={() => onSearchSongs()}
              onPressEnter={() => onSearchSongs()}
              onChange={e => onChangeKeywork(e.target.value)}
              value={keywork}
            />
          </Col>
          <Col md={4} sm={8} xs={12}>
            <div className="act-filter">
              <Button
                className="btn-filter"
                type="default"
                onClick={() => onFilter()}
              >
                {isShowFilter ? (
                  <CloseOutlined />
                ) : (
                  <img src="./images/icons/filter.png" />
                )}
                Filter
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Filters;
