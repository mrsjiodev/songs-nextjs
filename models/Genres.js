const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const GenreSchema = new Schema({
  title: { type: String },
  name: { type: String },
  image: {type: Object},
}, {
  timestamps: true
})

module.exports = mongoose.models.Genres || mongoose.model('Genres', GenreSchema);