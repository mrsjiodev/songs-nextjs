import Link from "next/link";
import { Layout, Menu, Button } from "antd";
import {
  UserOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UnorderedListOutlined,
  PicLeftOutlined
} from "@ant-design/icons";

const { SubMenu } = Menu;
const { Sider } = Layout;

class LeftMenu extends React.Component {
  state = {
    collapsed: false,
    isSelected: "",
  };
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  // componentDidMount = async () => {
  //   if (typeof window !== "undefined") {
  //     if (window.location.pathname === "/admin/users")
  //       await this.setState({ isSelected: "1" });
  //     if (window.location.pathname === "/admin/songs")
  //       await this.setState({ isSelected: "2" });
  //     if (window.location.pathname === "/admin/mood")
  //       await this.setState({ isSelected: "3" });
  //     if (window.location.pathname === "/admin/theme")
  //       await this.setState({ isSelected: "4" });
  //     if (window.location.pathname === "/admin/genre")
  //       await this.setState({ isSelected: "5" });
  //     if (window.location.pathname === "/admin/instrument")
  //       await this.setState({ isSelected: "6" });
  //   }
  // };

  render() {
    return (
      <Sider collapsedWidth="0">
        <div className="ctn">
          {/* <Button
            type="primary"
            onClick={this.toggleCollapsed}
            style={{ marginBottom: 16 }}
          >
            {React.createElement(
              this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined
            )}
          </Button> */}
          <Menu
            // defaultSelectedKeys={[this.state.isSelected]}
            // defaultOpenKeys={["sub1"]}
            mode="inline"
            theme="dark"
            inlineCollapsed={this.state.collapsed}
          >
            <Menu.Item key="10">
              <Link href="/admin/dashboard">
                <a>
                  <PicLeftOutlined />
                  <span className="nav-text">Analytics</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="1">
              <Link href="/admin/users">
                <a>
                  <UserOutlined />
                  <span className="nav-text">Users</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link href="/admin/songs">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Songs</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link href="/admin/mood">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Moods</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link href="/admin/theme">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Themes</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Link href="/admin/genre">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Genres</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="6">
              <Link href="/admin/instrument">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Instruments</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="7">
              <Link href="/admin/question">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Questions</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="8">
              <Link href="/admin/answer">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Answers</span>
                </a>
              </Link>
            </Menu.Item>
            <Menu.Item key="9">
              <Link href="/admin/support">
                <a>
                  <UnorderedListOutlined />
                  <span className="nav-text">Supports</span>
                </a>
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Sider>
    );
  }
}

export default LeftMenu;
