import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { getProfile, logout } from "../../redux/actions/users";
// ui
import {
  Row,
  Col,
  Input,
  Badge,
  Menu,
  Dropdown,
  Avatar,
  Button,
  Drawer
} from "antd";
import {
  SettingOutlined,
  BellFilled,
  DownOutlined,
  MenuOutlined
} from "@ant-design/icons";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDrawer: false,
      imageUrl: "/images/icons/user.png"
    };
  }

  componentDidMount = async () => {
    await this.props.getProfile();
  };

  showDrawer = () => {
    this.setState({ isDrawer: !this.state.isDrawer });
  };

  onClose = () => {
    this.setState({ isDrawer: false });
  };

  onLogout = async () => {
    await this.props.logout();
    await Router.push("/auth/login");
  };

  render() {
    const { user } = this.props;
    const { isDrawer } = this.state;

    return (
      <div className="header">
          <div className="container">
            <Row gutter={24}>
              <Col lg={4} md={4} sm={4} xs={4}>
                <div className="logo">
                  <Link href="/">
                    <span>SONGS</span>
                  </Link>
                </div>
              </Col>

              <Col lg={14} md={10} sm={0} xs={0}>
                <div className="navBar">
                  <Link href="/explore">
                    <a>Explore</a>
                  </Link>
                  <Link href="/playlists">
                    <a>Playlists</a>
                  </Link>
                  <Link href="/support">
                    <a>Support</a>
                  </Link>
                </div>
              </Col>

              {user ? (
                <Col lg={6} md={10} sm={0} xs={0}>
                  <div className="navMainRight">
                    <div className="actions">
                      {/* <a
                        onClick={() => Router.push("/settings")}
                        className="btn-setting"
                      >
                        <SettingOutlined />
                      </a>
  
                      <Badge count={5}>
                        <a href="#">
                          <BellFilled />
                        </a>
                      </Badge> */}

                      <Dropdown
                        overlay={
                          <Menu>
                            {user && user.role !== "user" && (
                              <Menu.Item key="0">
                                <Link href="/dashboard">
                                  <a>Dashboard</a>
                                </Link>
                              </Menu.Item>
                            )}
                            <Menu.Item key="1">
                              <Link href="/settings">
                                <a>Profile</a>
                              </Link>
                            </Menu.Item>
                            <Menu.Item key="2">
                              <Link href="/plans">
                                <a href="#">Upgrade</a>
                              </Link>
                            </Menu.Item>
                            <Menu.Divider />
                            <Menu.Item key="3" onClick={this.onLogout}>
                              Logout
                            </Menu.Item>
                          </Menu>
                        }
                        trigger={["click"]}
                      >
                        <a
                          className="ant-dropdown-link"
                          onClick={e => e.preventDefault()}
                        >
                          <Avatar
                            style={{ marginRight: 7 }}
                            src={
                              user && user.avatar
                                ? `${URL_MEDIA}${user.avatar.key}`
                                : this.state.imageUrl
                            }
                          />
                          {(user && user.first_name) || ""} <DownOutlined />
                        </a>
                      </Dropdown>
                    </div>
                  </div>
                </Col>
              ) : (
                <Col md={6} sm={0} xs={0}>
                  <div className="navBarRight">
                    <Link href="/auth/login">
                      <a type="link" className="btn-login">
                        Login
                      </a>
                    </Link>
                    <Link href="/auth/register">
                      <a type="link" className="btn-register">
                        Register
                      </a>
                    </Link>
                  </div>
                </Col>
              )}

              <Col lg={0} md={0} xs={20}>
                <div className="sc-mobile">
                  <div className="sc-mb-ctn">
                    <a className="mb-btn" onClick={this.showDrawer}>
                      <MenuOutlined />
                    </a>

                    <Drawer
                      title={null}
                      placement={"top"}
                      closable={false}
                      onClose={this.onClose}
                      visible={isDrawer}
                      className="mb-navBar"
                    >
                      <ul className="navbarMB">
                        <li>
                          <Link href="/explore">
                            <a>Explore</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="/playlists">
                            <a>Playlists</a>
                          </Link>
                        </li>
                        <li>
                          <Link href="/support">
                            <a>Support</a>
                          </Link>
                        </li>
                        <li className="line"></li>

                        {!user ? (
                          <>
                            <li>
                              <Link href="/auth/login">
                                <a>Login</a>
                              </Link>
                            </li>
                            <li>
                              <Link href="/auth/register">
                                <a>Regiser</a>
                              </Link>
                            </li>
                          </>
                        ) : (
                          <>
                            <li>
                              <Link href="/settings">
                                <a>Setting</a>
                              </Link>
                            </li>
                            <li>
                              <Link href="/notification">
                                <a>Notification</a>
                              </Link>
                            </li>
                            <li className="line"></li>
                            <li>
                              <Link href="/dashboard">
                                <a>Dashboard</a>
                              </Link>
                            </li>
                            <li>
                              <Link href="/profile">
                                <a>Profile</a>
                              </Link>
                            </li>
                            <li>
                              <Link href="/plans">
                                <a>Upgrade</a>
                              </Link>
                            </li>
                            <li className="line"></li>
                            <li>
                              <a onClick={this.onLogout}>Logout</a>
                            </li>
                          </>
                        )}
                      </ul>
                    </Drawer>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    // users
    user: store.users.user,
    // auth
    isLogout: store.auth.isLogout
  };
};

const mapDispatchToProps = {
  // users
  getProfile,
  // auth
  logout
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
