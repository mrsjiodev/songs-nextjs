import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Questions = require("../../../../../models/Questions");
const Answers = require("../../../../../models/Answers");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Questions.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Questions Not found" });
    else {
      await Answers.deleteMany({ question: id });
      return res.status(200).json({ message: "Delete Questions successfully" });
    }
  });
});

export default connectDb(handler);
