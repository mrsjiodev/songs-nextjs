import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import connectDb from "../../../middleware/dbMiddleware";

const nextConnect = require("next-connect");
const Users = require("../../../models/Users");
const handler = nextConnect();

handler.post(async (req, res) => {
  const { email, password } = req.body;
  let user = await Users.findOne({ email, role: "admin" }).exec();
  if (user) {
    let checked = bcrypt
      .compare(password, user.password)
      .then(async (checked) => {
        if (checked) {
          let token = await jwt.sign(
            { _id: user._id, email: user.email },
            process.env.secret,
            { expiresIn: "7d" }
          );
          user.token = token;
          await user.save();
          res.json({ success: true, token: "JWT " + token });
        }else {
          res
            .status(401)
            .send({ success: false, msg: "Username or password incorrect!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(401).json({ msg: "Check password failed!" });
      });
  } else {
    res
      .status(401)
      .send({ success: false, msg: "Users not permission action!." });
  }
});

export default connectDb(handler);
