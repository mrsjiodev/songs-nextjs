import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Instruments = require("../../../../../models/Instruments");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let instruments = await Instruments.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(instruments);
});

export default connectDb(handler);
