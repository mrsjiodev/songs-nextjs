import request from '../../config/request';

export function getAnalyticsUser(time) {
  return dispatch => {
    return request().get(`admin/analytics/view/${time}`).then(res => {
      dispatch({
        type: "ACTION_GET_ANALYTICS_USER",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export default {
  getAnalyticsUser
}