const initData = {
  categories: []
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_GET_ALL_CATEGORIES':
      return {
        ...state,
        categories: action.data
      }
    case 'ERROR_GET_ALL_CATEGORIES':
      return {
        ...state,
        categories: []
      }
    default:
      return {
        ...state
      }
  }
}