const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
  answer: { type: String },
  question: { type: Schema.Types.ObjectId, ref: 'Questions' },
  created_by: { type: Schema.Types.ObjectId, ref: 'Users' }
}, {
  timestamps: true
})

module.exports = mongoose.models.Answers || mongoose.model('Answers', AnswerSchema);

