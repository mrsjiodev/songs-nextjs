import request from '../config/request';

export function getProfile(){
  return dispatch => {
    return request().get(`users/profile`).then(res => {
      dispatch({
        type: "ACTION_GET_PROFILE",
        data: res.data
      })
    }).catch(err => {
      console.log(err);
      dispatch({
        type: 'ERROR_GET_PROFILE',
        msgErr: 'Authzication'
      })
    })
  }
}

export function updateProfile(data){
  return dispatch => {
    return request().put(`users/profile`, data).then(res => {
      dispatch({
        type: "ACTION_UPDATED_PROFILE",
        data: res.data 
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_UPDATED_PROFILE'
      })
    })
  }
}

export function logout(){
  return dispatch => {
    localStorage.removeItem('jwt_token');
    dispatch({
      type: "ACTION_LOGOUT"
    })
  }
}

export default {
  getProfile,
  updateProfile,
  logout
}