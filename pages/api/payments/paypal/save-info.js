import nextConnect from 'next-connect';
import connectDb from '../../../../middleware/dbMiddleware'
import checkLogin from '../../../../middleware/checkLogined'

const Users = require('../../../../models/Users');

const hanlder = nextConnect();


hanlder.use(checkLogin);

hanlder.post(async (req, res) => {
  const { _id } = req.user;

  const { orderID, payerID, paypal_id, payer, purchase_units, plan_id } = req.body;

  let user = await Users.findById(_id).exec();
  if (user) {
    user.plan_default = plan_id;
    user.isPaypal = true;
    user.payment_method_default = 'paypal';
    user.paypal_id = paypal_id;
    user.paypal_info = {
      orderID,
      payerID,
      payer,
      purchase_units
    }

    await user.save();

    res.status(200).json(user)
  } else {
    res.status(401).send({ success: false, msg: 'Authentication failed. Users not found.' });
  }
})

export default connectDb(hanlder);