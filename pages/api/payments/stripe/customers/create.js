import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

hanlder.post(async (req, res) => {
  try {
    const { payment_method_id, full_name, city, address, state, country, postal_code } = req.body;
    const { _id } = req.user;

    let user = await Users.findById(_id).exec();

    let new_customer = await stripe.customers.create({
      payment_method: payment_method_id,
      phone: '555-555-5555',
      email: user.email,
      name: full_name,
      invoice_settings: {
        default_payment_method: payment_method_id
      },
      address: {
        city: city,
        line1: address,
        state: state,
        country: country,
        postal_code: postal_code
      }
    });

    user.customer_stripe_id = new_customer.id;
    user.payment_method = payment_method_id;

    await user.save();

    return res.status(200).json(new_customer);
  } catch (err) {
    console.log(err);
    res.status(400).json({ msg: 'Created customer failed!' })
  }
})

export default connectDB(hanlder)