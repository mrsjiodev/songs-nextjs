import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Themes = require("../../../../../models/Themes");
const Songs = require("../../../../../models/Songs");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Themes.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Themes Not found" });
    else {
      await Songs.deleteMany({ theme: id });
      return res.status(200).json({ message: "Delete themes successfully" });
    }
  });
});

export default connectDb(handler);
