const initData = {
  isConfirmEmail: false,
  isNewPassword: false,
  msg: '',
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_CONFIRM_EMAIL':
      return {
        ...state,
        isConfirmEmail: true,
        msg: action.msg
      }
    case 'ERROR_CONFIRM_EMAIL':
      return {
        ...state,
        isConfirmEmail: false,
        msg: action.msg
      }
    case 'ACTION_NEW_PASSWORD':
      return {
        ...state,
        isNewPassword: true,
      }
    case 'ERROR_NEW_PASSWORD':
      return {
        ...state,
        isNewPassword: false
      }
    default:
      return {
        ...state,
        msg: ''
      }
  }
}