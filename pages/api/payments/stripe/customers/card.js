import Stripe from "stripe";
import nextConnect from 'next-connect'
const stripe = new Stripe(process.env.STRIPE_SECRET);
import connectDB from "../../../../../middleware/dbMiddleware";
import checkLogined from "../../../../../middleware/checkLogined";

const hanlder = nextConnect();
const Users = require('../../../../../models/Users')

hanlder.use(checkLogined);

// update create customer
hanlder.post(async (req, res) => {
  try {
    const { _id } = req.user;
    const { payment_id } = req.body;
    console.log(req.body)

    let user = await Users.findById(_id).exec();

    let pm = await stripe.paymentMethods.retrieve(payment_id);

    user.card_info = pm.card;
    user.payment_method_default = 'card';
    user.payment_method = payment_id;
    await user.save();

    await stripe.paymentMethods.attach(
      pm.id,
      { customer: user.customer_stripe_id },
      function (err, paymentMethod) {
        if(err){
          console.log('err; ', err)
          res.status(400).json({ status: 'error', msg: 'attach customer failed' })
        }
        console.log('paymentMethod: ', paymentMethod)
        // asynchronously called
        res.status(200).json(paymentMethod)
      }
    );

  } catch (error) {
    console.log(error)
    res.status(400).json({ status: 'error', msg: 'Update payment method failed!' })
  }

})

export default connectDB(hanlder)