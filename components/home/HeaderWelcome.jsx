import Link from "next/link";
// ui
import { Drawer, Avatar, Dropdown, Menu } from "antd";
import { MenuOutlined, DownOutlined } from "@ant-design/icons";

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

const HeaderWelcome = props => {
  const { isDrawer, user } = props;
  const imageUrl = "/images/icons/user.png";

  return (
    <div className="header-welcome">
      <div className="navBarWC">
        <div className="logos">
          <Link href="/">
            <a>Songs</a>
          </Link>
        </div>
        <div className="sc-desktop">
          <div className="mainBarWC">
            <Link href="/explore">
              <a>Browse Music</a>
            </Link>
            <Link href="/pricing">
              <a>Pricing</a>
            </Link>
            <Link href="/faqs">
              <a>FAQs</a>
            </Link>
            {/* <Link href="/blog">
              <a>Blog</a>
            </Link> */}
          </div>
          {user ? (
            <div className="actions-auth view-profile">
              {/* <Link href="/settings">
                <a className="btn-login">
                  <Avatar
                    style={{ marginRight: 7 }}
                    src={`${URL_MEDIA}${user.avatar.key}` || imageUrl}
                  />
                  {(user && user.first_name) || ""}
                </a>
              </Link> */}

              <Dropdown
                overlay={
                  <Menu>
                    {user && user.role !== "user" && (
                      <Menu.Item key="0">
                        <Link href="/dashboard">
                          <a>Dashboard</a>
                        </Link>
                      </Menu.Item>
                    )}

                    <Menu.Item key="1">
                      <Link href="/settings">
                        <a>Profile</a>
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                      <Link href="/plans">
                        <a href="#">Upgrade</a>
                      </Link>
                    </Menu.Item>
                    <Menu.Divider />
                    <Menu.Item key="3" onClick={() => props.onLogout()}>
                      Logout
                    </Menu.Item>
                  </Menu>
                }
                trigger={["click"]}
              >
                <a
                  className="ant-dropdown-link btn-login"
                  onClick={e => e.preventDefault()}
                >
                  <Avatar
                    style={{ marginRight: 7 }}
                    src={
                      user && user.avatar
                        ? `${URL_MEDIA}${user.avatar.key}`
                        : imageUrl
                    }
                  />
                  {(user && user.first_name) || ""} <DownOutlined />
                </a>
              </Dropdown>
            </div>
          ) : (
            <div className="actions-auth">
              <Link href="/auth/login">
                <a className="btn-login">Login</a>
              </Link>
              <Link href="/auth/register">
                <a className="btn-register">Register</a>
              </Link>
            </div>
          )}
        </div>

        <div className="sc-mobile">
          <a className="mb-btn" onClick={props.showDrawer}>
            <MenuOutlined />
          </a>

          <Drawer
            title={null}
            placement={"top"}
            closable={false}
            onClose={props.onClose}
            visible={isDrawer}
            className="mb-navBar"
          >
            <div className="mainBarWC">
              <Link href="/explore">
                <a>Browse Music</a>
              </Link>
              <Link href="/pricing">
                <a>Pricing</a>
              </Link>
              <Link href="/faqs">
                <a>FAQs</a>
              </Link>
              {/* <Link href="/blog">
                <a>Blog</a>
              </Link> */}
            </div>
            <div className="actions-auth">
              <Link href="/auth/login">
                <a className="btn-login">Login</a>
              </Link>
              <Link href="/auth/register">
                <a className="btn-register">Register</a>
              </Link>
            </div>
          </Drawer>
        </div>
      </div>
    </div>
  );
};

export default HeaderWelcome;
