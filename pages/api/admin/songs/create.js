import connectDb from '../../../../middleware/dbMiddleware'
import nextConnect from 'next-connect';
import uploadMulterS3 from '../../../../middleware/uploadMulterS3';
import bgColors from '../../../../common/bgColors';

const handler = nextConnect();

export const config = {
  api: {
    bodyParser: false
  },
}

const Songs = require('../../../../models/Songs');
const Users = require('../../../../models/Users');


const getFileType = (file) => {
  if (file.mimetype.match('image'))
    return 'image';

  if (file.mimetype.match('video'))
    return 'video';

  if (file.mimetype.match('audio'))
    return 'audio';

  return 'other';
}

handler.post(uploadMulterS3.array('files', 2), async (req, res) => {
  try {
    let user = await Users.findOne({ role: "admin" }).exec();
    const _id = user._id;
    let files = req.files;

    const { genre, instrument, mood, theme, track_title, artist_name, duration } = req.body;

    let audioMD = null;
    let photoMD = null;

    if (files[0]) {
      let F0 = files[0];
      let TF0 = getFileType(F0)
      if (TF0 === 'image') {
        photoMD = F0;
      } else {
        audioMD = F0;
      }
    }

    if (files[1]) {
      let F1 = files[1];
      let TF1 = getFileType(F1);
      if (TF1 === 'image') {
        photoMD = F1;
      } else {
        audioMD = F1;
      }
    }

    let song = new Songs({
      // categories
      theme,
      genre,
      instrument,
      mood,
      // others
      track_title,
      artist_name,
      image: photoMD,
      audio: audioMD,
      duration,
      created_by: _id,
      updated_by: _id,
    })

    if (!photoMD) {
      song.bgColor = bgColors[Math.floor(Math.random() * bgColors.length)];
    }

    await song.save();

    res.status(200).json(song);
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: 'created song failed!' })
  }
})

export default connectDb(handler);