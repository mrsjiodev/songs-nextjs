import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Songs = require("../../../../../models/Songs");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Songs.findOneAndDelete({ _id: id }, (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Song Not found" });
    else return res.status(200).json({ message: "Delete song successfully" });
  });
});

export default connectDb(handler);
