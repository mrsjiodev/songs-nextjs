import React from "react";
import Router from 'next/router';
import { connect } from "react-redux";
// actions
import { allAnswersAndQuestions } from "../../redux/actions/answers";
import { getProfile } from '../../redux/actions/users'
// components
import HeaderPage from "../../components/layouts/Header";
import FooterPage from "../../components/home/FooterWelcome";
import AskedQuestions from "../../components/support/AskedQuestions";
import SupportForm from "../../components/support/SupportForm";
// ui
import { Layout, message } from "antd";

const { Header, Content } = Layout;

class Support extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.props.allAnswersAndQuestions();
    await this.props.getProfile();
    await !this.props.user && Router.push('/');
  };


  render() {
    const { answers_questions } = this.props;

    return (
      <Layout className="home support">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>
        <Content className="content-page">
          <div className="container">
            <div className="support-ctn">
              <h1 className="sp-title">Support</h1>

              <SupportForm />

              <AskedQuestions data={answers_questions} />
            </div>
          </div>
        </Content>
        <FooterPage />
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    // users
    user: store.users.user,
    // answers
    answers_questions: store.answers.answers_questions,
  };
};

const mapDispatchToProps = {
  // users
  getProfile,
  // answers
  allAnswersAndQuestions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Support);
