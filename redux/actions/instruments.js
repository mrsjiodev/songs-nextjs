import request from '../config/request'

export function allInstruments(){
  return dispatch => {
    return request().get(`instruments`).then(res => {
      dispatch({
        type: "ACTION_ALL_INSTRUMENTS",
        data: res.data
      })
    }).catch(err => {
      dispatch({
        type: "ERROR_ALL_INSTRUMENTS"
      })
    })
  }
}

export function createInstrument(data) {
  return dispatch => {
    return request().post(`instruments`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_INSTRUMENT",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CREATED_INSTRUMENT"
      })
    })
  }
}

export default {
  allInstruments,
  createInstrument
}