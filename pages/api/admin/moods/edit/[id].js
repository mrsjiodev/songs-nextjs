import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Moods = require("../../../../../models/Moods");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let mood = await Moods.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(mood);
});

export default connectDb(handler);
