import nextConnect from 'next-connect'
import connectDb from '../../../../middleware/dbMiddleware'
const Users = require('../../../../models/Users');
const Questions = require('../../../../models/Questions');
const handler = nextConnect();

handler.post(async (req, res) => {
  let user = await Users.findOne({ role: "admin" }).exec();
  const  _id  = user._id;
  const { question } = req.body;

  let qs = new Questions({
    question,
    created_by: _id
  });

  await qs.save();

  res.status(200).json(qs)
})

export default connectDb(handler)