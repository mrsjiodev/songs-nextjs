import request from '../../config/request';

export function getAllMoods() {
  return dispatch => {
    return request().get('admin/moods/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_MOODS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteMood(id) {
  return dispatch => {
    return request().delete(`admin/moods/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_MOODS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateMood(id,data) {
  return dispatch => {
    return request().put(`admin/moods/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllMoods,
  deleteMood,
  updateMood
}