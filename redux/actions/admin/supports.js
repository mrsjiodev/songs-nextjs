import request from '../../config/request';

export function createSupport(data){
  return dispatch => {
    return request().post(`admin/supports`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_SUPPORT_ADMIN",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_CREATED_SUPPORT_ADMIN'
      })
    })
  }
} 

export function getAllSupports() {
  return dispatch => {
    return request().get('admin/supports/all').then(res => {
      dispatch({
        type: "ACTION_GET_SUPPORTS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function deleteSupport(id) {
  return dispatch => {
    return request().delete(`admin/supports/delete/${id}`).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateSupport(id,data) {
  return dispatch => {
    return request().put(`admin/supports/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  createSupport,
  getAllSupports,
  deleteSupport,
  updateSupport
}