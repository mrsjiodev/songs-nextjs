import connectDb from '../../../../middleware/dbMiddleware';
import jwt from 'jsonwebtoken';

const nextConnect = require('next-connect')
const Users = require('../../../../models/Users')
const handler = nextConnect();

handler.post(async (req, res) => {
  try {
    const { token } = req.body;

    // check token
    let decoded = await jwt.verify(token, 'secret', { expiresIn: '7d' });
    console.log(decoded);
    if (decoded._id) {
      res.status(200).json({ status: 'success', msg: 'Token correct' })
    } else {
      res.status(401).json({ status: 'success', msg: 'Token incorrect' })
    }
  } catch (error) {
    console.log(error)
    res.status(400).json({ status: 'error', msg: 'The token has expired or is incorrect.' })
  }
})

export default connectDb(handler);