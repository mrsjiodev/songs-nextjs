import request from '../config/request'

export function allAnswersAndQuestions(){
  return dispatch => {
    return request().get(`answers`).then(res => {
      dispatch({
        type: "ACTION_ALL_ANSWERS_OF_QUESTIONS",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_ALL_ANSWERS_OF_QUESTIONS"
      })
    })
  }
}

export default {
  allAnswersAndQuestions
}