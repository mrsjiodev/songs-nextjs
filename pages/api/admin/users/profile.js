import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Users = require('../../../../models/Users');
const handler = nextConnect();

export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let userAdmin = await Users.findOne({ role: "admin" }).exec();
  const  _id  = userAdmin._id;

  let user = await Users.findById(_id).exec();

  delete user.password;

  res.status(200).json(user);
})


export default connectDb(handler);