import request from '../config/request'

export function allGenres(){
  return dispatch => {
    return request().get(`genres`).then(res => {
      dispatch({
        type: "ACTION_ALL_GENRES",
        data: res.data
      })
    }).catch(err => {
      dispatch({
        type: "ERROR_ALL_GENRES"
      })
    })
  }
}

export function createGenre(data) {
  return dispatch => {
    return request().post(`genres`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_GENRE",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CREATED_GENRE"
      })
    })
  }
}

export function setFilterGenres(data){
  return dispatch => {
    dispatch({
      type: "ACTION_SET_FILTER_GENRES",
      data 
    })
  }
}

export default {
  allGenres,
  createGenre,
  setFilterGenres
}