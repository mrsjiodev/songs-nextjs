export const data = [
  {
    title: 'Electronic',
    name: 'electronic'
  },
  {
    title: 'Pop',
    name: 'pop'
  },
  {
    title: 'Hip-hop',
    name: 'hip_hop'
  },
  {
    title: 'Singer',
    name: 'singer'
  },
  {
    title: 'Classical',
    name: 'classical'
  },
  {
    title: 'Cinematic',
    name: 'cinematic'
  },
  {
    title: 'World',
    name: 'world'
  },
  {
    title: 'Jazz',
    name: 'jazz'
  },
  {
    title: 'Funky',
    name: 'funky'
  },
  {
    title: 'Acoustic',
    name: 'acoustic'
  },
  {
    title: 'Indie',
    name: 'indie'
  },

]

export default data;