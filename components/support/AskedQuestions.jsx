// ui
import { Collapse, Select } from "antd";
import { MinusOutlined, PlusOutlined } from "@ant-design/icons";

const { Panel } = Collapse;
const { Option } = Select;

const text = `
A dog is a type of domesticated animal.
Known for its loyalty and faithfulness,
it can be found as a welcome guest in many households across the world.
`;

class AskedQuestions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: "0"
    };
  }

  callback = isActive => {
    this.setState({ isActive });
  };

  genExtra = key => {
    const { isActive } = this.state;

    return isActive === key ? (
      <a className="extra-actions">
        <img src="/images/icons/ic-mine.png" alt="" />
      </a>
    ) : (
      <a className="extra-actions">
        <img src="/images/icons/ic-plus.png" alt="" />
      </a>
    );
  };

  render() {
    const { data } = this.props;

    return (
      <div className="asked-questions">
        <h3 className="asked-title">Frequently asked questions</h3>

        <Collapse
          defaultActiveKey={["0"]}
          onChange={this.callback}
          accordion={true}
          bordered={false}
        >
          {data &&
            data.map((item, i) => (
              <Panel
                showArrow={false}
                header={
                  <div className="question-tt">{item.question.question}</div>
                }
                key={i}
                extra={this.genExtra(String(i))}
              >
                <div className="ans">{item.answer}</div>
              </Panel>
            ))}
        </Collapse>
      </div>
    );
  }
}

export default AskedQuestions;
