import request from '../../config/request';

export function createSong(data) {
  return dispatch => {
    return request().post(`admin/songs/create`, data).then(res => {
      dispatch({
        type: "ACTION_SONGS_CREATED_ADMIN",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: "ERROR_SONGS_CREATED_ADMIN"
      })
    })
  }
}

export function getAllSongs() {
  return dispatch => {
    return request().get('admin/songs/all').then(res => {
      dispatch({
        type: "ACTION_GET_ALL_SONGS",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
      dispatch({
        type: 'ERROR_GET_ALL_SONGS',
      })
    })
  }
}

export function deleteSong(id) {
  return dispatch => {
    return request().delete(`admin/songs/delete/${id}`).then(res => {
      dispatch({
        type: "ACTION_DELETE_SONG",
        data: res.data
      })
    }).catch(err => {
      console.log(err.response)
    })
  }
}

export function updateSong(id,data) {
  return dispatch => {
    return request().put(`admin/songs/edit/${id}`, data).then(res => {
      dispatch({
        data: res.data
      })
    }).catch(err => {
      console.log(err.data);
    })
  }
}

export default {
  getAllSongs,
  deleteSong,
  updateSong,
  createSong
}