import request from '../config/request'

export function createSupport(data){
  return dispatch => {
    return request().post(`supports`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_SUPPORT",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: 'ERROR_CREATED_SUPPORT'
      })
    })
  }
} 

export default {
  createSupport
}