import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
// actions
import { confirmEmail } from "../../redux/actions/reset_password";
// ui
import { Form, Input, Button, message, Spin } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";

class ForgotPasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  handleFinish = async values => {
    const { email } = values;
    this.setState({ isLoading: true });
    await this.props.confirmEmail({ email });
    (await this.props.isConfirmEmail)
      ? message.success(this.props.msg)
      : message.error(this.props.msg);

    await this.setState({ isLoading: false });

    (await this.props.isConfirmEmail) === true
      ? Router.push("/auth/instructions-sent")
      : "";
  };

  render() {
    const { isLoading } = this.state;

    return (
      <Spin spinning={isLoading}>
        <div className="login-content">
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={this.handleFinish}
          >
            <h1>Forgot your password?</h1>
            <Form.Item
              name="email"
              rules={[{ required: true, message: "Please input your Email!" }]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Your Email"
              />
            </Form.Item>
            <div className="action-sb">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Send Me Instructions
              </Button>
            </div>
          </Form>
        </div>
      </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    isConfirmEmail: store.reset_password.isConfirmEmail,
    msg: store.reset_password.msg
  };
};

const mapDispatchToProps = {
  confirmEmail
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordForm);
