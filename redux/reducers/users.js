const initData = {
  user: null,
  favorites: [],
  isNeedLogin: false,
  isUpdatedProfile: false,
  isLogout: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case "ACTION_GET_PROFILE":
      return {
        ...state,
        user: action.data,
        favorites: action.data.favorites,
      }
    case 'ERROR_GET_PROFILE':
      return {
        ...state,
        isNeedLogin: true,
        user: null
      }
    case 'ACTION_UPDATED_PROFILE':
      return {
        ...state,
        isUpdatedProfile: true
      }
    case 'ERROR_UPDATED_PROFILE':
      return {
        ...state,
        isUpdatedProfile: false
      }
    case 'ACTION_LOGOUT':
      return {
        ...state,
        isLogout: true,
        user: null
      }
    default:
      return {
        ...state,
        isNeedLogin: false
      }
  }
}