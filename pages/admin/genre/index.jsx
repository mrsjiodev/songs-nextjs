import {
  Table,
  Input,
  Button,
  Popconfirm,
  Form,
  Layout,
  message,
  Modal,
  Upload,
} from "antd";
import React from "react";
import Router from "next/router";
import UploadImage from "../../../components/dashboard/content/UploadImage";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import HeaderMain from "../../../components/admin/layout/HeaderMain";
import LeftMenu from "../../../components/admin/layout/LeftMenu";
import { connect } from "react-redux";
import moment from "moment";
const URL_MEDIA = process.env.CLOUD_FRONT_URL;
import {
  getAllGenres,
  deleteGenre,
  updateGenre,
} from "../../../redux/actions/admin/genres";
import { createGenre } from "../../../redux/actions/genres";

const { Content } = Layout;

class Genre extends React.Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.formRefs = React.createRef();
  }
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null,
    sortedInfo: null,
    dataSource: [],
    count: 2,
    isShowModalAdd: false,
    selectedRowKeys: [],
    isUploadingImage: false,
    image: null,
    imageUrl: null,
    listImages: [],
    file: null,
  };

  deleteAll = () => {
    this.state.selectedRowKeys.map(async (item, index) => {
      await this.props.deleteGenre(item);
    });
    this.getDataFromAPI();
    this.setState({ selectedRowKeys: [] });
  };

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  };

  handleDelete = async (key) => {
    await this.props.deleteGenre(key);
    this.getDataFromAPI();
  };

  handleAdd = async () => {
    this.setState({
      isShowModalAdd: true,
    });
  };

  handleChange = (pagination, filters, sorter) => {
    console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon={<SearchOutlined />}
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  getDataFromAPI = async () => {
    await this.props.getAllGenres();
    this.setState({ dataSource: this.props.genres });
    const { dataSource } = this.state;
    dataSource.map((item, index) => {
      item.key = item._id;
    });
    this.setState({ dataSource });
  };

  componentDidMount = async () => {
    if (localStorage.getItem("jwt_token_admin")) {
      this.getDataFromAPI();
    } else {
      Router.push("/admin");
    }
  };

  onCancel = () => {
    this.setState({ isShowModalAdd: false, isShowModalEdit: false });
  };
  handleFinish = async (values) => {
    this.setState({ isLoading: true });
    const { image } = this.state;
    let fd = new FormData();
    fd.append("files", image.originFileObj);
    fd.append("title", values.title);
    fd.append("name", values.name);
    await this.props.createGenre(fd);
    await this.getDataFromAPI();
    this.setState({ isShowModalAdd: false });
  };

  onChangeFile = (info, type) => {
    if (type === "image") {
      this.setState({ isUploadingImage: true });
      if (info.file.name) {
        this.getBase64(info.file.originFileObj, (imageUrl) => {
          this.setState({
            image: info.file,
            listImages: info.fileList,
            imageUrl,
            isUploadingImage: false,
          });
          this.formRef.current.setFieldsValue({ image: info.file });
        });
      }
    }
  };

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  onRemoveFile = (info, type) => {
    console.log("onRemoveFile: ", info);
    if (type === "image") {
      this.setState({ image: null, listImages: [] });
    }
  };

  beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  edit = async (item) => {
    await this.setState({ title: item.title, id: item._id, name: item.name });
    await this.setState({
      isShowModalEdit: true,
    });
    // this.setState({ imageUrl: `${URL_MEDIA}${item.image.key}` });
    await this.formRefs.current.setFieldsValue({
      title: this.state.title,
    });
  };

  handleEdit = async (values) => {
    let fd = new FormData();
    fd.append("title", values.title);
    // if (this.state.file) {
    //   fd.append("image", this.state.file.originFileObj);
    // }
    await this.props.updateGenre(this.state.id, fd);
    await this.getDataFromAPI();
    this.setState({
      isShowModalEdit: false,
      // imageUrl: null,
      // file: null
    });
  };

  onChangeTitle = (e) => {
    this.setState({ title: e.target.value });
  };

  onCancelDelete = () => {
    this.setState({ selectedRowKeys: [] });
  };

  // onChangeName = (e) => {
  //   this.setState({ name: e.target.value });
  // };

  onChangeImage = async (info, id) => {
    if (info.file.status !== "uploading") {
      this.setState({ loading: true });
    }
    if (info.file.status === "done") {
      this.getBase64Edit(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
          file: info.file,
        })
      );
      let fd = new FormData();
      fd.append("image", info.file.originFileObj);
      await this.props.updateGenre(id, fd);
      await this.getDataFromAPI();
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  getBase64Edit(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUploadEdit(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  render() {
    let {
      sortedInfo,
      filteredInfo,
      selectedRowKeys,
      isUploadingImage,
      image,
      imageUrl,
    } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    const columns = [
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
        sorter: (a, b) => a.title.localeCompare(b.title),
        sortOrder: sortedInfo.columnKey === "title" && sortedInfo.order,
        ...this.getColumnSearchProps("title"),
      },
      // {
      //   title: "Name",
      //   dataIndex: "name",
      //   key: "name",
      //   sorter: (a, b) => a.name.localeCompare(b.name),
      //   sortOrder: sortedInfo.columnKey === "name" && sortedInfo.order,
      //   ellipsis: true,
      //   ...this.getColumnSearchProps("name"),
      // },
      {
        title: "Image",
        dataIndex: "image",
        key: "image",
        sorter: true,
        render: (image, record) => (
          // image &&
          // <img
          //   style={{ width: 150, height: 100 }}
          //   src={`${URL_MEDIA}${image.key || ""}`}
          //   alt=""
          // />
          <Upload
            className="btn-upload-image"
            accept="image/png, imgage/jpg, image/jpeg"
            name="image"
            onChange={(info) => this.onChangeImage(info, record._id)}
            beforeUpload={this.beforeUploadEdit}
            showUploadList={false}
          >
            {record && record.image ? (
              <img
                title="Click to Change image of genre"
                style={{ width: 100, height: 100, cursor: "pointer" }}
                src={`${URL_MEDIA}${record.image.key || ""}`}
                alt=""
              />
            ) : (
              <div>
                <Button>Upload</Button>
              </div>
            )}
          </Upload>
        ),
      },
      {
        title: "Updated",
        dataIndex: "updatedAt",
        key: "updatedAt",
        sorter: (a, b) =>
          moment(a.updatedAt).unix() - moment(b.updatedAt).unix(),
        sortOrder: sortedInfo.columnKey === "updatedAt" && sortedInfo.order,
        ...this.getColumnSearchProps("updatedAt"),
      },
      {
        title: "Action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <a
              onClick={() => {
                this.edit(record);
              }}
              style={{ marginRight: 15 }}
            >
              Edit
            </a>
            {this.state.dataSource.length >= 1 ? (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            ) : null}
          </span>
        ),
      },
    ];

    return (
      <Layout className="dashboard">
        <HeaderMain />

        <Layout>
          <LeftMenu />

          <Layout className="main-ctn">
            <Content className="content-plan">
              <div>
                <Button
                  onClick={this.handleAdd}
                  type="primary"
                  style={{
                    marginBottom: 16,
                  }}
                >
                  Add Genre
                </Button>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalEdit}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    ref={this.formRefs}
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleEdit}
                  >
                    <h1>Edit Genre</h1>
                    {/* <Form.Item
                      name="image"
                    >
                      <div className="st-image" style={{textAlign: "center"}}>
                        <Upload
                          className="btn-upload-image"
                          accept="image/png, imgage/jpg, image/jpeg"
                          name="image"
                          onChange={(info) => this.onChangeImage(info)}
                          beforeUpload={this.beforeUploadEdit}
                          showUploadList={false}
                        >
                         {this.state.imageBefore ? <img
                            src={imageUrl}
                            alt=""
                            style={{ cursor: "pointer" }}
                            title="Change image of song"
                          /> : <div>
                            <img
                            src={imageUrl}
                            alt=""
                            style={{ cursor: "pointer" }}
                            title="Change image of song"
                          />
                          <Button>Upload image</Button>
                          </div>}
                        </Upload>
                      </div>
                    </Form.Item> */}
                    <Form.Item name="title">
                      <Input placeholder="Title" />
                    </Form.Item>

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                        style={{ marginTop: 20 }}
                      >
                        Edit
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <Modal
                  className="custom-modal-admin"
                  visible={this.state.isShowModalAdd}
                  footer={null}
                  onCancel={this.onCancel}
                >
                  <Form
                    ref={this.formRef}
                    name="normal_login"
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={this.handleFinish}
                  >
                    <h1>Create Genre</h1>
                    <Form.Item
                      name="image"
                      rules={[
                        { required: true, message: "Please select image!" },
                      ]}
                    >
                      <UploadImage
                        isUploadingImage={isUploadingImage}
                        dataFile={image}
                        imageUrl={imageUrl}
                        onChangeFile={this.onChangeFile}
                        onRemoveFile={this.onRemoveFile}
                        beforeUpload={this.beforeUpload}
                      />
                    </Form.Item>
                    <Form.Item
                      name="title"
                      rules={[
                        {
                          required: true,
                          message: "Please input your title!",
                        },
                      ]}
                    >
                      <Input placeholder="Title" />
                    </Form.Item>
                    <Form.Item
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Please input your name!",
                        },
                      ]}
                    >
                      <Input placeholder="Name" />
                    </Form.Item>

                    <div className="action-sb">
                      <Button
                        type="primary"
                        htmlType="submit"
                        className="admin-form-button"
                      >
                        Create
                      </Button>
                    </div>
                  </Form>
                </Modal>
                <div style={{ marginBottom: 16 }}>
                  <Popconfirm
                    disabled={!hasSelected}
                    title="Sure to delete?"
                    onConfirm={this.deleteAll}
                    onCancel={this.onCancelDelete}
                  >
                    <Button type="primary" disabled={!hasSelected}>
                      Delete All
                    </Button>
                  </Popconfirm>
                  <span style={{ marginLeft: 8 }}>
                    {hasSelected
                      ? `Selected ${selectedRowKeys.length} genres`
                      : ""}
                  </span>
                </div>
                <Table
                  rowSelection={rowSelection}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "50", "100"],
                  }}
                  bordered
                  dataSource={this.state.dataSource}
                  onChange={this.handleChange}
                />
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    genres: store.adminGenre.genres,
  };
};

const mapDispatchToProps = {
  getAllGenres,
  deleteGenre,
  updateGenre,
  createGenre,
};

export default connect(mapStateToProps, mapDispatchToProps)(Genre);
