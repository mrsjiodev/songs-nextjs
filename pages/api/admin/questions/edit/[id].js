import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Questions = require("../../../../../models/Questions");
const handler = nextConnect();

handler.put(async (req, res) => {
  const {
    query: { id },
  } = req;

  let questions = await Questions.findOneAndUpdate(
    { _id: id },
    { $set: req.body },
    (err, response) => {
      if (err) throw err;
    }
  ).exec();

  return res.status(200).json(questions);
});

export default connectDb(handler);
