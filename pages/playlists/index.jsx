import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { allGenres, setFilterGenres } from "../../redux/actions/genres";
// components
import HeaderPage from "../../components/layouts/Header";
import FooterWelcome from "../../components/home/FooterWelcome";
// ui
import { Layout, Button, Row, Col } from "antd";

const { Header, Content, Footer } = Layout;

const URL_MEDIA = process.env.CLOUD_FRONT_URL;

class Playlists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.props.allGenres();
  };

  onRedirectGenre = async data => {
    await this.props.setFilterGenres(data);
    await Router.push('/explore')
  }

  render() {
    const { genres } = this.props;

    return (
      <Layout className="home playlists">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>
        <Content className="content-page">
          <div className="playlists-ctn">
            <div className="container">
              <div className="banner">
                <h1>
                  Find what your're after faster by exploring our different
                  genres
                </h1>
                <div className="subscribe">
                  <p>Subscribe and get unlimited downloads</p>
                  <Button
                    type="primary"
                    onClick={() => Router.push("/auth/register")}
                  >
                    Start free trial
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div className="genres">
            <Row gutter={0}>
              {genres &&
                genres.map((item, k) => (
                  <Col key={k} lg={6} md={8} sm={12} xs={12}>
                    <div
                      className="genre-item"
                      onClick={() => this.onRedirectGenre(item)}
                    >
                      <div className="bg-genre">
                        <img
                          className="bg"
                          src={`${URL_MEDIA}${
                            (item.image && item.image.key) || ""
                          }`}
                          alt="background"
                        />
                      </div>
                      <div className="genre-box">
                        <h3 className="genre-title">{item.title}</h3>
                      </div>
                    </div>
                  </Col>
                ))}
            </Row>
          </div>
        </Content>
        <FooterWelcome />
      </Layout>
    );
  }
}

const mapStateToProps = store => {
  return {
    genres: store.genres.genres
  };
};

const mapDispatchToProps = {
  allGenres,
  setFilterGenres
};

export default connect(mapStateToProps, mapDispatchToProps)(Playlists);
