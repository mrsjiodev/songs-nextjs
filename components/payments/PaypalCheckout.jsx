import React from "react";
import { connect } from "react-redux";
import { PayPalButton } from "react-paypal-button-v2";
// actions
import { saveInfoPaypal } from "../../redux/actions/payments";
// ui
import { message } from "antd";

class PaypalCheckout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPaypalSubscription = async (details, data) => {
    message.success("Subscription completed");

    const { plan_id } = this.props;
    let obj = {
      orderID: data.orderID,
      payerID: data.payerID,
      paypal_id: details.paypal_id,
      payer: details.payer,
      purchase_units: details.purchase_units,
      plan_id
    };

    await this.props.saveInfoPaypal(obj);
  };

  render() {
    const { amount_plan, currency } = this.props;
    return (
      <PayPalButton
        amount={amount_plan / 100}
        currency={currency.toUpperCase()}
        options={{
          clientId: process.env.PAYPAL_CLIENT_ID,
        }}
        shippingPreference="NO_SHIPPING"
        onError={() => message.error('Subscription failed!')}
        onSuccess={(details, data) => this.onPaypalSubscription(details, data)}
      />
    );
  }
}

const mapStateToProps = store => {
  return {
    isSaveInfoPaypal: store.payments.isSaveInfoPaypal,
    paypalInfo: store.payments.paypalInfo,
    plans: store.payments.plans,
  };
};

const mapDispatchToProps = {
  saveInfoPaypal
};

export default connect(mapStateToProps, mapDispatchToProps)(PaypalCheckout);
