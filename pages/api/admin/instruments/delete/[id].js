import connectDb from "../../../../../middleware/dbMiddleware";
const nextConnect = require("next-connect");
const Instruments = require("../../../../../models/Instruments");
const Songs = require("../../../../../models/Songs");

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
};

handler.delete(async (req, res) => {
  const {
    query: { id },
  } = req;
  await Instruments.findOneAndDelete({ _id: id }, async (err, doc) => {
    if (doc === null)
      return res.status(401).json({ message: "Instruments Not found" });
    else {
      await Songs.deleteMany({ instrument: id });
      return res
        .status(200)
        .json({ message: "Delete Instruments successfully" });
    }
  });
});

export default connectDb(handler);
