// packages
import React from "react";
// components
import PaymentForm from "../../components/payments/PaymentForm";
// ui
import { Tabs } from "antd";

const { TabPane } = Tabs;

const PaymentContent = props => {
  const onChangeTab = key => {
    console.log("tab: ", key);
    props.onSelectedPlan(key);
  };

  return (
    <div className="payment-content">
      <Tabs defaultActiveKey="2" onChange={onChangeTab}>
        <TabPane tab="Billed Monthly" key="2">
          <PaymentForm
            onLoading={props.onLoading}
            amount_plan={props.amount_plan}
            type="monthly"
            plan_id={props.plan_id}
            currency={props.currency}
          />
        </TabPane>
        <TabPane tab="Billed Yearly" key="1">
          <PaymentForm
            onLoading={props.onLoading}
            amount_plan={props.amount_plan}
            type="yearly"
            plan_id={props.plan_id}
            currency={props.currency}
          />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default PaymentContent;
