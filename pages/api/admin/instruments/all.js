import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Instruments = require('../../../../models/Instruments');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let instruments = await Instruments.find({})
  .exec();
  res.status(200).json(instruments);
})

export default connectDb(handler);