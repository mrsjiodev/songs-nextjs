import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { login } from "../../redux/actions/auth";
import { getProfile } from "../../redux/actions/users";
// components
import HeaderPage from "../../components/layouts/Header";
import LoginForm from "../../components/auth/LoginForm";

// ui
import { Layout, message, Spin } from "antd";

const { Header, Content } = Layout;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  componentDidUpdate = async (nextProps, nextState) => {
    if (nextProps.isLogined === true && this.props.user) {
      if (this.props.user.plan_default) {
        Router.push("/explore");
      } else {
        Router.push("/explore");
      }
    }
  };

  componentDidMount = async () => {
    (await this.props.user) && Router.push("/explore");
  };

  onFinish = async values => {
    this.setState({ isLoading: true });
    console.log("Received values of form: ", values);
    await this.props.login(values);
    (await (this.props.isLogined === true))
      ? message.success("Login successfully!")
      : message.error(this.props.msgErr);
    (await (this.props.isLogined === true)) && this.props.getProfile();
  };

  render() {
    const { isLoading } = this.state;

    return (
      // <Spin spinning={isLoading}>
      <Layout className="home login">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>

        <Content>
          <LoginForm onFinish={this.onFinish} />
        </Content>
      </Layout>
      // </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    // auth
    isLogined: store.auth.isLogined,
    msgErr: store.auth.msgErr,
    user: store.users.user
  };
};

const mapDispatchToProps = {
  // auth
  login,
  // users
  getProfile
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
