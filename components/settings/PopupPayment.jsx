// packages
import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import {
  CardElement,
  Elements,
  useStripe,
  useElements
} from "@stripe/react-stripe-js";
// components
import PaymentForm from "./PaymentForm";

const stripePromise = loadStripe(process.env.STRIPE_KEY);

class PopupPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      plan_id: "",
      currency: "usd",
      type: "monthly"
    };
  }

  onLoading = () => {};

  render() {
    const { plan_id, currency, type } = this.state;
    const { isShowUpdateAddress, isShowUpdatePayment, onShowUpdatePaymentCard, onShowUpdateAddressBilling } = this.props;

    return (
      <div className="popup-payment">
        <Elements stripe={stripePromise}>
          <PaymentForm
            onLoading={this.onLoading}
            plan_id={plan_id}
            currency={currency}
            type={type}
            isShowUpdateAddress={isShowUpdateAddress}
            isShowUpdatePayment={isShowUpdatePayment}
            onShowUpdatePaymentCard={onShowUpdatePaymentCard}
            onShowUpdateAddressBilling={onShowUpdateAddressBilling}
          />
        </Elements>
      </div>
    );
  }
}

export default PopupPayment;
