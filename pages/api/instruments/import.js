import nextConnect from 'next-connect'
import connectDb from '../../../middleware/dbMiddleware'
import Instruments from '../../../models/Instruments'

import data from './data';

const hanlder = nextConnect();

hanlder.post(async (req, res) => {
  try {

    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      let mood = new Instruments({
        title: item.title,
        name: item.name
      });

      await mood.save();
    }

    res.status(200).json({ msg: 'Import successfully!' })
  } catch (error) {
    console.log(error)
    res.status(400).json({ msg: "Import Instruments failed" })
  }
})

export default connectDb(hanlder);