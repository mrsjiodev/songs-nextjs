const initData = {
  themes: [],
  isCreatedGenre: false
}

export default function (state = initData, action) {
  switch (action.type) {
    case 'ACTION_ALL_THEMES':
      return {
        ...state,
        themes: action.data
      }
    case 'ERROR_ALL_THEMES':
      return {
        ...state,
        themes: []
      }
    case 'ACTION_CREATED_THEME':
      return {
        ...state,
        isCreatedGenre: true
      }
    case 'ERROR_CREATED_THEME':
      return {
        ...state,
        isCreatedGenre: false
      }
    default:
      return {
        ...state,
      }
  }
}