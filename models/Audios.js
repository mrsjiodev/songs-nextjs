const mongoose = require('mongoose');
const Schema = mongoose.Schema

const AudioSchema = new Schema({
  filename: { type: String },
  fieldname: { type: String },
  originalname: { type: String },
  encoding: { type: String },
  mimetype: { type: String },
  destination: { type: String },
  size: { type: Number },
  path: { type: String },
}, {
  timestamps: true
})

module.exports = mongoose.models.Audios || mongoose.model('Audios', AudioSchema);