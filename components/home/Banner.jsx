// packages
import Router from 'next/router'

// ui
import { Row, Col, Button } from "antd";

const onStartNow = () => {
  Router.push('/explore')
}

const Banner = props => {
  return (
    <div className="banner">
      <Row gutter={48}>
        <Col lg={12} md={12} xs={24}>
          <div className="ctn-left">
            <div className="info">
              <h1>The best music for your videos</h1>
              <p>Unlimited royalty free music</p>
              <Button className="default" onClick={onStartNow}>Start Now</Button>
            </div>
          </div>
        </Col>
        <Col lg={12} md={12} xs={24}>
          <div className="ctn-right">
            <div className="bg-page">
              <img src="/images/welcome/main.png" alt="" />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Banner;
