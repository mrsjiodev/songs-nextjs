import connectDb from '../../../../middleware/dbMiddleware'
const nextConnect = require('next-connect')
const Supports = require('../../../../models/Supports');

const handler = nextConnect();
export const config = {
  api: {
    bodyParser: false,
  },
}

handler.get(async (req, res) => {
  let support = await Supports.find({})
  .populate({
    path: "user",
    model: "Users",
  })
  .exec();
  res.status(200).json(support);
})

export default connectDb(handler);