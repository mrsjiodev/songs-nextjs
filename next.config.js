require('dotenv').config()
const path = require('path')
const Dotenv = require('dotenv-webpack')
const cssLoaderConfig = require('@zeit/next-css/css-loader-config')

module.exports = (nextConfig = {}) => {
  
  return Object.assign({}, nextConfig, {
    webpack(config, options) {
      if (!options.defaultLoaders) {
        throw new Error(
          'This plugin is not compatible with Next.js versions below 5.0.0 https://err.sh/next-plugins/upgrade'
        )
      }

      config.devtool = false;

      config.plugins = [
        ...config.plugins,
        new Dotenv({
          path: path.join(__dirname, '.env'),
          systemvars: true
        })
      ]

      env = {
        customKey: 'my-value',
        rapidapiURL: 'https://deezerdevs-deezer.p.rapidapi.com/',
        rapidapiHost: 'deezerdevs-deezer.p.rapidapi.com',
        rapidapiKey: '4c6c2850b9msh5ceb5c12a1909e2p1776d8jsnf40c3069aa7a',
      }

      const { dev, isServer } = options
      const {
        cssModules,
        cssLoaderOptions,
        postcssLoaderOptions,
        lessLoaderOptions = {}
      } = nextConfig

      options.defaultLoaders.less = cssLoaderConfig(config, {
        extensions: ['less'],
        cssModules,
        cssLoaderOptions,
        postcssLoaderOptions,
        dev,
        isServer,
        loaders: [
          {
            loader: 'less-loader',
            options: lessLoaderOptions
          }
        ]
      })

      options.config.env = {
        customKey: 'my-value',
        rapidapiURL: 'https://deezerdevs-deezer.p.rapidapi.com/',
        rapidapiHost: 'deezerdevs-deezer.p.rapidapi.com',
        rapidapiKey: '4c6c2850b9msh5ceb5c12a1909e2p1776d8jsnf40c3069aa7a',
      }

      config.module.rules.push({
        test: /\.less$/,
        use: options.defaultLoaders.less
      })

      if (typeof nextConfig.webpack === 'function') {
        return nextConfig.webpack(config, options)
      }

      return config
    }
  })
}