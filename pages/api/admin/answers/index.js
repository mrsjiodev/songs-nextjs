import nextConnect from 'next-connect'
import connectDb from '../../../../middleware/dbMiddleware'

const Answers = require('../../../../models/Answers');
const Users = require('../../../../models/Users');
const handler = nextConnect();

handler.post(async (req, res) => {
  let user = await Users.findOne({ role: "admin" }).exec();
  const _id = user._id;
  const { question, answer } = req.body;

  let qs = new Answers({
    question,
    answer,
    created_by: _id
  });

  await qs.save();

  res.status(200).json(qs)
})

export default connectDb(handler)