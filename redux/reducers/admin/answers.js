const initData = {
  answers: [],
  isCreatedAnswer : false
}

export default function (state = initData, action) {
  switch (action.type) { 
    case 'ACTION_CREATED_ANSWERS':
      return {
        ...state,
        isCreatedAnswer: true
      } 
    case 'ACTION_GET_ANSWERS':
      return {
        ...state,
        answers: action.data
      } 
    default:
      return {
        ...state,
        isCreatedAnswer: false
      }
  }
}