import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
// actions
import { register } from "../../redux/actions/auth";
// components
import HeaderPage from "../../components/layouts/Header";
import RegisterForm from "../../components/auth/RegisterForm";

// ui
import { Layout, message, Spin } from "antd";

const { Header, Content } = Layout;

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  componentDidMount = async () => {
    (await this.props.user) && Router.push("/explore");
  };

  onFinish = async values => {
    this.setState({ isLoading: true });
    console.log("Received values of form: ", values);
    await this.props.register(values);
    (await this.props.isRegister)
      ? message.success("Register successfully!")
      : message.error("Register failed. Try again!");
    (await this.props.isRegister) && Router.push("/auth/login");
  };

  render() {
    const { isLoading } = this.state;

    return (
      // <Spin spinning={isLoading}>
      <Layout className="home login">
        <Header style={{ position: "fixed", zIndex: 10, width: "100%" }}>
          <HeaderPage />
        </Header>

        <Content>
          <RegisterForm onFinish={this.onFinish} />
        </Content>
      </Layout>
      // </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    // auth
    isRegister: store.auth.isRegister,
    user: store.users.user
  };
};

const mapDispatchToProps = {
  // auth
  register
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
