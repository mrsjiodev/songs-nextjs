import React from "react";
import { connect } from "react-redux";
import moment from "moment";
// actions
import { createSong } from "../../redux/actions/songs";
import { getAllCategories } from "../../redux/actions/categories";
import { allMoods } from "../../redux/actions/moods";
import { allThemes } from "../../redux/actions/themes";
import { allGenres } from "../../redux/actions/genres";
import { allInstruments } from "../../redux/actions/instruments";
// components
import UploadAudio from "./content/UploadAudio";
import UploadImage from "./content/UploadImage";
import Categories from "../common/Categories";
// ui
import { Form, Input, Row, Col, Button, Spin, message } from "antd";

class DashboardMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      imageUrl: null,
      listImages: [],
      audio: null,
      audioUrl: null,
      listAudios: [],
      duration: null,
      isLoading: false,
      isUploadingAudio: false,
      isUploadingImage: false,
      isSubmit: true
    };
    this.formRef = React.createRef();
  }

  componentDidMount = async () => {
    this.props.getAllCategories();
    this.props.allInstruments();
    this.props.allGenres();
    this.props.allMoods();
    this.props.allThemes();
  };

  onFinish = async values => {
    this.setState({ isLoading: true });
    console.log("values: ", values);
    const { image, audio, duration } = this.state;

    let fd = new FormData();
    if (image) {
      fd.append("files", image.originFileObj);
    }
    fd.append("files", audio.originFileObj);
    fd.append("genre", values.genre);
    fd.append("instrument", values.instrument);
    fd.append("mood", values.mood);
    fd.append("theme", values.theme);
    fd.append("track_title", values.track_title);
    fd.append("artist_name", values.artist_name);
    fd.append("duration", duration);

    await this.props.createSong(fd);
    (await this.props.isCreatedSong)
      ? message.success("Created song successfully!")
      : message.error("Created song error!");

    (await this.props.isCreatedSong) && this.onReset();
    (await this.props.isCreatedSong) && this.setState({ isLoading: false });
    (await this.props.isError) && this.setState({ isLoading: false });
  };

  onChangeFile = (info, type) => {
    if (type === "audio") {
      let file = info.file.originFileObj;
      let reader = new FileReader();
      reader.onloadend = () => {
        let media = new Audio(reader.result);
        media.onloadedmetadata = () => {
          this.setState({ duration: media.duration });
        };
      };
      reader.readAsDataURL(file);

      // this.setState({ isUploadingAudio: true });
      if (info.file.name) {
        this.getBase64(info.file.originFileObj, audioUrl => {
          this.setState({
            audio: info.file,
            listAudios: info.fileList,
            audioUrl,
            isUploadingAudio: false
          });
          this.formRef.current.setFieldsValue({ audio: info.file });
        });
      }
    }

    if (type === "image") {
      this.setState({ isUploadingImage: true });
      if (info.file.name) {
        this.getBase64(info.file.originFileObj, imageUrl => {
          this.setState({
            image: info.file,
            listImages: info.fileList,
            imageUrl,
            isUploadingImage: false
          });
          this.formRef.current.setFieldsValue({ image: info.file });
        });
      }
    }
  };

  onRemoveFile = (info, type) => {
    if (type === "image") {
      this.setState({ image: null, listImages: [] });
    }

    if (type === "audio") {
      this.setState({ audio: null, listAudios: [], duration: null });
    }
  };

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  beforeUploadAudio = () => {
    this.setState({ isUploadingAudio: true });
  };

  onReset = () => {
    this.formRef.current.resetFields();
    this.setState({
      audio: null,
      image: null,
      audioUrl: null,
      imageUrl: null,
      listAudios: [],
      listImages: []
    });
  };

  onChangeFilters = async (value, name) => {
    console.log(value, name);
  };

  render() {
    const {
      image,
      imageUrl,
      audio,
      isLoading,
      isUploadingAudio,
      isUploadingImage
    } = this.state;

    const { instruments, moods, themes, genres } = this.props;

    return (
      <Spin spinning={isLoading}>
        <div className="main-dashboard">
          <Form
            ref={this.formRef}
            name="normal_login"
            className="form-upload"
            initialValues={{
              // mood: "Angry",
              // theme: "Vlog",
              // genre: "Electronic",
              // instrument: "Piano"
            }}
            onFinish={this.onFinish}
          >
            <Row gutter={36}>
              <Col lg={10} md={24} sm={24} xs={24}>
                <Form.Item
                  name="image"
                // rules={[{ required: true, message: "Please select image!" }]}
                >
                  <UploadImage
                    isUploadingImage={isUploadingImage}
                    dataFile={image}
                    imageUrl={imageUrl}
                    onChangeFile={this.onChangeFile}
                    onRemoveFile={this.onRemoveFile}
                    beforeUpload={this.beforeUpload}
                  />
                </Form.Item>
              </Col>
              <Col lg={14} md={24} sm={24} xs={24}>
                <Form.Item
                  name="audio"
                  rules={[{ required: true, message: "Please select audio!" }]}
                >
                  <UploadAudio
                    dataFile={audio}
                    isUploadingAudio={isUploadingAudio}
                    onChangeFile={this.onChangeFile}
                    onRemoveFile={this.onRemoveFile}
                    beforeUpload={this.beforeUploadAudio}
                  />
                </Form.Item>
              </Col>
              <Col lg={12} xs={24}>
                <Form.Item
                  name="track_title"
                  rules={[
                    {
                      required: true,
                      message: "Please input your track title!"
                    }
                  ]}
                >
                  <Input placeholder="Track Title" />
                </Form.Item>
              </Col>
              <Col lg={12} xs={24}>
                <Form.Item
                  name="artist_name"
                  rules={[
                    {
                      required: true,
                      message: "Please input your artist name!"
                    }
                  ]}
                >
                  <Input placeholder="Artist Name" />
                </Form.Item>
              </Col>
              <Col md={24}>
                <Categories
                  instruments={instruments}
                  moods={moods}
                  themes={themes}
                  required={true}
                  genres={genres}
                  onChangeFilters={this.onChangeFilters}
                />
              </Col>
              <Col xs={24}>
                <div className="actions">
                  <Button
                    htmlType="submit"
                    type="primary"
                    className="btn-upload"
                    disabled={!this.state.isSubmit}
                  >
                    Upload
                  </Button>
                  <Button type="default" className="btn-cancel">
                    Cancel
                  </Button>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
      </Spin>
    );
  }
}

const mapStateToProps = store => {
  return {
    // songs
    isCreatedSong: store.songs.isCreatedSong,
    isError: store.songs.isError,
    song: store.songs.song,
    // categories
    categories: store.categories.categories,
    themes: store.themes.themes,
    moods: store.moods.moods,
    genres: store.genres.genres,
    instruments: store.instruments.instruments
  };
};

const mapDispatchToProps = {
  createSong,
  // categories
  getAllCategories,
  allMoods,
  allGenres,
  allThemes,
  allInstruments
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardMain);
