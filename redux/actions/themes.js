import request from '../config/request'

export function allThemes(){
  return dispatch => {
    return request().get(`themes`).then(res => {
      dispatch({
        type: "ACTION_ALL_THEMES",
        data: res.data
      })
    }).catch(err => {
      dispatch({
        type: "ERROR_ALL_THEMES"
      })
    })
  }
}

export function createThem(data) {
  return dispatch => {
    return request().post(`themes`, data).then(res => {
      dispatch({
        type: "ACTION_CREATED_THEME",
        data: res.data
      })
    }).catch(err => {
      console.log(err)
      dispatch({
        type: "ERROR_CREATED_THEME"
      })
    })
  }
}

export default {
  allThemes,
  createThem
}