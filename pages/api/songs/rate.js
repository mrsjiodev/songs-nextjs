import connectDb from '../../../middleware/dbMiddleware'
import nextConnect from 'next-connect'; 

const handler = nextConnect();

const Songs = require('../../../models/Songs');

handler.post(async (req, res) => {
  console.log(req.body)
  let { song_id } = req.body;

  let song = await Songs.findById(song_id).exec();
  let total_played = Number(song.total_played) + 1;
  song.total_played = total_played;
  await song.save();

  res.status(200).json(song);
})

export default connectDb(handler);