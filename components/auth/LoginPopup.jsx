import React from "react";
import Link from "next/link";
// ui
import { Row, Col, Button, Input, Form } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";

class LoginPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onFinish = values => {
    this.props.onLoginPopup(values);
  };

  render() {
    return (
      <div className="login-popup-form">
        <Row gutter={0}>
          <Col lg={10} md={9} xs={24}>
            <div className="form-left">
              <div className="bn">
                <h3>Songs</h3>
                <p>Unlimited Songs</p>
              </div>
            </div>
          </Col>
          <Col lg={14} md={15} xs={24}>
            <div className="form-right">
              <h1>Sign In</h1>
              <Form onFinish={this.onFinish}>
                <Form.Item
                  name="email"
                  rules={[
                    { required: true, message: "Please input your email!" }
                  ]}
                >
                  <Input
                    placeholder="Your Email"
                    type="email"
                    prefix={<MailOutlined />}
                  />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[
                    { required: true, message: "Please input your Password!" }
                  ]}
                >
                  <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Your Password"
                  />
                </Form.Item>

                <Form.Item>
                  <p className="act-forgot">
                    <Link href="/auth/forgot-password">
                      <a className="login-form-forgot">Forgot Password?</a>
                    </Link>
                  </p>
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                  >
                    Sign in
                  </Button>
                  <p className="act-reg">
                    Dont have an account?{" "}
                    <Link href="/auth/register">
                      <a>Register</a>
                    </Link>
                  </p>
                </Form.Item>
              </Form>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default LoginPopup;
